﻿$PBExportHeader$uo_transaction.sru
forward
global type uo_transaction from transaction
end type
end forward

global type uo_transaction from transaction
end type
global uo_transaction uo_transaction

type prototypes
subroutine SP_IOJINTONG(string ARG_SDATE,string ARG_EDATE,double ARG_NALSU) RPCFUNC ALIAS FOR "SP_IOJINTONG"
subroutine SP_ROOM_ROTATION(string ARG_SDATE,string ARG_EDATE) RPCFUNC ALIAS FOR "SP_ROOM_ROTATION"
subroutine SP_DIAGOP_SUMMARY(string ARG_SDATE,string ARG_EDATE,string ARG_DEPT,string ARG_DG1,string ARG_DG2,string ARG_DG3,string ARG_DG4,string ARG_DG5,string ARG_OP1,string ARG_OP2,string ARG_OP3,string ARG_OP4,string ARG_OP5) RPCFUNC ALIAS FOR "SP_DIAGOP_SUMMARY"
subroutine SP_MIBI_DEPT(string ARG_SDATE,string ARG_EDATE) RPCFUNC ALIAS FOR "SP_MIBI_DEPT"
subroutine SP_MIBI_DOC(string ARG_SDATE,string ARG_EDATE) RPCFUNC ALIAS FOR "SP_MIBI_DOC"
subroutine SP_AGESTAT(string JUMINYY,string JUMIN,ref string AGE,ref double AGENO) RPCFUNC ALIAS FOR "SP_AGESTAT"
subroutine SP_JUBSU_INSERT(string HOSPNO_2,string DEPT_2,string DEPT_22,string BSDATE_2,datetime BSTIME_2,double EMERGY,string PATKIND,string PATTYPE_2,string DRCODE_2,double SPECIAL,string AGESTAT_1,string NIGHT_1,string CARD,string SABUN,string GADUNG,string RESERVE,string DCCODE,string CHOJAE,string GUBUN,string DEPTSTAT,string HUBUL,string PRT,double CONSULT,string REFER,string EXCEPTYN,string EXCEPTCODE,string JANGE,double TRANS,string ASSOCIATE,string PERSONID,string SELFSTAT,string PBONAME,string HANGJUMIN,string BUNUP,string DCSABUN,double MISUNABAMT,ref string ERRMSG) RPCFUNC ALIAS FOR "SP_JUBSU_INSERT"
subroutine SP_OSUNAB_RECHECK(string V_HOSPNO,string V_BSDATE,string V_DEPT) RPCFUNC ALIAS FOR "SP_OSUNAB_RECHECK"
subroutine SP_ANNUALREPORT_EQUIP(string V_MAGAMDATE) RPCFUNC ALIAS FOR "SP_ANNUALREPORT_EQUIP"
subroutine SP_ID_VARIFY(string P_ID,string P_GUBUN) RPCFUNC ALIAS FOR "~"MEGADB1~".~"SP_ID_VARIFY~""

end prototypes

on uo_transaction.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_transaction.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

