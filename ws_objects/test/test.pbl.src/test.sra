﻿$PBExportHeader$test.sra
$PBExportComments$Generated Application Object
forward
global type test from application
end type
global transaction sqlca
global dynamicdescriptionarea sqlda
global dynamicstagingarea sqlsa
global error error
global message message
end forward

global variables
/* c:\FatimaV12\config\ini */
String gs_ini

/* logon */
String gs_sabun, gs_dept, gs_drname, gs_drcode, gs_admindept, gs_admindept2
  
transaction mega1, mega2, mega3 
 
String gs_dn, gs_login, gs_passwd 

String glo_sabun,gs_today

String gs_ipaddress 

String gs_ip
end variables

global type test from application
string appname = "test"
string themepath = "C:\Program Files (x86)\Appeon\Shared\PowerBuilder\theme190"
string themename = "Do Not Use Themes"
long richtextedittype = 2
long richtexteditversion = 1
string richtexteditkey = ""
end type
global test test

type prototypes
FUNCTION INT HAN_TOGGLE( UINT hWnd, INTEGER Han_eng, INTEGER Jun_ban) LIBRARY "C:\FatimaV12\MGDLL\HAN_TOG.DLL"
FUNCTION boolean CopyFileA( Ref String filename, Ref String target, Boolean exists ) LIBRARY "kernel32.dll" alias for "CopyFileA;Ansi"
FUNCTION boolean CreateDirectoryA(ref string pathname, int sa) LIBRARY "Kernel32.dll" alias for "CreateDirectoryA;Ansi"
FUNCTION boolean CloseHandle(ulong w_handle) LIBRARY  "Kernel32.dll" ALIAS FOR "CloseHandle" 
FUNCTION ulong FindWindowA(ulong classname,string   windowname) LIBRARY "User32.dll"  ALIAS FOR "FindWindowA;Ansi" 

//ipaddress
Function integer GetIPAddress ( ref string buf, integer len ) library "C:\fatimav12\MGDLL\getmacip.dll" alias for "GetIPAddress;Ansi" 
Function integer GetMACAddress( ref string buf, integer len ) library "C:\fatimav12\MGDLL\getmacip.dll" alias for "GetMACAddress;Ansi" 

end prototypes

on test.create
appname="test"
message=create message
sqlca=create transaction
sqlda=create dynamicdescriptionarea
sqlsa=create dynamicstagingarea
error=create error
end on

on test.destroy
destroy(sqlca)
destroy(sqlda)
destroy(sqlsa)
destroy(error)
destroy(message)
end on

event open;integer li_er, li_hd, li_filenum
string ls_er, ls_hd, ls_newfile
string ls_deptcode
gs_ini ="C:\FatimaV19\config\"+ this.Appname+ ".ini"

con_mega(1, This.AppName)
con_mega(2, This.AppName)

IF MEGA1.SQLCode <> 0 THEN 
	Event Close()
	Return
End If
 
SQLCA = MEGA1

gs_ipaddress = space(100)

If GetIpAddress( gs_ipaddress, 100 ) = 1 Then
End if


gf_logon_msg(gs_ini, li_er, li_hd, gs_sabun, gs_drcode, gs_drname, gs_dept)

IF Upper(gs_sabun) = "ERROR" OR gs_sabun = "" OR IsNull(gs_sabun) THEN 
	Event Close()
	Return
End If

open(w_test)

IF FileExists(gs_ini) = False THEN
	li_FileNum = FileOpen( gs_ini, LineMode!, Write!, LockWrite!)
	FileClose(li_FileNum)
END IF

ls_newfile="C:\FatimaV19\Config\c"+gs_sabun+".ini"

IF FileExists(ls_newfile) = False THEN
	IF FileExists(gs_ini) THEN
		CopyFileA (gs_ini, ls_newfile, False)
	ELSE
		li_FileNum = FileOpen( ls_newfile, LineMode!, Write!, LockWrite!)
		FileClose(li_FileNum)
	END IF
END IF

gs_ini = ls_newfile

end event

event close;discon_mega(1)

halt close
end event

