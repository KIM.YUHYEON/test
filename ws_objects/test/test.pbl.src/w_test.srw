﻿$PBExportHeader$w_test.srw
forward
global type w_test from window
end type
type cb_3 from commandbutton within w_test
end type
type cb_2 from commandbutton within w_test
end type
type cb_1 from commandbutton within w_test
end type
type dw_list from datawindow within w_test
end type
end forward

global type w_test from window
integer width = 1774
integer height = 2640
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
dw_list dw_list
end type
global w_test w_test

on w_test.create
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_list=create dw_list
this.Control[]={this.cb_3,&
this.cb_2,&
this.cb_1,&
this.dw_list}
end on

on w_test.destroy
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_list)
end on

event open;dw_list.settransObject(mega1)

dw_list.reset()
dw_list.retrieve()

messagebox("",string(dw_list.rowcount())) 
messagebox("",string(dw_list.rowcount())) 
messagebox("",string(dw_list.rowcount())) 

end event

type cb_3 from commandbutton within w_test
integer x = 114
integer y = 20
integer width = 457
integer height = 132
integer taborder = 20
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "저장"
end type

event clicked;messagebox("저장","저장")
end event

type cb_2 from commandbutton within w_test
integer x = 672
integer y = 20
integer width = 457
integer height = 132
integer taborder = 20
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "추가"
end type

event clicked;messagebox("추가","추가")
end event

type cb_1 from commandbutton within w_test
integer x = 1147
integer y = 20
integer width = 457
integer height = 132
integer taborder = 10
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "종료"
end type

event clicked;close(parent)
end event

type dw_list from datawindow within w_test
integer x = 59
integer y = 160
integer width = 1490
integer height = 1704
integer taborder = 10
string title = "none"
string dataobject = "d_test"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

