﻿$PBExportHeader$w_mega_multirow_data.srw
$PBExportComments$Multi-Row DataWindow Ancestor
forward
global type w_mega_multirow_data from window
end type
type mgb_group2 from groupbox within w_mega_multirow_data
end type
type mpb_cancel from picturebutton within w_mega_multirow_data
end type
type mpb_run from picturebutton within w_mega_multirow_data
end type
type mdw_mega_print from datawindow within w_mega_multirow_data
end type
type mpb_preview from picturebutton within w_mega_multirow_data
end type
type mpb_excel from picturebutton within w_mega_multirow_data
end type
type mpb_search from picturebutton within w_mega_multirow_data
end type
type mpb_exit from picturebutton within w_mega_multirow_data
end type
type mpb_print from picturebutton within w_mega_multirow_data
end type
type mpb_save from picturebutton within w_mega_multirow_data
end type
type mpb_delete from picturebutton within w_mega_multirow_data
end type
type mpb_insert from picturebutton within w_mega_multirow_data
end type
type mdw_mega_data from datawindow within w_mega_multirow_data
end type
type mgb_group3 from groupbox within w_mega_multirow_data
end type
type mgb_group from groupbox within w_mega_multirow_data
end type
end forward

global type w_mega_multirow_data from window
integer width = 4649
integer height = 2924
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
long backcolor = 12632256
mgb_group2 mgb_group2
mpb_cancel mpb_cancel
mpb_run mpb_run
mdw_mega_print mdw_mega_print
mpb_preview mpb_preview
mpb_excel mpb_excel
mpb_search mpb_search
mpb_exit mpb_exit
mpb_print mpb_print
mpb_save mpb_save
mpb_delete mpb_delete
mpb_insert mpb_insert
mdw_mega_data mdw_mega_data
mgb_group3 mgb_group3
mgb_group mgb_group
end type
global w_mega_multirow_data w_mega_multirow_data

type variables
String  mis_fdate, mis_tdate, mis_fmonth, mis_tmonth, mis_fyear, mis_tyear
Integer mii_currentrow, mii_save, mii_print
end variables

event open;
This.X = 1
This.Y = 1

mdw_mega_data.SetTransObject(SQLCA)  

end event

event closequery;if gf_dwmodified_chk(mdw_mega_data) <> 1 then return 1
end event

on w_mega_multirow_data.create
this.mgb_group2=create mgb_group2
this.mpb_cancel=create mpb_cancel
this.mpb_run=create mpb_run
this.mdw_mega_print=create mdw_mega_print
this.mpb_preview=create mpb_preview
this.mpb_excel=create mpb_excel
this.mpb_search=create mpb_search
this.mpb_exit=create mpb_exit
this.mpb_print=create mpb_print
this.mpb_save=create mpb_save
this.mpb_delete=create mpb_delete
this.mpb_insert=create mpb_insert
this.mdw_mega_data=create mdw_mega_data
this.mgb_group3=create mgb_group3
this.mgb_group=create mgb_group
this.Control[]={this.mgb_group2,&
this.mpb_cancel,&
this.mpb_run,&
this.mdw_mega_print,&
this.mpb_preview,&
this.mpb_excel,&
this.mpb_search,&
this.mpb_exit,&
this.mpb_print,&
this.mpb_save,&
this.mpb_delete,&
this.mpb_insert,&
this.mdw_mega_data,&
this.mgb_group3,&
this.mgb_group}
end on

on w_mega_multirow_data.destroy
destroy(this.mgb_group2)
destroy(this.mpb_cancel)
destroy(this.mpb_run)
destroy(this.mdw_mega_print)
destroy(this.mpb_preview)
destroy(this.mpb_excel)
destroy(this.mpb_search)
destroy(this.mpb_exit)
destroy(this.mpb_print)
destroy(this.mpb_save)
destroy(this.mpb_delete)
destroy(this.mpb_insert)
destroy(this.mdw_mega_data)
destroy(this.mgb_group3)
destroy(this.mgb_group)
end on

type mgb_group2 from groupbox within w_mega_multirow_data
boolean visible = false
integer x = 1737
integer y = 36
integer width = 471
integer height = 168
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
borderstyle borderstyle = stylelowered!
end type

type mpb_cancel from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "취소"
boolean visible = false
integer x = 3794
integer y = 180
integer width = 302
integer height = 104
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
string picturename = "c:\fatimav19\icon\cancel_em.bmp"
string disabledname = "c:\fatimav19\icon\cancel_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_run from picturebutton within w_mega_multirow_data
event ue_start ( )
event ue_before ( )
event ue_end ( )
string tag = "실행"
boolean visible = false
integer x = 2894
integer y = 180
integer width = 302
integer height = 104
integer taborder = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "실행"
string picturename = "c:\fatimav19\icon\run_em.bmp"
string disabledname = "c:\fatimav19\icon\run_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mdw_mega_print from datawindow within w_mega_multirow_data
boolean visible = false
integer x = 32
integer y = 252
integer width = 672
integer height = 472
integer taborder = 70
boolean titlebar = true
string title = "mdw_mega_print"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type mpb_preview from picturebutton within w_mega_multirow_data
event ue_start ( )
event ue_before ( )
event ue_end ( )
string tag = "출력"
boolean visible = false
integer x = 3950
integer y = 284
integer width = 302
integer height = 104
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "출력"
string picturename = "c:\fatimav19\icon\preview_em.bmp"
string disabledname = "c:\fatimav19\icon\preview_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_preview")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_excel from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "엑셀"
boolean visible = false
integer x = 4114
integer y = 180
integer width = 302
integer height = 104
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "엑셀"
string picturename = "c:\fatimav19\icon\excel_em.bmp"
string disabledname = "c:\fatimav19\icon\excel_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_excel")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_search from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "조회"
boolean visible = false
integer x = 2743
integer y = 68
integer width = 302
integer height = 104
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "조회"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\search_em.bmp"
string disabledname = "c:\fatimav19\icon\search_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_retrieve")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_exit from picturebutton within w_mega_multirow_data
string tag = "종료"
integer x = 4251
integer y = 68
integer width = 302
integer height = 104
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\exit_em.bmp"
string disabledname = "c:\fatimav19\icon\exit_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;Close(Parent)

end event

type mpb_print from picturebutton within w_mega_multirow_data
event ue_start ( )
event ue_before ( )
event ue_end ( )
string tag = "출력"
integer x = 3950
integer y = 68
integer width = 302
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "출력"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\print_em.bmp"
string disabledname = "c:\fatimav19\icon\print_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_print")
end event

event ue_before;IF gf_messagebox("PRINTQ1") = 2 THEN 
	mii_print = 0
	RETURN
END IF
mii_print = 1
mdw_mega_print.Object.Data = mdw_mega_data.Object.Data
mdw_mega_print.AcceptText()
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_save from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "저장"
integer x = 3648
integer y = 68
integer width = 302
integer height = 104
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "저장"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\save_em.bmp"
string disabledname = "c:\fatimav19\icon\save_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_save")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_delete from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "삭제"
integer x = 3346
integer y = 68
integer width = 302
integer height = 104
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "삭제"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\delete_em.bmp"
string disabledname = "c:\fatimav19\icon\delete_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_delete")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mpb_insert from picturebutton within w_mega_multirow_data
event ue_before ( )
event ue_start ( )
event ue_end ( )
string tag = "추가"
integer x = 3045
integer y = 68
integer width = 302
integer height = 104
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "추가"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\insert_em.bmp"
string disabledname = "c:\fatimav19\icon\insert_dm.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event ue_start;mdw_mega_data.TriggerEvent("ue_insert")
end event

event clicked;This.TriggerEvent("ue_before")
This.TriggerEvent("ue_start")
This.TriggerEvent("ue_end")
end event

type mdw_mega_data from datawindow within w_mega_multirow_data
event ue_retrieve ( )
event ue_delete ( )
event ue_insert ( )
event ue_save ( )
event ue_print ( )
event ue_preview ( )
event ue_excel ( )
event ue_filter ( )
event ue_setdata ( integer row,  string dwoname )
event ue_enter pbm_dwnprocessenter
integer x = 23
integer y = 244
integer width = 4567
integer height = 2536
integer taborder = 60
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_delete;IF mii_currentrow = 0 OR mdw_mega_data.RowCount() < mii_currentrow THEN 
	MessageBox("알림","삭제할 자료를 선택해 주십시요")
	RETURN
END IF

IF gf_messagebox("DELETEQ1") = 1 THEN
	mdw_mega_data.deleterow(mii_currentrow)
END IF

end event

event ue_insert;IF mii_currentrow = 0 THEN
	mii_currentrow = mdw_mega_data.insertrow(0)
ELSE
	mii_currentrow = mdw_mega_data.insertrow(mii_currentrow )
END IF

mdw_mega_data.ScrollToRow(mii_currentrow)
mdw_mega_data.SetColumn(1)
mdw_mega_data.SetFocus()

end event

event ue_save;
IF gf_dwupdate(mdw_mega_data, TRUE, TRUE) = 1 THEN 
	mii_currentrow = 0
END IF
end event

event ue_print;IF mii_print = 0 THEN RETURN
mdw_mega_print.Print()
end event

event ue_preview();IF mdw_mega_data.RowCount() = 0 THEN
	MessageBox('알림','출력할 자료가 없습니다!')
	RETURN
END IF
mdw_mega_print.Object.Data = mdw_mega_data.Object.Data
mdw_mega_print.AcceptText()
OpenWithParm(w_print_view_popup, mdw_mega_print)
end event

event ue_excel;Boolean lb_exist
Integer value, li_ret
String  ls_txtname, ls_named

IF mdw_mega_data.RowCount() = 0 THEN 
	MessageBox('알림',"Excel File로 저장할 자료가 없습니다!")
	RETURN
END IF

IF gf_messagebox("EXCELQ1") = 2 THEN RETURN

IF GetFileSaveName("Select File", ls_txtname, ls_named, "EXCEL", "EXCEL Files (*.XLS),*.XLS") = 0 THEN RETURN

lb_exist = FileExists(ls_txtname)


IF lb_exist THEN 
	li_ret = MessageBox("Save", "같은 이름의 File이 있습니다!~r덮어 쓰시겠습니까?" + ls_txtname, Question!, YesNoCancel!, 1)
	IF li_ret = 1 THEN
	   mdw_mega_data.SaveAs(ls_txtname, EXCEL5! , TRUE)
	ELSEIF li_ret = 2 THEN
		
	ELSEIF li_ret = 3 THEN
		RETURN
	END IF
ELSE
	mdw_mega_data.SaveAs(ls_txtname, EXCEL5! , TRUE)
END IF

//IF 1 = MessageBox("Program 실행확인!","현재의 자료를 Excel 프로그램으로 보시겠습니까?", &
//                   Question!, YesNo!, 1) THEN
//						 
//	Run("C:\Program Files\Microsoft Office\Office\EXCEL.EXE " + ls_txtname , Maximized!)
//
//END IF

end event

event ue_setdata(integer row, string dwoname);string ls_key, ls_type
ls_key = this.Describe( dwoname + ".Key")
IF ls_key = "yes" THEN
	MessageBox('알림','정확한 값을 입력하십시요!', Exclamation!)
ELSE
	ls_type = mdw_mega_data.Describe( String(dwoname) + ".ColType")
	CHOOSE CASE leftA(ls_type, 5)
		CASE 'char('
			This.SetItem(row, dwoname, ' ')
		CASE 'numbe','decim','long','int','real','ulong'
			This.SetItem(row, dwoname, 0)
		CASE 'date'
			This.SetItem(row, dwoname, gf_sysdatetime())
		CASE 'datet'
			This.SetItem(row, dwoname, gf_sysdatetime())
		CASE ELSE 
			MessageBox('알림','정확한 값을 입력하십시요!', Exclamation!)
	END CHOOSE
END IF

This.ScrollToRow(row)
This.SetColumn(dwoname)
This.SetFocus()

end event

event type long ue_enter();Send(Handle(this), 256, 9, Long(0,0))
return 1
end event

event clicked;string ls_colname

IF row = 0 THEN
	ls_colname = dwo.name
	IF rightA(ls_colname, 2) = "_t" THEN
		This.SetSort(midA(ls_colname, 1, lenA(ls_colname) - 2) + " A")
		This.Sort()
	END IF
END IF

mii_currentrow = row
end event

event retrieveend;IF rowcount = 0 THEN
	OpenWithParm(w_dwupdate_msg, "S0040")  
END IF
end event

event itemchanged;IF data = '' OR IsNull(data) THEN
	POST EVENT ue_setdata(row, dwo.name)
END IF

end event

event itemerror;return gf_itemerror (this, row, dwo.name, data)

end event

type mgb_group3 from groupbox within w_mega_multirow_data
boolean visible = false
integer x = 2235
integer y = 32
integer width = 443
integer height = 172
integer taborder = 70
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
borderstyle borderstyle = stylelowered!
end type

type mgb_group from groupbox within w_mega_multirow_data
integer x = 23
integer width = 4567
integer height = 220
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
borderstyle borderstyle = stylelowered!
end type

