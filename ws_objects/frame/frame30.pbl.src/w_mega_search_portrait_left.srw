﻿$PBExportHeader$w_mega_search_portrait_left.srw
$PBExportComments$자료검색용 Window 세로 Form 좌측버튼
forward
global type w_mega_search_portrait_left from w_mega_search_landscape
end type
type gb_2 from groupbox within w_mega_search_portrait_left
end type
end forward

global type w_mega_search_portrait_left from w_mega_search_landscape
gb_2 gb_2
end type
global w_mega_search_portrait_left w_mega_search_portrait_left

on w_mega_search_portrait_left.create
int iCurrent
call super::create
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_2
end on

on w_mega_search_portrait_left.destroy
call super::destroy
destroy(this.gb_2)
end on

type mdw_mega_print from w_mega_search_landscape`mdw_mega_print within w_mega_search_portrait_left
integer x = 439
integer y = 256
end type

type mpb_preview from w_mega_search_landscape`mpb_preview within w_mega_search_portrait_left
end type

type mpb_excel from w_mega_search_landscape`mpb_excel within w_mega_search_portrait_left
end type

type mpb_search from w_mega_search_landscape`mpb_search within w_mega_search_portrait_left
end type

type mpb_exit from w_mega_search_landscape`mpb_exit within w_mega_search_portrait_left
integer x = 55
integer y = 800
boolean originalsize = false
end type

type mpb_print from w_mega_search_landscape`mpb_print within w_mega_search_portrait_left
integer x = 55
integer y = 676
boolean originalsize = false
end type

type mpb_save from w_mega_search_landscape`mpb_save within w_mega_search_portrait_left
integer x = 55
integer y = 552
boolean originalsize = false
end type

type mpb_delete from w_mega_search_landscape`mpb_delete within w_mega_search_portrait_left
integer x = 55
integer y = 428
boolean originalsize = false
end type

type mpb_insert from w_mega_search_landscape`mpb_insert within w_mega_search_portrait_left
integer x = 55
integer y = 304
end type

type mdw_mega_data from w_mega_search_landscape`mdw_mega_data within w_mega_search_portrait_left
integer x = 421
integer width = 4169
end type

type mgb_group from w_mega_search_landscape`mgb_group within w_mega_search_portrait_left
end type

type mst_date from w_mega_search_landscape`mst_date within w_mega_search_portrait_left
end type

type mem_fdate from w_mega_search_landscape`mem_fdate within w_mega_search_portrait_left
end type

type mst_arrow from w_mega_search_landscape`mst_arrow within w_mega_search_portrait_left
end type

type mem_tdate from w_mega_search_landscape`mem_tdate within w_mega_search_portrait_left
end type

type mst_hospno from w_mega_search_landscape`mst_hospno within w_mega_search_portrait_left
end type

type msle_hospno from w_mega_search_landscape`msle_hospno within w_mega_search_portrait_left
end type

type mst_name from w_mega_search_landscape`mst_name within w_mega_search_portrait_left
end type

type gb_2 from groupbox within w_mega_search_portrait_left
integer x = 23
integer y = 220
integer width = 370
integer height = 2560
integer taborder = 70
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
borderstyle borderstyle = stylelowered!
end type

