﻿$PBExportHeader$w_mega_multirow_dataright.srw
$PBExportComments$Multi-Row DataWindow Right
forward
global type w_mega_multirow_dataright from w_mega_multirow_data
end type
end forward

global type w_mega_multirow_dataright from w_mega_multirow_data
end type
global w_mega_multirow_dataright w_mega_multirow_dataright

on w_mega_multirow_dataright.create
call super::create
end on

on w_mega_multirow_dataright.destroy
call super::destroy
end on

event resize;call super::resize;mdw_mega_data.resize(This.workspacewidth() - mdw_mega_data.x, This.workspaceheight() - mdw_mega_data.y - 1)
end event

type mdw_mega_print from w_mega_multirow_data`mdw_mega_print within w_mega_multirow_dataright
integer x = 73
end type

type mpb_preview from w_mega_multirow_data`mpb_preview within w_mega_multirow_dataright
integer x = 4293
integer y = 580
end type

type mpb_excel from w_mega_multirow_data`mpb_excel within w_mega_multirow_dataright
integer x = 4293
integer y = 836
boolean originalsize = false
end type

type mpb_search from w_mega_multirow_data`mpb_search within w_mega_multirow_dataright
integer x = 4293
end type

type mpb_exit from w_mega_multirow_data`mpb_exit within w_mega_multirow_dataright
integer x = 4293
integer y = 580
boolean originalsize = false
end type

type mpb_print from w_mega_multirow_data`mpb_print within w_mega_multirow_dataright
integer x = 4293
integer y = 452
boolean originalsize = false
end type

type mpb_save from w_mega_multirow_data`mpb_save within w_mega_multirow_dataright
integer x = 4293
integer y = 324
boolean originalsize = false
end type

type mpb_delete from w_mega_multirow_data`mpb_delete within w_mega_multirow_dataright
integer x = 4293
integer y = 196
boolean originalsize = false
end type

type mpb_insert from w_mega_multirow_data`mpb_insert within w_mega_multirow_dataright
integer x = 4293
boolean originalsize = false
end type

type mdw_mega_data from w_mega_multirow_data`mdw_mega_data within w_mega_multirow_dataright
integer x = 41
integer y = 64
integer width = 4206
integer height = 2716
end type

type mgb_group from w_mega_multirow_data`mgb_group within w_mega_multirow_dataright
boolean visible = false
end type

