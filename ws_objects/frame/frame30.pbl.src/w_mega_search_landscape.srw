﻿$PBExportHeader$w_mega_search_landscape.srw
$PBExportComments$자료검색용 Window 가로 Form
forward
global type w_mega_search_landscape from w_mega_multirow_data
end type
type mst_date from statictext within w_mega_search_landscape
end type
type mem_fdate from editmask within w_mega_search_landscape
end type
type mst_arrow from statictext within w_mega_search_landscape
end type
type mem_tdate from editmask within w_mega_search_landscape
end type
type mst_hospno from statictext within w_mega_search_landscape
end type
type msle_hospno from singlelineedit within w_mega_search_landscape
end type
type mst_name from statictext within w_mega_search_landscape
end type
type mem_fmonth from editmask within w_mega_search_landscape
end type
type mem_tmonth from editmask within w_mega_search_landscape
end type
type mem_fyear from editmask within w_mega_search_landscape
end type
type mem_tyear from editmask within w_mega_search_landscape
end type
end forward

global type w_mega_search_landscape from w_mega_multirow_data
mst_date mst_date
mem_fdate mem_fdate
mst_arrow mst_arrow
mem_tdate mem_tdate
mst_hospno mst_hospno
msle_hospno msle_hospno
mst_name mst_name
mem_fmonth mem_fmonth
mem_tmonth mem_tmonth
mem_fyear mem_fyear
mem_tyear mem_tyear
end type
global w_mega_search_landscape w_mega_search_landscape

type variables
String mis_status
end variables

forward prototypes
public subroutine mwf_dw5years_title ()
public subroutine mwf_dwdaily_title ()
public subroutine mwf_dwmonth_title ()
end prototypes

public subroutine mwf_dw5years_title ();Integer i

mdw_mega_data.Modify("c1_t.Text = '" + mis_fyear + "'")
mdw_mega_print.Modify("c1_t.Text = '" + mis_fyear + "'")

FOR i = 1 TO 4
	
	mdw_mega_data.Modify("c" + String(i+1) + "_t.Text = '" + String(Integer(mis_fyear) + i) + "'")
	mdw_mega_print.Modify("c" + String(i+1) + "_t.Text = '" + String(Integer(mis_fyear) + i) + "'")
	
NEXT
end subroutine

public subroutine mwf_dwdaily_title ();String ls_end, ls_nextmonth
Integer i, li_end, li_month

SELECT TO_CHAR(TO_DATE(SUBSTR(:mis_fdate, 1, 6) || '01','YYYYMMDD') - 1, 'YYYYMMDD'),
       TO_CHAR(Add_months(TO_DATE(:mis_fdate,'YYYYMMDD'),1), 'YYYYMM')
INTO   :ls_end, :ls_nextmonth
FROM   Dual
USING  SQLCA;

li_end = Integer(rightA(ls_end,2))

FOR i = 1 TO li_end

	mdw_mega_data.Modify("c" + string(i) + "_t.Text = '" + String(li_month) + "'")
	mdw_mega_print.Modify("c" + string(i) + "_t.Text = '" + String(li_month) + "'")
	
NEXT

mdw_mega_data.Modify("c33_t.Text = '" + rightA(ls_nextmonth,2) + "월'")
mdw_mega_print.Modify("c33_t.Text = '" + rightA(ls_nextmonth,2) + "월'")

end subroutine

public subroutine mwf_dwmonth_title ();String ls_month, ls_year
Integer i, li_year, li_month

ls_year  = midA(mis_fmonth,3,2)
li_year  = Integer(ls_year)
li_month = Integer(rightA(mis_fmonth,2))

mdw_mega_data.Modify("c1_t.Text = '" + ls_year + '.' + String(li_month) + "'")
mdw_mega_print.Modify("c1_t.Text = '" + ls_year + '.' + String(li_month) + "'")

FOR i = 2 TO 12
	li_month = li_month + 1
	IF li_month = 13 THEN
		li_month = 1
		li_year = li_year + 1
		mdw_mega_data.Modify("c" + string(i) + "_t.Text = '" + String(li_year, "00") + "." + String(li_month) + "'")
		mdw_mega_print.Modify("c" + string(i) + "_t.Text = '" + String(li_year, "00") + "." + String(li_month) + "'")
	ELSE
		mdw_mega_data.Modify("c" + string(i) + "_t.Text = '" + String(li_month) + "'")
		mdw_mega_print.Modify("c" + string(i) + "_t.Text = '" + String(li_month) + "'")
	END IF
	
NEXT

mdw_mega_data.Modify("c13_t.Text = '" + String(Integer(leftA(mis_fmonth,4)) - 1) + "'")
mdw_mega_print.Modify("c13_t.Text = '" + String(Integer(leftA(mis_fmonth,4)) - 1) + "'")

end subroutine

on w_mega_search_landscape.create
int iCurrent
call super::create
this.mst_date=create mst_date
this.mem_fdate=create mem_fdate
this.mst_arrow=create mst_arrow
this.mem_tdate=create mem_tdate
this.mst_hospno=create mst_hospno
this.msle_hospno=create msle_hospno
this.mst_name=create mst_name
this.mem_fmonth=create mem_fmonth
this.mem_tmonth=create mem_tmonth
this.mem_fyear=create mem_fyear
this.mem_tyear=create mem_tyear
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.mst_date
this.Control[iCurrent+2]=this.mem_fdate
this.Control[iCurrent+3]=this.mst_arrow
this.Control[iCurrent+4]=this.mem_tdate
this.Control[iCurrent+5]=this.mst_hospno
this.Control[iCurrent+6]=this.msle_hospno
this.Control[iCurrent+7]=this.mst_name
this.Control[iCurrent+8]=this.mem_fmonth
this.Control[iCurrent+9]=this.mem_tmonth
this.Control[iCurrent+10]=this.mem_fyear
this.Control[iCurrent+11]=this.mem_tyear
end on

on w_mega_search_landscape.destroy
call super::destroy
destroy(this.mst_date)
destroy(this.mem_fdate)
destroy(this.mst_arrow)
destroy(this.mem_tdate)
destroy(this.mst_hospno)
destroy(this.msle_hospno)
destroy(this.mst_name)
destroy(this.mem_fmonth)
destroy(this.mem_tmonth)
destroy(this.mem_fyear)
destroy(this.mem_tyear)
end on

event open;call super::open;mis_fdate  = gf_sysdate()
mis_tdate  = mis_fdate
mis_fmonth = leftA(mis_fdate,6)
mis_tmonth = mis_fmonth
mis_fyear  = leftA(mis_fdate,4)
mis_tyear  = mis_fyear

IF mem_fdate.Visible = TRUE AND mem_fmonth.Visible = FALSE AND mem_fyear.Visible = FALSE THEN
	mem_fdate.Text = String(mis_fdate, '@@@@-@@-@@')
	mem_tdate.Text = String(mis_tdate, '@@@@-@@-@@')
ELSEIF mem_fdate.Visible = FALSE AND mem_fmonth.Visible = TRUE AND mem_fyear.Visible = FALSE THEN
	mst_date.Text = '검색월'  
	mem_fmonth.Text = String(mis_fmonth, '@@@@-@@')  
	mem_tmonth.Text = String(mis_tmonth, '@@@@-@@')  
ELSEIF mem_fdate.Visible = FALSE AND mem_fmonth.Visible = FALSE AND mem_fyear.Visible = TRUE THEN  
	mst_date.Text = '검색연도' 
	mem_fyear.Text = String(mis_fyear, '@@@@')
	mem_tyear.Text = String(mis_tyear, '@@@@')	
END IF
end event

type mgb_group2 from w_mega_multirow_data`mgb_group2 within w_mega_search_landscape
end type

type mpb_cancel from w_mega_multirow_data`mpb_cancel within w_mega_search_landscape
end type

type mpb_run from w_mega_multirow_data`mpb_run within w_mega_search_landscape
end type

type mdw_mega_print from w_mega_multirow_data`mdw_mega_print within w_mega_search_landscape
integer y = 380
end type

type mpb_preview from w_mega_multirow_data`mpb_preview within w_mega_search_landscape
end type

type mpb_excel from w_mega_multirow_data`mpb_excel within w_mega_search_landscape
end type

type mpb_search from w_mega_multirow_data`mpb_search within w_mega_search_landscape
integer width = 311
integer height = 112
end type

event mpb_search::ue_before;call super::ue_before;String ls_tmonth

IF mem_fdate.Visible = TRUE AND mem_tdate.Visible = TRUE THEN

	IF mis_fdate > mis_tdate THEN
		gf_messagebox("COMP1")
		mis_status = 'RETURN'
		RETURN
	END IF
	
END IF

IF mem_fmonth.Visible = TRUE AND mem_tmonth.Visible = TRUE THEN

	SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:mis_tmonth,'YYYYMM'), -12), 'YYYYMM')
	INTO :ls_tmonth
	from dual ;

	IF mis_fmonth > mis_tmonth THEN
		gf_messagebox("COMP1")
		mis_status = 'RETURN'
		RETURN
	ELSEIF (Long(mis_fmonth) < Long(ls_tmonth)) THEN
		gf_messagebox("COMP2")
		mis_status = 'RETURN'
		RETURN
	END IF
	
END IF

IF mem_fyear.Visible = TRUE AND mem_tyear.Visible = TRUE THEN

	IF mis_fyear > mis_tyear THEN
		gf_messagebox("COMP1")
		mis_status = 'RETURN'
		RETURN
	END IF
	
END IF

mis_status = ''
end event

type mpb_exit from w_mega_multirow_data`mpb_exit within w_mega_search_landscape
integer width = 311
integer height = 112
end type

type mpb_print from w_mega_multirow_data`mpb_print within w_mega_search_landscape
integer width = 311
integer height = 112
end type

type mpb_save from w_mega_multirow_data`mpb_save within w_mega_search_landscape
integer width = 311
integer height = 112
end type

type mpb_delete from w_mega_multirow_data`mpb_delete within w_mega_search_landscape
integer width = 311
integer height = 112
end type

type mpb_insert from w_mega_multirow_data`mpb_insert within w_mega_search_landscape
integer width = 311
integer height = 112
end type

type mdw_mega_data from w_mega_multirow_data`mdw_mega_data within w_mega_search_landscape
end type

event mdw_mega_data::ue_retrieve();call super::ue_retrieve;/*

검색일자에 대한 검색조건이 부적절할때 
mis_status 에 'RETURN' 을 셋팅한다.
따라서 Retrieve 하기 전에 체크한다.

mpb_search : ue_before Event 참조.

문의 : 김기석

*/


IF mis_status = 'RETURN' THEN RETURN


end event

type mgb_group3 from w_mega_multirow_data`mgb_group3 within w_mega_search_landscape
end type

type mgb_group from w_mega_multirow_data`mgb_group within w_mega_search_landscape
end type

type mst_date from statictext within w_mega_search_landscape
integer x = 64
integer y = 92
integer width = 347
integer height = 72
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
string text = "검색일자"
alignment alignment = right!
boolean focusrectangle = false
end type

type mem_fdate from editmask within w_mega_search_landscape
integer x = 434
integer y = 76
integer width = 462
integer height = 84
integer taborder = 90
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy-mm-dd"
boolean spin = true
double increment = 1
end type

event losefocus;mis_fdate = leftA(This.Text, 4) + midA(This.Text, 6, 2) + rightA(This.Text, 2)
end event

event rbuttondown;OpenWithParm( w_calendar, mis_fdate )

mis_fdate = Message.StringParm

DO WHILE (TRUE)

	IF mis_fdate > '' THEN
		This.Text = String(mis_fdate, '@@@@-@@-@@')
		EXIT
	ELSE
		MessageBox("알림","반드시 '검색일자'를 입력하십시요!")
		OpenWithParm( w_calendar, mis_fdate )
		mis_fdate = Message.StringParm
	END IF
LOOP






end event

type mst_arrow from statictext within w_mega_search_landscape
integer x = 928
integer y = 96
integer width = 64
integer height = 52
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
string text = "▶"
boolean focusrectangle = false
end type

type mem_tdate from editmask within w_mega_search_landscape
integer x = 1015
integer y = 76
integer width = 462
integer height = 84
integer taborder = 100
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy-mm-dd"
boolean spin = true
double increment = 1
end type

event losefocus;mis_tdate = leftA(This.Text, 4) + midA(This.Text, 6, 2) + rightA(This.Text, 2)
end event

event rbuttondown;OpenWithParm( w_calendar, mis_tdate )

mis_tdate = Message.StringParm

DO WHILE (TRUE)

	IF mis_tdate > '' THEN
		This.Text = String(mis_tdate, '@@@@-@@-@@')
		EXIT
	ELSE
		MessageBox("알림","반드시 '검색일자'를 입력하십시요!")
		OpenWithParm( w_calendar, mis_tdate )
		mis_tdate = Message.StringParm
	END IF
LOOP



end event

type mst_hospno from statictext within w_mega_search_landscape
integer x = 1687
integer y = 92
integer width = 283
integer height = 64
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
string text = "등록번호"
boolean focusrectangle = false
end type

type msle_hospno from singlelineedit within w_mega_search_landscape
integer x = 1966
integer y = 80
integer width = 421
integer height = 84
integer taborder = 110
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event modified;String ls_name

ls_name = gf_idmast(This.Text, 'name')

IF ls_name = '' OR IsNull(ls_name) THEN
	OpenWithParm( w_dwupdate_msg, "검색된 자료가 없습니다!")	
	RETURN
END IF

mst_name.Text = ls_name
end event

type mst_name from statictext within w_mega_search_landscape
integer x = 2395
integer y = 80
integer width = 421
integer height = 84
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type mem_fmonth from editmask within w_mega_search_landscape
boolean visible = false
integer x = 434
integer y = 164
integer width = 347
integer height = 92
integer taborder = 100
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy-mm"
boolean spin = true
double increment = 1
end type

event losefocus;mis_fmonth = leftA(This.Text, 4) + rightA(This.Text, 2)
end event

event rbuttondown;OpenWithParm( w_month, mis_fmonth )

mis_fmonth = Message.StringParm

DO WHILE (TRUE)

	IF mis_fmonth > '' THEN
		This.Text = String(mis_fmonth, '@@@@-@@')
		EXIT
	ELSE
		MessageBox("알림","반드시 '검색월'을 입력하셔야 합니다!")
		OpenWithParm( w_calendar, mis_fmonth )
		mis_fmonth = Message.StringParm
	END IF
LOOP

end event

type mem_tmonth from editmask within w_mega_search_landscape
boolean visible = false
integer x = 1015
integer y = 164
integer width = 347
integer height = 92
integer taborder = 110
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy-mm"
boolean spin = true
double increment = 1
end type

event rbuttondown;OpenWithParm( w_month, mis_tmonth )

mis_tmonth = Message.StringParm

DO WHILE (TRUE)

	IF mis_tmonth > '' THEN
		This.Text = String(mis_tmonth, '@@@@-@@')
		EXIT
	ELSE
		MessageBox("알림","반드시 '검색월'을 입력하셔야 합니다!")
		OpenWithParm( w_calendar, mis_tmonth )
		mis_tmonth = Message.StringParm
	END IF
LOOP

end event

event losefocus;mis_tmonth = leftA(This.Text, 4) + rightA(This.Text, 2)
end event

type mem_fyear from editmask within w_mega_search_landscape
boolean visible = false
integer x = 434
integer y = 264
integer width = 279
integer height = 92
integer taborder = 110
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
double increment = 1
end type

event losefocus;mis_fyear = This.Text
end event

type mem_tyear from editmask within w_mega_search_landscape
boolean visible = false
integer x = 1015
integer y = 264
integer width = 279
integer height = 92
integer taborder = 120
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
double increment = 1
end type

event losefocus;mis_tyear = This.Text
end event

