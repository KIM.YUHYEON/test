﻿$PBExportHeader$w_sys_save.srw
$PBExportComments$저장Ancestor
forward
global type w_sys_save from window
end type
type dw_1 from g_dw_gb_base within w_sys_save
end type
type pb_print from picturebutton within w_sys_save
end type
type pb_delete from picturebutton within w_sys_save
end type
type pb_insert from picturebutton within w_sys_save
end type
type pb_save from picturebutton within w_sys_save
end type
type pb_exit from picturebutton within w_sys_save
end type
end forward

global type w_sys_save from window
integer width = 2533
integer height = 1408
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
dw_1 dw_1
pb_print pb_print
pb_delete pb_delete
pb_insert pb_insert
pb_save pb_save
pb_exit pb_exit
end type
global w_sys_save w_sys_save

type variables
string is_update = ''
Integer ii_currentrow

end variables

on w_sys_save.create
this.dw_1=create dw_1
this.pb_print=create pb_print
this.pb_delete=create pb_delete
this.pb_insert=create pb_insert
this.pb_save=create pb_save
this.pb_exit=create pb_exit
this.Control[]={this.dw_1,&
this.pb_print,&
this.pb_delete,&
this.pb_insert,&
this.pb_save,&
this.pb_exit}
end on

on w_sys_save.destroy
destroy(this.dw_1)
destroy(this.pb_print)
destroy(this.pb_delete)
destroy(this.pb_insert)
destroy(this.pb_save)
destroy(this.pb_exit)
end on

event open;gf_center(this)

dw_1.SetTransObject(SQLCA)

end event

event closequery;if gf_dwmodified_chk(dw_1) <> 1 then return 1

end event

type dw_1 from g_dw_gb_base within w_sys_save
event ue_setdata ( integer row,  string dwoname )
event ue_enter pbm_dwnprocessenter
integer x = 50
integer y = 300
integer width = 2400
integer height = 964
integer taborder = 60
end type

event ue_setdata;string ls_key, ls_type
ls_key = this.Describe( dwoname + ".Key")
IF ls_key = "yes" THEN
	MessageBox('알림','정확한 값을 입력하십시요!', Exclamation!)
ELSE
	ls_type = dw_1.Describe( String(dwoname) + ".ColType")
	CHOOSE CASE leftA(ls_type, 5)
		CASE 'char('
			This.SetItem(row, dwoname, ' ')
		CASE 'numbe','decim','long','int','real','ulong'
			This.SetItem(row, dwoname, 0)
		CASE 'date'
			This.SetItem(row, dwoname, gf_sysdatetime())
		CASE 'datet'
			This.SetItem(row, dwoname, gf_sysdatetime())
		CASE ELSE 
			MessageBox('알림','정확한 값을 입력하십시요!', Exclamation!)
	END CHOOSE
END IF

This.ScrollToRow(row)
This.SetColumn(dwoname)
This.SetFocus()

end event

event ue_enter;Send(Handle(this), 256, 9, Long(0,0))
return 1

end event

event rowfocuschanged;ii_currentrow = currentrow

end event

event doubleclicked;string ls_colname

IF row = 0 THEN
	ls_colname = dwo.name
	IF rightA(ls_colname, 2) = "_t" THEN
		dw_1.SetSort(midA(ls_colname, 1, lenA(ls_colname) - 2) + " A")
		dw_1.Sort()
	END IF
END IF

end event

event rbuttondown;IF row < 1 THEN Return
this.ScrollToRow(row)
pb_delete.PostEvent(Clicked!)

end event

event retrieveend;IF rowcount = 0 THEN
   OpenWithParm(w_dwupdate_msg, 'S0040')
ELSE
	this.SetRowFocusIndicator(Hand!)
END IF

end event

event itemerror;call super::itemerror;return gf_itemerror (this, row, dwo.name, data)

end event

event itemchanged;call super::itemchanged;IF data = '' OR IsNull(data) THEN
	POST EVENT ue_setdata(row, dwo.name)
END IF

end event

type pb_print from picturebutton within w_sys_save
string tag = "출력"
integer x = 1961
integer y = 60
integer width = 219
integer height = 152
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "출력"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\print_el.bmp"
string disabledname = "c:\fatimav19\icon\print_dl.bmp"
end type

event clicked;OpenWithParm(w_print_view, dw_1)

end event

type pb_delete from picturebutton within w_sys_save
string tag = "삭제"
integer x = 1458
integer y = 60
integer width = 219
integer height = 152
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "삭제"
string picturename = "c:\fatimav19\icon\delete_el.bmp"
string disabledname = "c:\fatimav19\icon\delete_dl.bmp"
end type

event clicked;IF dw_1.GetRow() = 0 THEN Return

IF MessageBox(gf_msg("H0001"), gf_msg("Q0030"), Question!, YesNo!, 1) = 1 THEN
	dw_1.deleterow(dw_1.GetRow())
END IF

end event

type pb_insert from picturebutton within w_sys_save
string tag = "추가"
integer x = 1207
integer y = 60
integer width = 219
integer height = 152
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "추가"
string picturename = "c:\fatimav19\icon\insert_el.bmp"
string disabledname = "c:\fatimav19\icon\insert_dl.bmp"
end type

event clicked;integer li_row
IF dw_1.GetRow() = 0 THEN
	li_row = dw_1.insertrow(0)
ELSE
	li_row = dw_1.insertrow(dw_1.GetRow() +1 )
END IF

dw_1.SetFocus()
dw_1.SetColumn(1)
dw_1.ScrollToRow(li_row)

end event

type pb_save from picturebutton within w_sys_save
event ue_save ( )
string tag = "저장"
integer x = 1710
integer y = 60
integer width = 219
integer height = 152
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "저장"
string picturename = "c:\fatimav19\icon\save_el.bmp"
string disabledname = "c:\fatimav19\icon\save_dl.bmp"
end type

event ue_save;Integer Rtn

is_update = 'NO'

Rtn = gf_dwupdate(Parent.dw_1, TRUE, TRUE)  // Rtn 0:저장취소, 1:저장성공 -1:저장실폐
IF Rtn = 1 THEN
	OpenWithParm(w_dwupdate_msg, 'S0030')
	is_update = 'OK'
END IF

end event

event clicked;dw_1.AcceptText()
Post Event ue_save()

end event

type pb_exit from picturebutton within w_sys_save
string tag = "종료"
integer x = 2213
integer y = 60
integer width = 219
integer height = 152
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
string picturename = "c:\fatimav19\icon\exit_el.bmp"
string disabledname = "c:\fatimav19\icon\exit_dl.bmp"
end type

event clicked;Close(Parent)

end event

