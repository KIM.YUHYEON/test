﻿$PBExportHeader$uo_fatimalibrary.sru
forward
global type uo_fatimalibrary from nonvisualobject
end type
end forward

global type uo_fatimalibrary from nonvisualobject
end type
global uo_fatimalibrary uo_fatimalibrary

type prototypes
FUNCTION  INT HAN_TOGGLE( UINT hWnd, INTEGER Han_eng, INTEGER Jun_ban ) LIBRARY "c:\fatimav12\mgdll\HAN_TOG.DLL"
end prototypes

on uo_fatimalibrary.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_fatimalibrary.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

