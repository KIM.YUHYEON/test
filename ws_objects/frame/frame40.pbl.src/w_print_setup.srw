﻿$PBExportHeader$w_print_setup.srw
$PBExportComments$openwithparm(datawindow) 출력물 보기 없이 설정
forward
global type w_print_setup from window
end type
type p_cancel from picture within w_print_setup
end type
type p_printer from picture within w_print_setup
end type
type p_ok from picture within w_print_setup
end type
type dw_1 from datawindow within w_print_setup
end type
type st_2 from statictext within w_print_setup
end type
type em_copies from editmask within w_print_setup
end type
type em_zoom from editmask within w_print_setup
end type
type sle_range from singlelineedit within w_print_setup
end type
type rb_5 from radiobutton within w_print_setup
end type
type rb_current_page from radiobutton within w_print_setup
end type
type rb_all from radiobutton within w_print_setup
end type
type cbx_print_to_file from checkbox within w_print_setup
end type
type rb_port from radiobutton within w_print_setup
end type
type rb_land from radiobutton within w_print_setup
end type
type st_pages2 from statictext within w_print_setup
end type
type st_pages1 from statictext within w_print_setup
end type
type st_4 from statictext within w_print_setup
end type
type cb_3 from commandbutton within w_print_setup
end type
type cb_2 from commandbutton within w_print_setup
end type
type cb_1 from commandbutton within w_print_setup
end type
type gb_1 from groupbox within w_print_setup
end type
type gb_2 from groupbox within w_print_setup
end type
type gb_3 from groupbox within w_print_setup
end type
end forward

global type w_print_setup from window
integer width = 1271
integer height = 1200
windowtype windowtype = response!
long backcolor = 67108864
boolean clientedge = true
p_cancel p_cancel
p_printer p_printer
p_ok p_ok
dw_1 dw_1
st_2 st_2
em_copies em_copies
em_zoom em_zoom
sle_range sle_range
rb_5 rb_5
rb_current_page rb_current_page
rb_all rb_all
cbx_print_to_file cbx_print_to_file
rb_port rb_port
rb_land rb_land
st_pages2 st_pages2
st_pages1 st_pages1
st_4 st_4
cb_3 cb_3
cb_2 cb_2
cb_1 cb_1
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
global w_print_setup w_print_setup

type variables

end variables

event open;gf_center(w_print_setup)

datawindow dw_mega_print

dw_mega_print = Create datawindow

dw_mega_print.SetRedraw(FALSE)

/* structure return value */
dw_mega_print = Message.PowerObjectParm

dw_1.dataObject = dw_mega_print.dataObject
dw_1.SetTransObject(sqlca)
dw_1.Reset() 
dw_mega_print.ShareData(dw_1) 

dw_1.modify("datawindow.print.Preview = YES")

em_zoom.Text = String(dw_mega_print.Object.datawindow.zoom)

IF dw_mega_print.Object.DataWindow.Print.Orientation = '2' THEN
	rb_land.Checked = TRUE
ELSE
	rb_port.Checked = TRUE
END IF
end event

on w_print_setup.create
this.p_cancel=create p_cancel
this.p_printer=create p_printer
this.p_ok=create p_ok
this.dw_1=create dw_1
this.st_2=create st_2
this.em_copies=create em_copies
this.em_zoom=create em_zoom
this.sle_range=create sle_range
this.rb_5=create rb_5
this.rb_current_page=create rb_current_page
this.rb_all=create rb_all
this.cbx_print_to_file=create cbx_print_to_file
this.rb_port=create rb_port
this.rb_land=create rb_land
this.st_pages2=create st_pages2
this.st_pages1=create st_pages1
this.st_4=create st_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.cb_1=create cb_1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
this.Control[]={this.p_cancel,&
this.p_printer,&
this.p_ok,&
this.dw_1,&
this.st_2,&
this.em_copies,&
this.em_zoom,&
this.sle_range,&
this.rb_5,&
this.rb_current_page,&
this.rb_all,&
this.cbx_print_to_file,&
this.rb_port,&
this.rb_land,&
this.st_pages2,&
this.st_pages1,&
this.st_4,&
this.cb_3,&
this.cb_2,&
this.cb_1,&
this.gb_1,&
this.gb_2,&
this.gb_3}
end on

on w_print_setup.destroy
destroy(this.p_cancel)
destroy(this.p_printer)
destroy(this.p_ok)
destroy(this.dw_1)
destroy(this.st_2)
destroy(this.em_copies)
destroy(this.em_zoom)
destroy(this.sle_range)
destroy(this.rb_5)
destroy(this.rb_current_page)
destroy(this.rb_all)
destroy(this.cbx_print_to_file)
destroy(this.rb_port)
destroy(this.rb_land)
destroy(this.st_pages2)
destroy(this.st_pages1)
destroy(this.st_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

type p_cancel from picture within w_print_setup
integer x = 823
integer y = 324
integer width = 82
integer height = 72
boolean enabled = false
boolean originalsize = true
string picturename = "c:\fatimav19\icon\cancel_es.bmp"
boolean focusrectangle = false
end type

type p_printer from picture within w_print_setup
integer x = 823
integer y = 200
integer width = 82
integer height = 72
boolean enabled = false
boolean originalsize = true
string picturename = "c:\fatimav19\icon\print_es.bmp"
boolean focusrectangle = false
end type

type p_ok from picture within w_print_setup
integer x = 823
integer y = 72
integer width = 82
integer height = 72
boolean enabled = false
boolean originalsize = true
string picturename = "c:\fatimav19\icon\app_es.bmp"
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_print_setup
integer x = 2135
integer y = 496
integer width = 411
integer height = 432
integer taborder = 50
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_print_setup
integer x = 78
integer y = 200
integer width = 347
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Print Zoom"
boolean focusrectangle = false
end type

type em_copies from editmask within w_print_setup
integer x = 434
integer y = 96
integer width = 288
integer height = 84
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
double increment = 1
string minmax = "1~~100"
end type

type em_zoom from editmask within w_print_setup
event ue_change pbm_enchange
event ue_rezoom pbm_custom01
integer x = 434
integer y = 192
integer width = 288
integer height = 84
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "100"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
double increment = 1
string minmax = "50~~200"
end type

event ue_change;//This ue_change event will be called any time another radio button is pressed. This is 
//a common area the zoom size is adjusted each time.

//When viewing a datawindow w/labels, the datawindow is automatically placed in print
//preview mode.  This will change the zoom size of the datawindow in print preiew mode.

If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.Object.Datawindow.zoom = Integer(this.text)
End If

//rb_custom.checked = true

end event

event ue_rezoom;If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.Object.datawindow.zoom = Integer(this.text)
End If
end event

event modified;this.PostEvent("ue_change")
end event

type sle_range from singlelineedit within w_print_setup
integer x = 411
integer y = 884
integer width = 727
integer height = 92
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type rb_5 from radiobutton within w_print_setup
integer x = 91
integer y = 888
integer width = 320
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Pages :"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,0,0)
	st_pages2.TextColor = rgb(128,0,0)
	sle_range.BackColor = rgb(255,255,255)
	sle_range.Enabled = TRUE
	sle_range.SetFocus()
end if
//wf_page_range(this)
end event

type rb_current_page from radiobutton within w_print_setup
integer x = 91
integer y = 792
integer width = 466
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Current Page"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,128,128)
	st_pages2.TextColor = rgb(128,128,128)
	sle_range.BackColor = rgb(192,192,192)
	sle_range.Enabled = FALSE
end if
//wf_page_range(this)
end event

type rb_all from radiobutton within w_print_setup
integer x = 91
integer y = 712
integer width = 169
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "All"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,128,128)
	st_pages2.TextColor = rgb(128,128,128)
	sle_range.BackColor = rgb(192,192,192)
	sle_range.Enabled = FALSE
end if
//wf_page_range(this)
end event

type cbx_print_to_file from checkbox within w_print_setup
integer x = 786
integer y = 524
integer width = 421
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Print to File"
borderstyle borderstyle = stylelowered!
end type

type rb_port from radiobutton within w_print_setup
integer x = 137
integer y = 468
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Portrait"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	dw_1.Object.datawindow.Print.orientation = 2
end if
end event

type rb_land from radiobutton within w_print_setup
integer x = 137
integer y = 384
integer width = 402
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Landscape"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	dw_1.Object.datawindow.Print.orientation = 1
end if
end event

type st_pages2 from statictext within w_print_setup
integer x = 219
integer y = 1056
integer width = 370
integer height = 56
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ex. 1, 3, 5-10"
boolean focusrectangle = false
end type

type st_pages1 from statictext within w_print_setup
integer x = 219
integer y = 992
integer width = 933
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Input page number or page ranges"
boolean focusrectangle = false
end type

type st_4 from statictext within w_print_setup
integer x = 78
integer y = 108
integer width = 247
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Copies :"
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_print_setup
integer x = 805
integer y = 308
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "    &Cancel"
end type

event clicked;Close(Parent)
end event

type cb_2 from commandbutton within w_print_setup
integer x = 805
integer y = 180
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "     &Printer"
end type

event clicked;//Clicked script for cb_pr_setup

if PrintSetup( ) = -1 then
	MessageBox("Error!","PrintSetup Failed")
	Return
end if
Parent.Title = dw_1.Describe('datawindow.printer')
end event

type cb_1 from commandbutton within w_print_setup
integer x = 805
integer y = 52
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "      &OK"
end type

event clicked;//if IsValid(w_print) then
	string	ls_temp, ls_command, ls_path, ls_filename
	long		row 
	int		value
	
	if rb_all.Checked then  // All?
		ls_temp = ''
	elseif rb_current_page.Checked then// Current page?
		row = dw_1.getrow()
		ls_temp = dw_1.Describe("evaluate('page()',"+string(row)+")")
	else // A range?
		ls_temp = sle_range.text
		IF lenA(ls_temp) = 0 THEN
			MessageBox(	"Print Range - Pages:", &
							"Page range must be entered for this option.", &
							 Exclamation!)
			sle_range.SetFocus()
			RETURN
		END IF
	end if

	IF lenA(ls_temp) > 0 THEN ls_command = ls_command +  " datawindow.print.page.range = '"+ls_temp+"'"
	
	// Number of copies ?
	IF lenA(em_copies.text) > 0 THEN ls_command = ls_command +  " datawindow.print.copies = "+em_copies.text
	
	//화일 프린트?
	IF cbx_print_to_file.checked THEN 	// Print to file ?
		value = dw_1.saveas()
		Return
	END IF
	
	ls_temp = dw_1.Modify(ls_command)
	IF lenA(ls_temp) > 0 THEN // if error THEN display 
		MessageBox('Error Setting Print Options','Error message = ' + ls_temp + '~r~nls_command = ' + ls_command)
		RETURN
	END IF
	
	dw_1.Print( )
	Close(Parent)
//end if	

end event

type gb_1 from groupbox within w_print_setup
integer x = 64
integer y = 300
integer width = 654
integer height = 264
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "[ Paper Orientation ]"
borderstyle borderstyle = stylelowered!
end type

type gb_2 from groupbox within w_print_setup
integer x = 18
integer y = 616
integer width = 1189
integer height = 528
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "[ Paper Range ]"
borderstyle borderstyle = stylelowered!
end type

type gb_3 from groupbox within w_print_setup
integer x = 18
integer y = 16
integer width = 750
integer height = 584
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "[ Print Option ]"
borderstyle borderstyle = stylelowered!
end type

