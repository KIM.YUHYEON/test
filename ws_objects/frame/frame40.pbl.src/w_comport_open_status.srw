﻿$PBExportHeader$w_comport_open_status.srw
forward
global type w_comport_open_status from window
end type
end forward

global type w_comport_open_status from window
boolean visible = false
integer width = 2533
integer height = 1408
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
end type
global w_comport_open_status w_comport_open_status

type prototypes
Function Long CreateFileA(ref string lpszName,long fdwAccess,long fdwShareMode,long lpsa,long fdwCreate,long fdwAttrsAndFlags,long hTemplateFile ) Library "kernel32.dll" alias for "CreateFileA;Ansi"
Function Boolean CloseHandle(Long hObject ) Library "kernel32.dll"

end prototypes

forward prototypes
public function boolean wf_comport_open_status (string comport)
end prototypes

public function boolean wf_comport_open_status (string comport);/* COM Port가 Open 가능한지 테스트 하는 Function */
CONSTANT			Long		GENERIC_READ = 2147483648
CONSTANT			Long		GENERIC_WRITE = 1073741824
CONSTANT			Long		SHARE_MODE = 0
CONSTANT			Long		OPEN_EXISTING = 3
CONSTANT			Long		FILE_FLAG_OVERLAPPED = 1073741824

String			ls_port
Integer			li_FileNum
Long				ll_com

ls_port = Upper(comport)

ll_com = CreateFileA(ls_port,GENERIC_READ + GENERIC_WRITE,SHARE_MODE,0,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,0)

IF ll_com < 0 THEN Return False

CloseHandle(ll_com)

Return True

end function

event open;///* COM Port가 Open 가능한지 테스트 하는 Function */
//CONSTANT			Long		GENERIC_READ = 2147483648
//CONSTANT			Long		GENERIC_WRITE = 1073741824
//CONSTANT			Long		SHARE_MODE = 0
//CONSTANT			Long		OPEN_EXISTING = 3
//CONSTANT			Long		FILE_FLAG_OVERLAPPED = 1073741824
//
//st_dcb 			lst_dcb
//String			ls_port
//Integer			li_FileNum
//Long				ll_com
//
//ls_port = Upper(comport)
//
//ll_com = CreateFileA(ls_port,GENERIC_READ + GENERIC_WRITE,SHARE_MODE,0,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,0)
//
//IF ll_com < 0 THEN Return False
//
//CloseHandle(ll_com)
//
//Return True
//
end event

on w_comport_open_status.create
end on

on w_comport_open_status.destroy
end on

