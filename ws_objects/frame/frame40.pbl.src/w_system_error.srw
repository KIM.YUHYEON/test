﻿$PBExportHeader$w_system_error.srw
$PBExportComments$시스템 에러를 출력
forward
global type w_system_error from window
end type
type dw_error from datawindow within w_system_error
end type
type cb_print from commandbutton within w_system_error
end type
type cb_exit from commandbutton within w_system_error
end type
type cb_continue from commandbutton within w_system_error
end type
end forward

global type w_system_error from window
integer x = 379
integer y = 428
integer width = 2245
integer height = 1880
boolean titlebar = true
string title = "System Error"
windowtype windowtype = response!
long backcolor = 76585128
toolbaralignment toolbaralignment = alignatleft!
dw_error dw_error
cb_print cb_print
cb_exit cb_exit
cb_continue cb_continue
end type
global w_system_error w_system_error

type variables
String is_errmessage
end variables

on w_system_error.create
this.dw_error=create dw_error
this.cb_print=create cb_print
this.cb_exit=create cb_exit
this.cb_continue=create cb_continue
this.Control[]={this.dw_error,&
this.cb_print,&
this.cb_exit,&
this.cb_continue}
end on

on w_system_error.destroy
destroy(this.dw_error)
destroy(this.cb_print)
destroy(this.cb_exit)
destroy(this.cb_continue)
end on

event open;dw_error.insertrow (0)

dw_error.setitem (1,"errornum",string(error.number))
dw_error.setitem (1,"message" ,error.text+'('+is_errmessage+')')
dw_error.setitem (1,"where"   ,error.windowmenu)
dw_error.setitem (1,"object"  ,error.object)
dw_error.setitem (1,"event"   ,error.objectevent)
dw_error.setitem (1,"line"    ,string(error.line))

dw_error.Setcolumn( "to_programmer" )
dw_error.Setfocus()


//dw_error.print()
end event

type dw_error from datawindow within w_system_error
integer x = 55
integer y = 36
integer width = 2117
integer height = 1536
integer taborder = 10
string dataobject = "d_system_error"
borderstyle borderstyle = stylelowered!
end type

type cb_print from commandbutton within w_system_error
integer x = 69
integer y = 1612
integer width = 553
integer height = 156
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "굴림"
string text = "출력 후 종료"
end type

event clicked;dw_error.AcceptText()
dw_error.print()

Halt Close 
end event

type cb_exit from commandbutton within w_system_error
integer x = 1275
integer y = 1612
integer width = 667
integer height = 156
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "굴림"
string text = "프로그램 종료"
end type

event clicked;
halt close
end event

type cb_continue from commandbutton within w_system_error
boolean visible = false
integer x = 859
integer y = 1396
integer width = 430
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Continue"
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_continue
//
// Purpose:
// 			Closes w_system_error
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

close(parent)
end on

