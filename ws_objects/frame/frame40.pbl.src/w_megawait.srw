﻿$PBExportHeader$w_megawait.srw
forward
global type w_megawait from window
end type
type dw_1 from datawindow within w_megawait
end type
end forward

global type w_megawait from window
integer x = 1486
integer y = 512
integer width = 2272
integer height = 724
windowtype windowtype = popup!
long backcolor = 23875396
string pointer = "HourGlass!"
dw_1 dw_1
end type
global w_megawait w_megawait

type prototypes
FUNCTION  Long GetSystemMetrics( Long nindex ) LIBRARY "User32.dll"

end prototypes

type variables

end variables

event open;Long		ll_x
Integer	li_second

ll_x = GetSystemMetrics(0)

ll_x = ll_x - UnitsToPixels(This.Width, XUnitsToPixels!)

ll_x = PixelsToUnits(ll_x, XPixelsToUnits!)

This.x = ll_x / 2

li_second = Integer(Message.StringParm)

IF li_second > 0 THEN Timer(li_second)

end event

on w_megawait.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on w_megawait.destroy
destroy(this.dw_1)
end on

event timer;Close (This)
end event

type dw_1 from datawindow within w_megawait
integer x = 352
integer y = 212
integer width = 1906
integer height = 376
integer taborder = 1
string dataobject = "d_sys_megawait"
boolean border = false
boolean livescroll = true
end type

