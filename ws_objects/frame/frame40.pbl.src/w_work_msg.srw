﻿$PBExportHeader$w_work_msg.srw
$PBExportComments$작업 메시지
forward
global type w_work_msg from window
end type
type st_message from statictext within w_work_msg
end type
end forward

global type w_work_msg from window
integer x = 951
integer y = 1200
integer width = 2944
integer height = 120
windowtype windowtype = popup!
long backcolor = 1090519039
st_message st_message
end type
global w_work_msg w_work_msg

on w_work_msg.create
this.st_message=create st_message
this.Control[]={this.st_message}
end on

on w_work_msg.destroy
destroy(this.st_message)
end on

event open;String ls_message

ls_message = Message.StringParm

st_message.Text = ls_message

Timer(2)
end event

event timer;Close(w_work_msg)
end event

type st_message from statictext within w_work_msg
integer x = 238
integer y = 16
integer width = 2437
integer height = 84
integer textsize = -14
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 1090519039
string text = "저장 되었습니다!"
alignment alignment = center!
boolean focusrectangle = false
end type

