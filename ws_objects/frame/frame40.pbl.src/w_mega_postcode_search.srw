﻿$PBExportHeader$w_mega_postcode_search.srw
$PBExportComments$우편번호 찾기
forward
global type w_mega_postcode_search from window
end type
type pb_search from picturebutton within w_mega_postcode_search
end type
type pb_input from picturebutton within w_mega_postcode_search
end type
type pb_exit from picturebutton within w_mega_postcode_search
end type
type st_1 from statictext within w_mega_postcode_search
end type
type sle_postname from singlelineedit within w_mega_postcode_search
end type
type dw_mega_postcode from datawindow within w_mega_postcode_search
end type
end forward

global type w_mega_postcode_search from window
integer width = 2802
integer height = 1688
boolean titlebar = true
string title = "우편번호 찾기"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
pb_search pb_search
pb_input pb_input
pb_exit pb_exit
st_1 st_1
sle_postname sle_postname
dw_mega_postcode dw_mega_postcode
end type
global w_mega_postcode_search w_mega_postcode_search

type variables
Integer ii_row
end variables

on w_mega_postcode_search.create
this.pb_search=create pb_search
this.pb_input=create pb_input
this.pb_exit=create pb_exit
this.st_1=create st_1
this.sle_postname=create sle_postname
this.dw_mega_postcode=create dw_mega_postcode
this.Control[]={this.pb_search,&
this.pb_input,&
this.pb_exit,&
this.st_1,&
this.sle_postname,&
this.dw_mega_postcode}
end on

on w_mega_postcode_search.destroy
destroy(this.pb_search)
destroy(this.pb_input)
destroy(this.pb_exit)
destroy(this.st_1)
destroy(this.sle_postname)
destroy(this.dw_mega_postcode)
end on

event open;String ls_message

dw_mega_postcode.SetTransObject(SQLCA)

ls_message = Message.StringParm

IF IsNull(ls_message) OR ls_message = '' THEN
	sle_postname.SetFocus()
	RETURN
ELSEIF IsNumber(ls_message) THEN
	dw_mega_postcode.Retrieve(ls_message + '%', '%')
ELSE
	dw_mega_postcode.Retrieve('%', '%' + ls_message + '%')
END IF


end event

type pb_search from picturebutton within w_mega_postcode_search
integer x = 1856
integer y = 36
integer width = 302
integer height = 104
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "조회"
string picturename = "c:\fatimav19\icon\search_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;IF lenA(sle_postname.Text) < 4 THEN
	MessageBox("알림","두 글자 이상 입력하십시요!", Exclamation!)
	sle_postname.SelectText(1, lenA(sle_postname.Text))
	sle_postname.SetFocus()
	RETURN
END IF

dw_mega_postcode.Retrieve('%', '%' + sle_postname.Text + '%')
end event

type pb_input from picturebutton within w_mega_postcode_search
integer x = 2158
integer y = 36
integer width = 302
integer height = 104
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "입력"
string picturename = "c:\fatimav19\icon\insert_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;String  ls_postcode, ls_postname

IF ii_row = 0 THEN
	MessageBox("알림","지역명칭을 선택해 주십시요!", StopSign!)
	RETURN
END IF

ls_postcode = dw_mega_postcode.Object.postcode[ii_row]
ls_postname = dw_mega_postcode.Object.postname[ii_row]

CloseWithReturn( Parent, ls_postcode + ls_postname )
end event

type pb_exit from picturebutton within w_mega_postcode_search
integer x = 2459
integer y = 36
integer width = 311
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
boolean originalsize = true
string picturename = "c:\fatimav19\icon\exit_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;CloseWithReturn(Parent, "None")
end event

type st_1 from statictext within w_mega_postcode_search
integer x = 82
integer y = 60
integer width = 302
integer height = 52
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "지역명칭:"
boolean focusrectangle = false
end type

type sle_postname from singlelineedit within w_mega_postcode_search
event ue_enterkey pbm_keydown
integer x = 389
integer y = 44
integer width = 901
integer height = 92
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event ue_enterkey;IF KeyDown(KeyEnter!) THEN
	pb_search.TriggerEvent(Clicked!)
END IF
end event

event modified;This.TriggerEvent("ue_enterkey")
end event

type dw_mega_postcode from datawindow within w_mega_postcode_search
event ue_keydown pbm_dwnkey
integer x = 23
integer y = 164
integer width = 2743
integer height = 1416
integer taborder = 10
string title = "none"
string dataobject = "d_mega_postcode_search"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_keydown;Integer li_totrow

li_totrow = This.RowCount()

CHOOSE CASE key
	CASE KeyUpArrow!
		IF ii_row = 0 OR ii_row = 1 THEN RETURN
		This.SelectRow(0,FALSE)
		This.SelectRow(ii_row - 1, TRUE)
		ii_row = ii_row - 1
		
	CASE KeyDownArrow!
		IF ii_row = 0 OR ii_row = li_totrow THEN RETURN
		This.SelectRow(0,FALSE)
		This.SelectRow(ii_row + 1, TRUE)
		ii_row = ii_row + 1

	CASE KeyEnter!
		pb_input.TriggerEvent(Clicked!)
		
END CHOOSE

end event

event clicked;string ls_colname

IF row = 0 THEN
	ii_row = 0
	This.SelectRow(0,FALSE)
	ls_colname = dwo.name
	IF rightA(ls_colname, 2) = "_t" THEN
		This.SetSort(midA(ls_colname, 1, lenA(ls_colname) - 2) + " A")
		This.Sort()
	END IF
END IF

IF row > 0 THEN
	ii_row = row
	This.SelectRow(0,FALSE)
	This.SelectRow(row,TRUE)
END IF
end event

event retrieveend;IF rowcount = 0 THEN
	ii_row = 0
	OpenWithParm(w_dwupdate_msg, "S0040")  
	RETURN
END IF

ii_row = 1
This.SelectRow(0,FALSE)
This.SelectRow(1,TRUE)
This.ScrollToRow(1)
This.SetFocus()
end event

event doubleclicked;String ls_postcode, ls_postname

IF row = 0 THEN RETURN

ls_postcode = This.Object.postcode[row]
ls_postname = This.Object.postname[row]

CloseWithReturn( Parent, ls_postcode + ls_postname )
end event

