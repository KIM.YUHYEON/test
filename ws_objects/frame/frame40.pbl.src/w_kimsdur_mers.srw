﻿$PBExportHeader$w_kimsdur_mers.srw
forward
global type w_kimsdur_mers from window
end type
type cb_exit from commandbutton within w_kimsdur_mers
end type
type ole_durginfo from olecustomcontrol within w_kimsdur_mers
end type
end forward

global type w_kimsdur_mers from window
integer width = 3886
integer height = 964
boolean titlebar = true
windowtype windowtype = response!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_exit cb_exit
ole_durginfo ole_durginfo
end type
global w_kimsdur_mers w_kimsdur_mers

type variables
String	is_kims_durip,	is_dur_result
end variables

on w_kimsdur_mers.create
this.cb_exit=create cb_exit
this.ole_durginfo=create ole_durginfo
this.Control[]={this.cb_exit,&
this.ole_durginfo}
end on

on w_kimsdur_mers.destroy
destroy(this.cb_exit)
destroy(this.ole_durginfo)
end on

event open;String  ls_text, ls_text1, ls_text2, ls_text3, ls_temp, ls_io, ls_kiosk, ls_orderno
Integer ll_ret, ll_durcount, ll_flag
OLEObject ole1

is_kims_durip = gf_hospital_data('Kims_server')

ll_flag = 0
is_dur_result = '0'

ole1    = CREATE OLEObject

IF ll_flag = 0 THEN
	ll_ret = ole1.ConnectToNewObject("KIMSPOCCom.MedicalInfo") 
ELSE
	ll_ret = ole1.ConnectToNewObject("KIMSPOC.MedicalInfo") 
END IF

ole1.Connect(is_kims_durip, 'PACWPH', '')
ole1.Timeout = 5

ls_temp      = message.StringParm

ls_kiosk     = Mid(ls_temp, 1, Pos(ls_temp, '|') -1)
ls_temp      = Mid(ls_temp, Pos(ls_temp, '|') + 1)

ls_orderno   = Mid(ls_temp, 1, Pos(ls_temp, '|') -1)
ls_temp      = Mid(ls_temp, Pos(ls_temp, '|') + 1)

ls_io        = Mid(ls_temp, 1, Pos(ls_temp, '|') -1)
ls_temp      = Mid(ls_temp, Pos(ls_temp, '|') + 1)

ls_text      = Mid(ls_temp, 1, Len(ls_temp) - 1)

//ls_kiosk     = Left(ls_text, 1)
//ls_io        = Mid(ls_text, 2, 1)
//ls_text      = mid(ls_text, 3, len(ls_text) - 1)
//is_text = ls_text

//integer li_FileNum
//li_FileNum = FileOpen("C:\DUR_XML.XML", LineMode!, Write!, LockWrite!, Append!)
//FileWrite(li_FileNum, ls_text)

//is_durorderno   = gs_orderno

/*ls_text1    = ole1.GetAlertInfo("NHIDurAlert", ls_text, 1, 5)

select Replace(:ls_text1,'|','') into :ls_text2 from dual using mega1;

if ls_text1 = '0|0|0|0|0|0|0|0|0|0|2|0|0|0|0|0|0|0|0|0|0|0|0' then //임부금기2등급
	is_dur_result = '0'
	Close(This)
	return
end if*/

ll_durcount = ole1.CheckAlert("NHIDurAlert", ls_text)

is_dur_result = String(ll_durcount)

//ls_text2    = ole1.CallPOCFunction(ls_text1)
if ll_durcount < 0 then
	DESTROY ole1
	
	CloseWithReturn(This, is_dur_result)	
elseif ll_durcount = 0 then
	is_dur_result = '0'
	//ole1.SaveRxData(ls_text)
	DESTROY ole1
	
	CloseWithReturn(This, is_dur_result)
else
	IF ls_kiosk = 'K' THEN
		//ole1.SaveRxData(ls_text)
		DESTROY ole1
		
		CloseWithReturn(This,is_dur_result)
	ELSE
//		ole_durginfo.object.navigate('http://' + is_kims_durip + '/Page.aspx?MenuID=RxReview&OrderNo='+ls_orderno+'&Opt=4095')		
//		ole_durginfo.object.navigate('http://' + is_kims_durip + '/Page.aspx?MenuID=RxReview&OrderNo='+ls_orderno)
		gf_kims_navigate('6',ls_orderno, ole_durginfo)		
	END IF
end if

DESTROY ole1
end event

type cb_exit from commandbutton within w_kimsdur_mers
integer x = 3438
integer y = 12
integer width = 416
integer height = 100
integer taborder = 40
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
end type

event clicked;CloseWithReturn(Parent, String(is_dur_result))
end event

type ole_durginfo from olecustomcontrol within w_kimsdur_mers
event statustextchange ( string text )
event progresschange ( long progress,  long progressmax )
event commandstatechange ( long command,  boolean enable )
event downloadbegin ( )
event downloadcomplete ( )
event titlechange ( string text )
event propertychange ( string szproperty )
event beforenavigate2 ( oleobject pdisp,  any url,  any flags,  any targetframename,  any postdata,  any headers,  ref boolean cancel )
event newwindow2 ( ref oleobject ppdisp,  ref boolean cancel )
event navigatecomplete2 ( oleobject pdisp,  any url )
event documentcomplete ( oleobject pdisp,  any url )
event onquit ( )
event onvisible ( boolean ocx_visible )
event ontoolbar ( boolean toolbar )
event onmenubar ( boolean menubar )
event onstatusbar ( boolean statusbar )
event onfullscreen ( boolean fullscreen )
event ontheatermode ( boolean theatermode )
event windowsetresizable ( boolean resizable )
event windowsetleft ( long left )
event windowsettop ( long top )
event windowsetwidth ( long ocx_width )
event windowsetheight ( long ocx_height )
event windowclosing ( boolean ischildwindow,  ref boolean cancel )
event clienttohostwindow ( ref long cx,  ref long cy )
event setsecurelockicon ( long securelockicon )
event filedownload ( ref boolean cancel )
event navigateerror ( oleobject pdisp,  any url,  any frame,  any statuscode,  ref boolean cancel )
event printtemplateinstantiation ( oleobject pdisp )
event printtemplateteardown ( oleobject pdisp )
event updatepagestatus ( oleobject pdisp,  any npage,  any fdone )
event privacyimpactedstatechange ( boolean bimpacted )
integer x = 9
integer y = 8
integer width = 3415
integer height = 2956
integer taborder = 10
boolean bringtotop = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
string binarykey = "w_kimsdur_mers.win"
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
end type


Start of PowerBuilder Binary Data Section : Do NOT Edit
02w_kimsdur_mers.bin 
2A00000a00e011cfd0e11ab1a1000000000000000000000000000000000003003e0009fffe000000060000000000000000000000010000000100000000000010000000000200000001fffffffe0000000000000000fffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffdfffffffefffffffefffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff006f00520074006f004500200074006e00790072000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000050016ffffffffffffffff0000000100000000000000000000000000000000000000000000000000000000c0da5ac001d541d600000003000001800000000000500003004f0042005800430054005300450052004d0041000000000000000000000000000000000000000000000000000000000000000000000000000000000102001affffffff00000002ffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000000000009c00000000004200500043004f00530058004f00540041005200450047000000000000000000000000000000000000000000000000000000000000000000000000000000000001001affffffffffffffff000000038856f96111d0340ac0006ba9a205d74f00000000c0da5ac001d541d6c0da5ac001d541d6000000000000000000000000004f00430054004e004e00450053005400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001020012ffffffffffffffffffffffff000000000000000000000000000000000000000000000000000000000000000000000000000000030000009c000000000000000100000002fffffffe0000000400000005fffffffeffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff
22ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff0000004c00004d3400004c610000000000000000000000000000000000000000000000000000004c0000000000000000000000010057d0e011cf3573000869ae62122e2b00000008000000000000004c0002140100000000000000c0460000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004c00004d3400004c610000000000000000000000000000000000000000000000000000004c0000000000000000000000010057d0e011cf3573000869ae62122e2b00000008000000000000004c0002140100000000000000c0460000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000001000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
12w_kimsdur_mers.bin 
End of PowerBuilder Binary Data Section : No Source Expected After This Point
