﻿$PBExportHeader$w_ctmeter.srw
$PBExportComments$Progress Bar
forward
global type w_ctmeter from window
end type
type uo_meter from uo_ctmeter within w_ctmeter
end type
end forward

global type w_ctmeter from window
integer width = 2066
integer height = 264
boolean titlebar = true
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
uo_meter uo_meter
end type
global w_ctmeter w_ctmeter

on w_ctmeter.create
this.uo_meter=create uo_meter
this.Control[]={this.uo_meter}
end on

on w_ctmeter.destroy
destroy(this.uo_meter)
end on

event open;gf_center (This)

end event

type uo_meter from uo_ctmeter within w_ctmeter
integer x = 14
integer y = 12
integer taborder = 10
end type

on uo_meter.destroy
call uo_ctmeter::destroy
end on

