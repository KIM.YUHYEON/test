﻿$PBExportHeader$w_frame40_personal_infor.srw
forward
global type w_frame40_personal_infor from window
end type
type sle_handphone from singlelineedit within w_frame40_personal_infor
end type
type st_4 from statictext within w_frame40_personal_infor
end type
type st_title from statictext within w_frame40_personal_infor
end type
type st_title_back from statictext within w_frame40_personal_infor
end type
type sle_jumin2 from singlelineedit within w_frame40_personal_infor
end type
type sle_jumin1 from singlelineedit within w_frame40_personal_infor
end type
type cb_exit from commandbutton within w_frame40_personal_infor
end type
type cb_ok from commandbutton within w_frame40_personal_infor
end type
type st_3 from statictext within w_frame40_personal_infor
end type
type sle_empname from singlelineedit within w_frame40_personal_infor
end type
type sle_sabun from singlelineedit within w_frame40_personal_infor
end type
type st_2 from statictext within w_frame40_personal_infor
end type
type st_1 from statictext within w_frame40_personal_infor
end type
end forward

global type w_frame40_personal_infor from window
integer width = 1906
integer height = 840
boolean titlebar = true
windowtype windowtype = response!
long backcolor = 16777215
string icon = "AppIcon!"
boolean center = true
sle_handphone sle_handphone
st_4 st_4
st_title st_title
st_title_back st_title_back
sle_jumin2 sle_jumin2
sle_jumin1 sle_jumin1
cb_exit cb_exit
cb_ok cb_ok
st_3 st_3
sle_empname sle_empname
sle_sabun sle_sabun
st_2 st_2
st_1 st_1
end type
global w_frame40_personal_infor w_frame40_personal_infor

type variables
String	is_sabun, is_jumin
end variables

on w_frame40_personal_infor.create
this.sle_handphone=create sle_handphone
this.st_4=create st_4
this.st_title=create st_title
this.st_title_back=create st_title_back
this.sle_jumin2=create sle_jumin2
this.sle_jumin1=create sle_jumin1
this.cb_exit=create cb_exit
this.cb_ok=create cb_ok
this.st_3=create st_3
this.sle_empname=create sle_empname
this.sle_sabun=create sle_sabun
this.st_2=create st_2
this.st_1=create st_1
this.Control[]={this.sle_handphone,&
this.st_4,&
this.st_title,&
this.st_title_back,&
this.sle_jumin2,&
this.sle_jumin1,&
this.cb_exit,&
this.cb_ok,&
this.st_3,&
this.sle_empname,&
this.sle_sabun,&
this.st_2,&
this.st_1}
end on

on w_frame40_personal_infor.destroy
destroy(this.sle_handphone)
destroy(this.st_4)
destroy(this.st_title)
destroy(this.st_title_back)
destroy(this.sle_jumin2)
destroy(this.sle_jumin1)
destroy(this.cb_exit)
destroy(this.cb_ok)
destroy(this.st_3)
destroy(this.sle_empname)
destroy(this.sle_sabun)
destroy(this.st_2)
destroy(this.st_1)
end on

event open;is_sabun = Message.StringParm

IF is_sabun = '' OR IsNull(is_sabun) THEN
	sle_sabun.SetFocus()
ELSE
	sle_sabun.Text = is_sabun
	sle_sabun.TriggerEvent(modified!)
	
	sle_jumin1.SetFocus()
END IF
end event

type sle_handphone from singlelineedit within w_frame40_personal_infor
integer x = 594
integer y = 460
integer width = 809
integer height = 108
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type st_4 from statictext within w_frame40_personal_infor
integer x = 297
integer y = 484
integer width = 274
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "전화번호"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_title from statictext within w_frame40_personal_infor
integer x = 64
integer y = 48
integer width = 1778
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 1073741824
long backcolor = 553648127
string text = "비밀번호 5회 오류자 본인인증"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_title_back from statictext within w_frame40_personal_infor
integer x = 37
integer y = 28
integer width = 1833
integer height = 128
integer textsize = -9
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 19230186
boolean focusrectangle = false
end type

type sle_jumin2 from singlelineedit within w_frame40_personal_infor
integer x = 1033
integer y = 328
integer width = 475
integer height = 92
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
boolean password = true
integer limit = 7
borderstyle borderstyle = stylelowered!
end type

type sle_jumin1 from singlelineedit within w_frame40_personal_infor
integer x = 590
integer y = 332
integer width = 334
integer height = 92
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
integer limit = 6
borderstyle borderstyle = stylelowered!
end type

type cb_exit from commandbutton within w_frame40_personal_infor
integer x = 1033
integer y = 616
integer width = 430
integer height = 112
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
string text = "종료"
end type

event clicked;closeWithReturn(parent, 'N')
end event

type cb_ok from commandbutton within w_frame40_personal_infor
integer x = 453
integer y = 616
integer width = 430
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
string text = "확인"
end type

event clicked;Integer	li_flag
String	ls_errMsg, ls_jumin_check, ls_msgTitle
Long	ll_rtn

is_jumin = sle_jumin1.Text + sle_jumin2.Text

IF LenA(is_jumin) <> 13 THEN
	Messagebox("확 인", "주민등록번호 자리수가 맞지 않습니다.~r~n" +&
	                    "다시 입력하세요!")
	
	sle_jumin1.SelectText(1, LenA(sle_jumin1.Text))
	
	Return 0	
END IF


// 주민등록번호 유효성 검사
select f_jumin_check( :is_jumin ) 
  into :ls_jumin_check
  from dual
using SQLCA;
gf_sqlerror2(SQLCA, False)

IF ls_jumin_check = '0' THEN
	Messagebox("확 인", "주민등록번호를 잘못 입력했습니다.~r~n" +&
	                    "다시 입력하세요!")
	
	sle_jumin1.SelectText(1, LenA(sle_jumin1.Text))
	
	Return 0
END IF

DECLARE PERSONAL_INFOR_CHK PROCEDURE FOR MEGADB1.SP_PERSONAL_INFOR_CHK( :is_sabun, :is_jumin ) using SQLCA;
EXECUTE PERSONAL_INFOR_CHK;
FETCH PERSONAL_INFOR_CHK INTO :li_flag, :ls_errMsg;

IF SQLCA.SQLCODE <> 0 THEN
	CLOSE PERSONAL_INFOR_CHK;
	Messagebox("오 류", "SP_PERSONAL_INFOR_CHK 실행 오류 : " + SQLCA.SQLErrText)
	
	Return 0
ELSE
	IF li_flag = 0 THEN
		ls_msgTitle = "확 인"
	ELSE
		ls_msgTitle = "오 류"
	END IF
	
	Messagebox(ls_msgTitle, ls_errMsg)
END IF

CLOSE PERSONAL_INFOR_CHK;

closeWithReturn(parent, 'Y')





end event

type st_3 from statictext within w_frame40_personal_infor
integer x = 933
integer y = 344
integer width = 91
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "-"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_empname from singlelineedit within w_frame40_personal_infor
integer x = 937
integer y = 204
integer width = 571
integer height = 92
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 31383262
boolean enabled = false
integer limit = 8
end type

type sle_sabun from singlelineedit within w_frame40_personal_infor
integer x = 590
integer y = 204
integer width = 334
integer height = 92
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
integer limit = 8
borderstyle borderstyle = stylelowered!
end type

event modified;String	ls_empName, ls_empHandphone

is_sabun = This.Text

select a.name
  into :ls_empName
  from tb_employee a
 where a.sabun = :is_sabun
   and a.stat = '0'
using SQLCA;
gf_sqlerror2(SQLCA, False)

IF ls_empName = '' OR IsNull(ls_empName) THEN
	Messagebox("확 인", "존재하지 않은 사원번호입니다.~r~n" +&
	                    "다시 입력하세요!")
	
	This.SelectText(1, LenA(This.Text))
	
	Return 0
END IF

select c.handphone
  into :ls_empHandphone
  from tb_employee a, 
		 tc_header b, 
		 tc_personal c
 where a.sabun = ( case 
							 when lengthb(:is_sabun) > 5 then
								 decode(substrb(:is_sabun, 1, 2), 'hd', '0'||Trim(substrb(:is_sabun, 3)), substrb(:is_sabun, 1, 5))
							 else
								 :is_sabun
						 end )
	and a.sabun = b.sabun					 
	and b.sabun = c.sabun(+)     
	and a.stat = '0'
	and b.status = '01'
using SQLCA;
gf_sqlerror2(SQLCA, False)

sle_empname.Text = ls_empName
sle_handphone.Text = ls_empHandphone

sle_jumin1.SetFocus()

end event

type st_2 from statictext within w_frame40_personal_infor
integer x = 297
integer y = 348
integer width = 274
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "주민번호"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_frame40_personal_infor
integer x = 297
integer y = 220
integer width = 274
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "맑은 고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "사원번호"
alignment alignment = right!
boolean focusrectangle = false
end type

