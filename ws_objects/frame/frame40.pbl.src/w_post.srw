﻿$PBExportHeader$w_post.srw
$PBExportComments$우편번호찾기-2004버젼
forward
global type w_post from window
end type
type pb_close from picturebutton within w_post
end type
type pb_retrieve from picturebutton within w_post
end type
type sle_dong from singlelineedit within w_post
end type
type st_1 from statictext within w_post
end type
type dw_postlist from datawindow within w_post
end type
type cb_continue from commandbutton within w_post
end type
type gb_2 from groupbox within w_post
end type
type gb_1 from groupbox within w_post
end type
end forward

global type w_post from window
integer x = 379
integer y = 428
integer width = 2245
integer height = 1700
boolean titlebar = true
string title = "우편번호 찾기"
windowtype windowtype = response!
long backcolor = 76585128
toolbaralignment toolbaralignment = alignatleft!
event ue_fkey pbm_dwnkey
pb_close pb_close
pb_retrieve pb_retrieve
sle_dong sle_dong
st_1 st_1
dw_postlist dw_postlist
cb_continue cb_continue
gb_2 gb_2
gb_1 gb_1
end type
global w_post w_post

type variables
String is_addr
end variables

on w_post.create
this.pb_close=create pb_close
this.pb_retrieve=create pb_retrieve
this.sle_dong=create sle_dong
this.st_1=create st_1
this.dw_postlist=create dw_postlist
this.cb_continue=create cb_continue
this.gb_2=create gb_2
this.gb_1=create gb_1
this.Control[]={this.pb_close,&
this.pb_retrieve,&
this.sle_dong,&
this.st_1,&
this.dw_postlist,&
this.cb_continue,&
this.gb_2,&
this.gb_1}
end on

on w_post.destroy
destroy(this.pb_close)
destroy(this.pb_retrieve)
destroy(this.sle_dong)
destroy(this.st_1)
destroy(this.dw_postlist)
destroy(this.cb_continue)
destroy(this.gb_2)
destroy(this.gb_1)
end on

event open;string ls_dong

ls_dong = message.StringParm 

sle_dong.text = ls_dong

if dw_postlist.Retrieve(ls_dong) > 0 then
	dw_postlist.Setfocus()
	dw_postlist.scrolltorow(1)
	is_addr = dw_postlist.Object.postcode[1] + dw_postlist.Object.postname[1]
end if
end event

type pb_close from picturebutton within w_post
integer x = 1883
integer y = 60
integer width = 302
integer height = 104
integer taborder = 20
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
string picturename = "c:\fatimav19\icon\exit_em.bmp"
alignment htextalign = right!
end type

event clicked;CloseWithReturn(parent, is_addr) 
end event

type pb_retrieve from picturebutton within w_post
integer x = 1339
integer y = 64
integer width = 110
integer height = 96
integer taborder = 10
integer textsize = -12
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string picturename = "c:\fatimav19\icon\search_es.bmp"
alignment htextalign = left!
end type

event clicked;string ls_dong

ls_dong = sle_dong.text

if dw_postlist.Retrieve(ls_dong) > 0 then
	dw_postlist.Setfocus()
	dw_postlist.scrolltorow(1)
	is_addr = dw_postlist.Object.postcode[1] + dw_postlist.Object.postname[1]
end if
end event

type sle_dong from singlelineedit within w_post
event ue_enter pbm_dwnprocessenter
integer x = 649
integer y = 72
integer width = 672
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event type long ue_enter();pb_retrieve.Triggerevent('Clicked!')

return 0
end event

event modified;pb_retrieve.Triggerevent('Clicked!')
end event

type st_1 from statictext within w_post
integer x = 50
integer y = 76
integer width = 590
integer height = 72
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "동을 입력하세요"
boolean focusrectangle = false
end type

type dw_postlist from datawindow within w_post
event ue_fkey pbm_dwnkey
integer x = 23
integer y = 200
integer width = 2181
integer height = 1376
integer taborder = 10
string dataobject = "d_postlist"
boolean vscrollbar = true
boolean border = false
end type

event type long ue_fkey(keycode key, unsignedlong keyflags);long ll
CHOOSE CASE key
	CASE keydownarrow!, keyrightarrow!
		dw_postlist.setfocus()
		dw_postlist.scrollnextrow()
	CASE keyuparrow!, keyleftarrow!
		dw_postlist.setfocus()
		dw_postlist.scrollpriorrow()
	CASE keypagedown!
		dw_postlist.setfocus()
		dw_postlist.scrollnextpage()
	CASE keypageup!
		dw_postlist.setfocus()
		dw_postlist.scrollpriorpage()
	CASE keyescape!
		is_addr = ' '
		CloseWithReturn(parent, is_addr) 
	CASE keyEnter!
		CloseWithReturn(parent, is_addr) 
END CHOOSE

return 1
end event

event constructor;SetTransObject(SQLCA)
end event

event clicked;if row < 1 then
	return
end if

This.Selectrow(0, False)
This.Selectrow(row, True)

is_addr = This.Object.postcode[row] + This.Object.postname[row]
This.scrolltorow(row)
This.setfocus()
end event

event doubleclicked;if row < 1 then
	return
end if

This.Selectrow(0, False)
This.Selectrow(row, True)

is_addr = This.Object.postcode[row] + This.Object.postname[row]

CloseWithReturn(parent, is_addr) 
end event

event rowfocuschanged;if currentrow > 0 then
	dw_postlist.selectrow(0,false)
	dw_postlist.selectrow(currentrow,true)
	dw_postlist.scrolltorow(currentrow)

	is_addr = This.Object.postcode[currentrow] + This.Object.postname[currentrow]
end if
end event

type cb_continue from commandbutton within w_post
boolean visible = false
integer x = 859
integer y = 1396
integer width = 430
integer height = 88
integer taborder = 20
integer textsize = -8
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "MS Sans Serif"
string text = "&Continue"
end type

on clicked;/////////////////////////////////////////////////////////////////////////
//
// Event	 :  w_system_error.cb_continue
//
// Purpose:
// 			Closes w_system_error
//
// Log:
// 
// DATE		NAME				REVISION
//------		-------------------------------------------------------------
// Powersoft Corporation	INITIAL VERSION
//
///////////////////////////////////////////////////////////////////////////

close(parent)
end on

type gb_2 from groupbox within w_post
integer x = 18
integer y = 8
integer width = 2194
integer height = 180
integer taborder = 10
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_1 from groupbox within w_post
integer x = 18
integer y = 168
integer width = 2194
integer height = 1416
integer taborder = 20
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
end type

