﻿$PBExportHeader$w_dup_hjname.srw
$PBExportComments$날짜 선택 달력
forward
global type w_dup_hjname from window
end type
type st_1 from statictext within w_dup_hjname
end type
type cb_2 from commandbutton within w_dup_hjname
end type
type cb_1 from commandbutton within w_dup_hjname
end type
type dw_list2 from datawindow within w_dup_hjname
end type
type dw_list1 from datawindow within w_dup_hjname
end type
type gb_1 from groupbox within w_dup_hjname
end type
type gb_2 from groupbox within w_dup_hjname
end type
end forward

global type w_dup_hjname from window
integer width = 2715
integer height = 1512
boolean titlebar = true
string title = "생년월일이 동일한 동명이인"
windowtype windowtype = response!
long backcolor = 16777215
string icon = "AppIcon!"
boolean center = true
st_1 st_1
cb_2 cb_2
cb_1 cb_1
dw_list2 dw_list2
dw_list1 dw_list1
gb_1 gb_1
gb_2 gb_2
end type
global w_dup_hjname w_dup_hjname

event open;dw_list1.settransobject(mega1)
dw_list2.settransobject(mega1)

String ls_parm,ls_hospno, ls_hjname, ls_sex,ls_jumin

ls_parm = message.stringparm
ls_hospno = ls_parm

dw_list1.Reset()
dw_list1.Retrieve(ls_hospno)

select name,sex,JUMINYY||substr(JUMIN,1,6)
  into :ls_hjname,:ls_sex,:ls_jumin
  from tb_idmast
 where hospno = :ls_hospno
 using mega1;

dw_list2.Reset()
dw_list2.Retrieve(ls_hospno,ls_hjname,ls_jumin)


end event

on w_dup_hjname.create
this.st_1=create st_1
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_list2=create dw_list2
this.dw_list1=create dw_list1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.Control[]={this.st_1,&
this.cb_2,&
this.cb_1,&
this.dw_list2,&
this.dw_list1,&
this.gb_1,&
this.gb_2}
end on

on w_dup_hjname.destroy
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_list2)
destroy(this.dw_list1)
destroy(this.gb_1)
destroy(this.gb_2)
end on

type st_1 from statictext within w_dup_hjname
integer x = 59
integer y = 44
integer width = 2592
integer height = 84
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "나눔고딕"
long textcolor = 16777215
long backcolor = 29846593
string text = " 생년월일이 동일한 동명이인 입니다."
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_dup_hjname
integer x = 1381
integer y = 1284
integer width = 430
integer height = 108
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "나눔고딕"
string text = "확인"
end type

event clicked;closewithreturn(parent, 'TRUE')
end event

type cb_1 from commandbutton within w_dup_hjname
integer x = 951
integer y = 1284
integer width = 430
integer height = 108
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "나눔고딕"
string text = "되돌아가기"
end type

event clicked;closewithreturn(parent, 'FALSE')
end event

type dw_list2 from datawindow within w_dup_hjname
integer x = 105
integer y = 680
integer width = 2510
integer height = 532
integer taborder = 30
string title = "none"
string dataobject = "d_dup_hjname2"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_list1 from datawindow within w_dup_hjname
integer x = 105
integer y = 244
integer width = 2510
integer height = 264
integer taborder = 20
string title = "none"
string dataobject = "d_dup_hjname1"
boolean border = false
boolean livescroll = true
end type

type gb_1 from groupbox within w_dup_hjname
integer x = 55
integer y = 164
integer width = 2601
integer height = 396
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "나눔고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "선택환자"
end type

type gb_2 from groupbox within w_dup_hjname
integer x = 55
integer y = 580
integer width = 2601
integer height = 672
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = variable!
fontfamily fontfamily = modern!
string facename = "나눔고딕"
long textcolor = 33554432
long backcolor = 16777215
string text = "생년월일이 동일한 동명이인"
end type

