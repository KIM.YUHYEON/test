﻿$PBExportHeader$w_month.srw
$PBExportComments$달력(월)
forward
global type w_month from window
end type
type st_18 from statictext within w_month
end type
type em_year from editmask within w_month
end type
type st_12 from statictext within w_month
end type
type st_11 from statictext within w_month
end type
type st_10 from statictext within w_month
end type
type st_9 from statictext within w_month
end type
type st_8 from statictext within w_month
end type
type st_7 from statictext within w_month
end type
type st_6 from statictext within w_month
end type
type st_5 from statictext within w_month
end type
type st_4 from statictext within w_month
end type
type st_3 from statictext within w_month
end type
type st_2 from statictext within w_month
end type
type st_1 from statictext within w_month
end type
type st_17 from statictext within w_month
end type
type gb_1 from groupbox within w_month
end type
end forward

global type w_month from window
integer x = 503
integer y = 820
integer width = 1234
integer height = 556
boolean titlebar = true
string title = "월력(년/월)"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 79741120
string icon = "Form!"
st_18 st_18
em_year em_year
st_12 st_12
st_11 st_11
st_10 st_10
st_9 st_9
st_8 st_8
st_7 st_7
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
st_17 st_17
gb_1 gb_1
end type
global w_month w_month

type variables
string is_yy, is_mm, is_yymm, is_sw


end variables

forward prototypes
public subroutine wf_setcolor ()
end prototypes

public subroutine wf_setcolor ();//reset color
st_1.Backcolor  = RGB(192,192,192)
st_2.Backcolor  = RGB(192,192,192)
st_3.Backcolor  = RGB(192,192,192)
st_4.Backcolor  = RGB(192,192,192)
st_5.Backcolor  = RGB(192,192,192)
st_6.Backcolor  = RGB(192,192,192)
st_7.Backcolor  = RGB(192,192,192)
st_8.Backcolor  = RGB(192,192,192)
st_9.Backcolor  = RGB(192,192,192)
st_10.Backcolor = RGB(192,192,192)
st_11.Backcolor = RGB(192,192,192)
st_12.Backcolor = RGB(192,192,192)

st_1.TextColor  = RGB(0,0,0)
st_2.TextColor  = RGB(0,0,0)
st_3.TextColor  = RGB(0,0,0)
st_4.TextColor  = RGB(0,0,0)
st_5.TextColor  = RGB(0,0,0)
st_6.TextColor  = RGB(0,0,0)
st_7.TextColor  = RGB(0,0,0)
st_8.TextColor  = RGB(0,0,0)
st_9.TextColor  = RGB(0,0,0)
st_10.TextColor  = RGB(0,0,0)
st_11.TextColor  = RGB(0,0,0)
st_12.TextColor  = RGB(0,0,0)

CHOOSE CASE is_mm
	CASE '01'
		st_1.Backcolor = RGB(255, 128, 30)
		st_1.TextColor  = RGB(255,255,255)
	CASE '02'
		st_2.Backcolor = RGB(255, 128, 30)
		st_2.TextColor  = RGB(255,255,255)
	CASE '03'
		st_3.Backcolor = RGB(255, 128, 30)
		st_3.TextColor  = RGB(255,255,255)
	CASE '04'
		st_4.Backcolor = RGB(255, 128, 30)
		st_4.TextColor  = RGB(255,255,255)
	CASE '05'
		st_5.Backcolor = RGB(255, 128, 30)
		st_5.TextColor  = RGB(255,255,255)
	CASE '06'
		st_6.Backcolor = RGB(255, 128, 30)
		st_6.TextColor  = RGB(255,255,255)
	CASE '07'
		st_7.Backcolor = RGB(255, 128, 30)
		st_7.TextColor  = RGB(255,255,255)
	CASE '08'
		st_8.Backcolor = RGB(255, 128, 30)
		st_8.TextColor  = RGB(255,255,255)
	CASE '09'
		st_9.Backcolor = RGB(255, 128, 30)
		st_9.TextColor  = RGB(255,255,255)
	CASE '10'
		st_10.Backcolor = RGB(255, 128, 30)
		st_10.TextColor  = RGB(255,255,255)
	CASE '11'
		st_11.Backcolor = RGB(255, 128, 30)
		st_11.TextColor  = RGB(255,255,255)
	CASE '12'
		st_12.Backcolor = RGB(255, 128, 30)
		st_12.TextColor  = RGB(255,255,255)
END CHOOSE

if is_sw = 'open' then
else
	close(w_month)
end if

end subroutine

event open;string ls_today

ls_today = message.StringParm 


if ls_today > ' ' then
	is_yy   = leftA(ls_today, 4)
	is_yymm = leftA(ls_today, 6)
else
	is_yy   = leftA(gf_sysdate(), 4)  
	is_yymm = leftA(gf_sysdate(), 6)  
end if

em_year.Text = is_yy

is_mm = rightA(is_yymm, 2)

is_sw = 'open'
wf_setcolor()
is_sw = ' '


end event

on w_month.create
this.st_18=create st_18
this.em_year=create em_year
this.st_12=create st_12
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.st_17=create st_17
this.gb_1=create gb_1
this.Control[]={this.st_18,&
this.em_year,&
this.st_12,&
this.st_11,&
this.st_10,&
this.st_9,&
this.st_8,&
this.st_7,&
this.st_6,&
this.st_5,&
this.st_4,&
this.st_3,&
this.st_2,&
this.st_1,&
this.st_17,&
this.gb_1}
end on

on w_month.destroy
destroy(this.st_18)
destroy(this.em_year)
destroy(this.st_12)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.st_17)
destroy(this.gb_1)
end on

event close;CloseWithReturn(this, is_yymm) 
end event

type st_18 from statictext within w_month
integer x = 398
integer y = 88
integer width = 78
integer height = 64
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "년"
boolean focusrectangle = false
end type

type em_year from editmask within w_month
integer x = 73
integer y = 72
integer width = 311
integer height = 96
integer taborder = 30
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 16777215
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "yyyy"
boolean spin = true
end type

event losefocus;is_yy = This.Text
end event

type st_12 from statictext within w_month
integer x = 974
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "12"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '12'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_11 from statictext within w_month
integer x = 795
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "11"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '11'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_10 from statictext within w_month
integer x = 617
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "10"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '10'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_9 from statictext within w_month
integer x = 439
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "9"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '09'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_8 from statictext within w_month
integer x = 261
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "8"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '08'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_7 from statictext within w_month
integer x = 82
integer y = 308
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "7"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '07'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_6 from statictext within w_month
integer x = 974
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "6"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '06'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_5 from statictext within w_month
integer x = 795
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "5"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '05'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_4 from statictext within w_month
integer x = 617
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "4"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '04'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_3 from statictext within w_month
integer x = 439
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "3"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '03'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_2 from statictext within w_month
integer x = 261
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "2"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '02'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_1 from statictext within w_month
integer x = 82
integer y = 208
integer width = 178
integer height = 100
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = roman!
string facename = "MS Serif"
long backcolor = 12632256
string text = "1"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;is_mm = '01'
is_yymm = is_yy+is_mm
wf_setcolor()

end event

type st_17 from statictext within w_month
integer x = 73
integer y = 196
integer width = 1083
integer height = 220
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_month
integer x = 23
integer width = 1170
integer height = 456
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 79741120
borderstyle borderstyle = styleraised!
end type

