﻿$PBExportHeader$w_dwupdate_msg.srw
$PBExportComments$저장 메시지
forward
global type w_dwupdate_msg from window
end type
type st_message from statictext within w_dwupdate_msg
end type
end forward

global type w_dwupdate_msg from window
integer x = 699
integer y = 1200
integer width = 3255
integer height = 120
windowtype windowtype = popup!
long backcolor = 33545401
st_message st_message
end type
global w_dwupdate_msg w_dwupdate_msg

on w_dwupdate_msg.create
this.st_message=create st_message
this.Control[]={this.st_message}
end on

on w_dwupdate_msg.destroy
destroy(this.st_message)
end on

event open;String ls_message

ls_message = Message.StringParm

st_message.Text = gf_msg(ls_message)

Timer(1)
end event

event timer;Close(w_dwupdate_msg)
end event

type st_message from statictext within w_dwupdate_msg
integer x = 421
integer y = 16
integer width = 2437
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 33545401
string text = "저장 되었습니다!"
alignment alignment = center!
long bordercolor = 8421376
boolean focusrectangle = false
end type

