﻿$PBExportHeader$w_select_win.srw
$PBExportComments$구분코드 선택화면
forward
global type w_select_win from window
end type
type dw_body from datawindow within w_select_win
end type
type mle_heading from multilineedit within w_select_win
end type
type gb_1 from groupbox within w_select_win
end type
end forward

global type w_select_win from window
integer x = 302
integer y = 300
integer width = 1161
integer height = 584
boolean titlebar = true
string title = "선택화면"
windowtype windowtype = response!
long backcolor = 67108864
dw_body dw_body
mle_heading mle_heading
gb_1 gb_1
end type
global w_select_win w_select_win

type variables
integer	ii_rtnnum 

string	is_ini 

end variables

on w_select_win.create
this.dw_body=create dw_body
this.mle_heading=create mle_heading
this.gb_1=create gb_1
this.Control[]={this.dw_body,&
this.mle_heading,&
this.gb_1}
end on

on w_select_win.destroy
destroy(this.dw_body)
destroy(this.mle_heading)
destroy(this.gb_1)
end on

event open;//openwithparm('w_select_win', ls_defnum + ls_heading + ls_body)
//ls_defnum  	= '02'
//ls_heading 	= '구문을 선택하세요.@'
//ls_body 		= '구문1^구문2^구문3^구문4^취소^' 

gf_center(w_select_win) 

this.y = this.y - 300
this.x = this.x - 200

string	ls_imsi 
string	ls_selnum, ls_defnum, ls_string 
long		ll_selnum, ll_defnum 

ls_imsi = Message.StringParm

ll_defnum  = integer(midA(ls_imsi, 1, 2))  
ls_string  = rightA(ls_imsi, lenA(ls_imsi) - 2) 

integer	i, li_pos 
string	ls_heading, ls_body 
string	ls_string_rem, ls_string_use  

ls_string_rem = ls_string 

li_pos = posA(ls_string_rem, '@') 
ls_heading = midA(ls_string_rem, 1, li_pos - 1)  

mle_heading.text = ls_heading 
ls_string_rem = rightA(ls_string_rem, lenA(ls_string_rem) - (lenA(ls_heading)+1))  
DO
	i ++ 
	li_pos = posA(ls_string_rem, '^') 
	ls_string_use = midA(ls_string_rem, 1, li_pos - 1)  
	if ls_string_use > ' ' then 
		dw_body.InsertRow(0) 
		dw_body.Object.body[i] = '  '+ls_string_use 
	end if
	ls_string_rem = rightA(ls_string_rem, lenA(ls_string_rem) - li_pos)  
LOOP WHILE ( li_pos > 0 )

dw_body.scrolltorow(ll_defnum)

ii_rtnnum = ll_defnum 

integer 	li_resize 
li_resize = (dw_body.Rowcount() -1) * 110

dw_body.height = dw_body.height + li_resize 
gb_1.height 	= gb_1.height + li_resize
this.height 	= this.height + li_resize

end event

event close;CloseWithReturn(this, string(ii_rtnnum))

end event

event key;ii_rtnnum = 0

IF KeyDown(Key1!) and dw_body.RowCount() >= 1 THEN
	ii_rtnnum = 1 
END IF	
IF KeyDown(Key2!) and dw_body.RowCount() >= 2 THEN
	ii_rtnnum = 2 
END IF	
IF KeyDown(Key3!) and dw_body.RowCount() >= 3 THEN
	ii_rtnnum = 3 
END IF	
IF KeyDown(Key4!) and dw_body.RowCount() >= 4 THEN
	ii_rtnnum = 4 
END IF	
IF KeyDown(Key5!) and dw_body.RowCount() >= 5 THEN
	ii_rtnnum = 5 
END IF	
IF KeyDown(Key6!) and dw_body.RowCount() >= 6 THEN
	ii_rtnnum = 6 
END IF	
IF KeyDown(Key7!) and dw_body.RowCount() >= 7 THEN
	ii_rtnnum = 7 
END IF	
IF KeyDown(Key8!) and dw_body.RowCount() >= 8 THEN
	ii_rtnnum = 8 
END IF	
IF KeyDown(Key9!) and dw_body.RowCount() >= 9 THEN
	ii_rtnnum = 9 
END IF	

IF ii_rtnnum > 0 THEN CloseWithReturn(this, ii_rtnnum)

end event

type dw_body from datawindow within w_select_win
integer x = 27
integer y = 360
integer width = 1097
integer height = 120
string title = "none"
string dataobject = "d_select_win"
boolean border = false
end type

event clicked;scrolltorow(row) 
ii_rtnnum = row 

CloseWithReturn(parent, ii_rtnnum) 

end event

event doubleclicked;ii_rtnnum = row 

CloseWithReturn(parent, ii_rtnnum) 

end event

type mle_heading from multilineedit within w_select_win
integer x = 27
integer y = 16
integer width = 1106
integer height = 304
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 12632256
boolean displayonly = true
borderstyle borderstyle = styleraised!
boolean hideselection = false
end type

type gb_1 from groupbox within w_select_win
integer x = 9
integer y = 320
integer width = 1125
integer height = 168
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
borderstyle borderstyle = stylelowered!
end type

