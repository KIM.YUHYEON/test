﻿$PBExportHeader$w_print_view_popup.srw
$PBExportComments$openwithparm(datawindow) 출력물 미리보기- Popup
forward
global type w_print_view_popup from window
end type
type pb_excel from picturebutton within w_print_view_popup
end type
type pb_exit from picturebutton within w_print_view_popup
end type
type pb_setup from picturebutton within w_print_view_popup
end type
type pb_print from picturebutton within w_print_view_popup
end type
type st_1 from statictext within w_print_view_popup
end type
type st_4 from statictext within w_print_view_popup
end type
type st_2 from statictext within w_print_view_popup
end type
type em_zoom2 from editmask within w_print_view_popup
end type
type rb_z200 from radiobutton within w_print_view_popup
end type
type rb_z150 from radiobutton within w_print_view_popup
end type
type rb_z120 from radiobutton within w_print_view_popup
end type
type rb_z100 from radiobutton within w_print_view_popup
end type
type rb_z80 from radiobutton within w_print_view_popup
end type
type rb_z50 from radiobutton within w_print_view_popup
end type
type dw_1 from datawindow within w_print_view_popup
end type
type em_zoom from editmask within w_print_view_popup
end type
type em_copies from editmask within w_print_view_popup
end type
type st_print_zoom from statictext within w_print_view_popup
end type
type st_copies from statictext within w_print_view_popup
end type
type rb_land from radiobutton within w_print_view_popup
end type
type rb_port from radiobutton within w_print_view_popup
end type
type gb_prient from groupbox within w_print_view_popup
end type
type rb_all from radiobutton within w_print_view_popup
end type
type rb_current_page from radiobutton within w_print_view_popup
end type
type rb_pages from radiobutton within w_print_view_popup
end type
type sle_range from singlelineedit within w_print_view_popup
end type
type st_pages1 from statictext within w_print_view_popup
end type
type st_pages2 from statictext within w_print_view_popup
end type
type gb_paper from groupbox within w_print_view_popup
end type
type cbx_print_to_file from checkbox within w_print_view_popup
end type
type gb_print from groupbox within w_print_view_popup
end type
type cbx_rulers from checkbox within w_print_view_popup
end type
type pb_first from picturebutton within w_print_view_popup
end type
type pb_prior from picturebutton within w_print_view_popup
end type
type pb_next from picturebutton within w_print_view_popup
end type
type pb_last from picturebutton within w_print_view_popup
end type
type st_6 from statictext within w_print_view_popup
end type
type st_preview from statictext within w_print_view_popup
end type
type gb_preview from groupbox within w_print_view_popup
end type
end forward

global type w_print_view_popup from window
integer width = 3168
integer height = 2428
boolean titlebar = true
string title = "Print Preview"
boolean controlmenu = true
boolean minbox = true
windowtype windowtype = popup!
windowstate windowstate = maximized!
long backcolor = 67108864
pb_excel pb_excel
pb_exit pb_exit
pb_setup pb_setup
pb_print pb_print
st_1 st_1
st_4 st_4
st_2 st_2
em_zoom2 em_zoom2
rb_z200 rb_z200
rb_z150 rb_z150
rb_z120 rb_z120
rb_z100 rb_z100
rb_z80 rb_z80
rb_z50 rb_z50
dw_1 dw_1
em_zoom em_zoom
em_copies em_copies
st_print_zoom st_print_zoom
st_copies st_copies
rb_land rb_land
rb_port rb_port
gb_prient gb_prient
rb_all rb_all
rb_current_page rb_current_page
rb_pages rb_pages
sle_range sle_range
st_pages1 st_pages1
st_pages2 st_pages2
gb_paper gb_paper
cbx_print_to_file cbx_print_to_file
gb_print gb_print
cbx_rulers cbx_rulers
pb_first pb_first
pb_prior pb_prior
pb_next pb_next
pb_last pb_last
st_6 st_6
st_preview st_preview
gb_preview gb_preview
end type
global w_print_view_popup w_print_view_popup

type variables
String is_print
datawindow dw_print_view
end variables

event open;String ls_imsi, ls_valuename


dw_print_view = Create datawindow
is_Print = 'N'

/* structure return value */
dw_print_view = Message.PowerObjectParm

dw_1.dataObject = dw_print_view.dataObject
dw_1.SetTransObject(sqlca)
dw_1.Reset() 
dw_print_view.ShareData(dw_1) 

dw_1.modify("datawindow.print.Preview = YES")	
dw_1.modify("datawindow.print.Preview.Rulers = YES")

dw_1.SetRedraw(FALSE)

em_zoom.Text = String(dw_print_view.Object.datawindow.zoom)
dw_1.Object.Datawindow.zoom = Integer(em_zoom.text)

ls_imsi = string(dw_print_view.Object.DataWindow.objects)
DO UNTIL ls_imsi=""
	if posA(ls_imsi,"	") < 1 then
		ls_valuename = trim(ls_imsi)
		ls_imsi = ""
	else
		ls_valuename = midA(ls_imsi,1,posA(ls_imsi,"	")-1)
		ls_imsi = midA(ls_imsi,posA(ls_imsi,"	")+1,lenA(ls_imsi))		
	end if	

	if trim(dw_print_view.Describe(ls_valuename+".Type")) = "text" then
		dw_1.Modify(ls_valuename+".Text='"+dw_print_view.Describe(ls_valuename+".Text")+" '")
	end if
LOOP

IF dw_print_view.Object.DataWindow.Print.Orientation = '1' THEN
	rb_land.Checked = TRUE
ELSE
	rb_port.Checked = TRUE
END IF
dw_1.SetRedraw(TRUE)


end event

on w_print_view_popup.create
this.pb_excel=create pb_excel
this.pb_exit=create pb_exit
this.pb_setup=create pb_setup
this.pb_print=create pb_print
this.st_1=create st_1
this.st_4=create st_4
this.st_2=create st_2
this.em_zoom2=create em_zoom2
this.rb_z200=create rb_z200
this.rb_z150=create rb_z150
this.rb_z120=create rb_z120
this.rb_z100=create rb_z100
this.rb_z80=create rb_z80
this.rb_z50=create rb_z50
this.dw_1=create dw_1
this.em_zoom=create em_zoom
this.em_copies=create em_copies
this.st_print_zoom=create st_print_zoom
this.st_copies=create st_copies
this.rb_land=create rb_land
this.rb_port=create rb_port
this.gb_prient=create gb_prient
this.rb_all=create rb_all
this.rb_current_page=create rb_current_page
this.rb_pages=create rb_pages
this.sle_range=create sle_range
this.st_pages1=create st_pages1
this.st_pages2=create st_pages2
this.gb_paper=create gb_paper
this.cbx_print_to_file=create cbx_print_to_file
this.gb_print=create gb_print
this.cbx_rulers=create cbx_rulers
this.pb_first=create pb_first
this.pb_prior=create pb_prior
this.pb_next=create pb_next
this.pb_last=create pb_last
this.st_6=create st_6
this.st_preview=create st_preview
this.gb_preview=create gb_preview
this.Control[]={this.pb_excel,&
this.pb_exit,&
this.pb_setup,&
this.pb_print,&
this.st_1,&
this.st_4,&
this.st_2,&
this.em_zoom2,&
this.rb_z200,&
this.rb_z150,&
this.rb_z120,&
this.rb_z100,&
this.rb_z80,&
this.rb_z50,&
this.dw_1,&
this.em_zoom,&
this.em_copies,&
this.st_print_zoom,&
this.st_copies,&
this.rb_land,&
this.rb_port,&
this.gb_prient,&
this.rb_all,&
this.rb_current_page,&
this.rb_pages,&
this.sle_range,&
this.st_pages1,&
this.st_pages2,&
this.gb_paper,&
this.cbx_print_to_file,&
this.gb_print,&
this.cbx_rulers,&
this.pb_first,&
this.pb_prior,&
this.pb_next,&
this.pb_last,&
this.st_6,&
this.st_preview,&
this.gb_preview}
end on

on w_print_view_popup.destroy
destroy(this.pb_excel)
destroy(this.pb_exit)
destroy(this.pb_setup)
destroy(this.pb_print)
destroy(this.st_1)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.em_zoom2)
destroy(this.rb_z200)
destroy(this.rb_z150)
destroy(this.rb_z120)
destroy(this.rb_z100)
destroy(this.rb_z80)
destroy(this.rb_z50)
destroy(this.dw_1)
destroy(this.em_zoom)
destroy(this.em_copies)
destroy(this.st_print_zoom)
destroy(this.st_copies)
destroy(this.rb_land)
destroy(this.rb_port)
destroy(this.gb_prient)
destroy(this.rb_all)
destroy(this.rb_current_page)
destroy(this.rb_pages)
destroy(this.sle_range)
destroy(this.st_pages1)
destroy(this.st_pages2)
destroy(this.gb_paper)
destroy(this.cbx_print_to_file)
destroy(this.gb_print)
destroy(this.cbx_rulers)
destroy(this.pb_first)
destroy(this.pb_prior)
destroy(this.pb_next)
destroy(this.pb_last)
destroy(this.st_6)
destroy(this.st_preview)
destroy(this.gb_preview)
end on

event resize;IF THIS.Width < 2469 THEN
	THIS.Width = 2469 
	Return
END IF

IF THIS.Height < 2428 THEN
	THIS.Height = 2428
	Return
END IF

THIS.dw_1.Height = THIS.Height - 284
THIS.dw_1.Width = THIS.Width - 846

pb_excel.X = THIS.Width - 1706
pb_print.X = THIS.Width - 1294
pb_setup.X = THIS.Width - 882
pb_exit.X  = THIS.Width - 466

gb_preview.X = THIS.Width - 814
gb_print.X   = THIS.Width - 814
gb_paper.X   = THIS.Width - 814

gb_prient.X  = THIS.Width - 768

st_preview.X    = THIS.Width - 754
st_copies.X     = THIS.Width - 754
st_print_zoom.X = THIS.Width - 754
cbx_rulers.X    = THIS.Width - 754
cbx_print_to_file.X = THIS.Width - 754
rb_all.X = THIS.Width - 754
rb_current_page.X   = THIS.Width - 754
rb_pages.X      = THIS.Width - 754

em_zoom2.X  = THIS.Width - 393
em_copies.X = THIS.Width - 393
em_zoom.X   = THIS.Width - 393

rb_z50.X  = THIS.Width - 722
rb_z80.X  = THIS.Width - 722
rb_z100.X = THIS.Width - 722
rb_land.X = THIS.Width - 722
rb_port.X = THIS.Width - 722

rb_z120.X = THIS.Width - 425
rb_z150.X = THIS.Width - 425
rb_z200.X = THIS.Width - 425

sle_range.X = THIS.Width - 658
st_pages1.X = THIS.Width - 658
st_pages2.X = THIS.Width - 658

st_2.X = THIS.Width - 571
st_4.X = THIS.Width - 581
end event

event closequery;Message.StringParm = is_print

end event

event close;//Destroy dw_print_view
end event

type pb_excel from picturebutton within w_print_view_popup
integer x = 1874
integer y = 36
integer width = 302
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "Excel"
boolean originalsize = true
string picturename = "c:\fatimav12\icon\excel_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;Boolean lb_exist
Integer value, li_ret
String  ls_txtname, ls_named

IF dw_1.RowCount() = 0 THEN 
	MessageBox('알림',"Excel File로 저장할 자료가 없습니다!")
	RETURN
END IF

IF gf_messagebox("EXCELQ1") = 2 THEN RETURN

IF GetFileSaveName("Select File", ls_txtname, ls_named, "EXCEL", "EXCEL Files (*.XLS),*.XLS") = 0 THEN RETURN

lb_exist = FileExists(ls_txtname)


IF lb_exist THEN 
	li_ret = MessageBox("Save", "같은 이름의 File이 있습니다!~r덮어 쓰시겠습니까?" + ls_txtname, Question!, YesNoCancel!, 1)
	IF li_ret = 1 THEN
	   dw_1.SaveAs(ls_txtname, EXCEL5! , TRUE)
	ELSEIF li_ret = 2 THEN
		
	ELSEIF li_ret = 3 THEN
		RETURN
	END IF
ELSE
	dw_1.SaveAs(ls_txtname, EXCEL5! , TRUE)
END IF

//IF 1 = MessageBox("Program 실행확인!","현재의 자료를 Excel 프로그램으로 보시겠습니까?", &
//                   Question!, YesNo!, 1) THEN
//						 
//	Run("C:\Program Files\Microsoft Office\Office\EXCEL.EXE " + ls_txtname , Maximized!)
//
//END IF

end event

type pb_exit from picturebutton within w_print_view_popup
integer x = 2798
integer y = 36
integer width = 302
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
boolean originalsize = true
string picturename = "c:\fatimav12\icon\exit_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;Close(Parent)
end event

type pb_setup from picturebutton within w_print_view_popup
integer x = 2482
integer y = 36
integer width = 315
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "설정"
string picturename = "c:\fatimav12\icon\pprint_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;//Clicked script for cb_pr_setup

if PrintSetup( ) = -1 then
	MessageBox("Error!","PrintSetup Failed")
	Return
end if
Parent.Title = dw_1.Describe('datawindow.printer')
end event

type pb_print from picturebutton within w_print_view_popup
integer x = 2181
integer y = 36
integer width = 302
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "출력"
boolean originalsize = true
string picturename = "c:\fatimav12\icon\print_em.bmp"
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;//if IsValid(w_print) then
	string	ls_temp, ls_command, ls_path, ls_filename
	long		row 
	int		value
	
	if rb_all.Checked then  // All?
		ls_temp = ''
	elseif rb_current_page.Checked then// Current page?
		row = dw_1.getrow()
		ls_temp = dw_1.Describe("evaluate('page()',"+string(row)+")")
	else // A range?
		ls_temp = sle_range.text
		IF lenA(ls_temp) = 0 THEN
			MessageBox(	"Print Range - Pages:", &
							"Page range must be entered for this option.", &
							 Exclamation!)
			sle_range.SetFocus()
			RETURN
		END IF
	end if

	IF lenA(ls_temp) > 0 THEN ls_command = ls_command +  " datawindow.print.page.range = '"+ls_temp+"'"
	
	// Number of copies ?
	IF lenA(em_copies.text) > 0 THEN ls_command = ls_command +  " datawindow.print.copies = "+em_copies.text
	
	//화일 프린트?
	IF cbx_print_to_file.checked THEN 	// Print to file ?
		value = dw_1.saveas()
		Return
	END IF
	
	ls_temp = dw_1.Modify(ls_command)
	IF lenA(ls_temp) > 0 THEN // if error THEN display 
		MessageBox('Error Setting Print Options','Error message = ' + ls_temp + '~r~nls_command = ' + ls_command)
		RETURN
	END IF
	
	dw_1.Print( )
	is_Print = 'Y'
//	Close(Parent)
//end if	

end event

type st_1 from statictext within w_print_view_popup
integer x = 59
integer y = 60
integer width = 443
integer height = 68
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
string text = "페이지 이동"
boolean focusrectangle = false
end type

type st_4 from statictext within w_print_view_popup
integer x = 2587
integer y = 1440
integer width = 91
integer height = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 15793151
long backcolor = 15793151
boolean border = true
boolean focusrectangle = false
end type

type st_2 from statictext within w_print_view_popup
integer x = 2597
integer y = 1344
integer width = 69
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 15793151
long backcolor = 15793151
boolean border = true
boolean focusrectangle = false
end type

type em_zoom2 from editmask within w_print_view_popup
event ue_change pbm_enchange
event ue_rezoom pbm_custom01
integer x = 2775
integer y = 276
integer width = 288
integer height = 84
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "100"
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "50~~200"
end type

event ue_change;//This ue_change event will be called any time another radio button is pressed. This is 
//a common area the zoom size is adjusted each time.

//When viewing a datawindow w/labels, the datawindow is automatically placed in print
//preview mode.  This will change the zoom size of the datawindow in print preiew mode.

If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.modify("datawindow.print.preview.zoom = "+this.text)
End If

//rb_custom.checked = true

end event

event ue_rezoom;If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.modify("datawindow.print.preview.zoom = "+this.text)
End If
end event

event modified;this.PostEvent("ue_change")
end event

type rb_z200 from radiobutton within w_print_view_popup
integer x = 2743
integer y = 596
integer width = 233
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "200%"
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '200'

em_zoom2.PostEvent("ue_rezoom")
end event

type rb_z150 from radiobutton within w_print_view_popup
integer x = 2743
integer y = 508
integer width = 233
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "150%"
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '150'

em_zoom2.PostEvent("ue_rezoom")
end event

type rb_z120 from radiobutton within w_print_view_popup
integer x = 2743
integer y = 420
integer width = 265
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "120%"
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '120'

em_zoom2.PostEvent("ue_rezoom")
end event

type rb_z100 from radiobutton within w_print_view_popup
integer x = 2446
integer y = 596
integer width = 233
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "100%"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '100'

em_zoom2.PostEvent("ue_rezoom")
end event

type rb_z80 from radiobutton within w_print_view_popup
integer x = 2446
integer y = 508
integer width = 224
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "80%"
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '80'

em_zoom2.PostEvent("ue_rezoom")
end event

type rb_z50 from radiobutton within w_print_view_popup
integer x = 2446
integer y = 420
integer width = 224
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "50%"
borderstyle borderstyle = stylelowered!
end type

event clicked;em_zoom2.Text = '50'

em_zoom2.PostEvent("ue_rezoom")
end event

type dw_1 from datawindow within w_print_view_popup
integer y = 176
integer width = 2322
integer height = 2144
integer taborder = 20
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type em_zoom from editmask within w_print_view_popup
event ue_change pbm_enchange
event ue_rezoom pbm_custom01
integer x = 2775
integer y = 1056
integer width = 288
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "100"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "50~~200"
end type

event ue_change;//This ue_change event will be called any time another radio button is pressed. This is 
//a common area the zoom size is adjusted each time.

//When viewing a datawindow w/labels, the datawindow is automatically placed in print
//preview mode.  This will change the zoom size of the datawindow in print preiew mode.

If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.Object.Datawindow.zoom = Integer(this.text)
End If

//rb_custom.checked = true

end event

event ue_rezoom;If Integer(This.Text) > 0 and Integer(This.Text) <= 200 Then
	dw_1.Object.datawindow.zoom = Integer(this.text)
End If
end event

event modified;this.PostEvent("ue_change")
end event

type em_copies from editmask within w_print_view_popup
integer x = 2775
integer y = 960
integer width = 288
integer height = 84
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###"
boolean spin = true
double increment = 1
string minmax = "1~~100"
end type

type st_print_zoom from statictext within w_print_view_popup
integer x = 2414
integer y = 1068
integer width = 347
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "배율 :"
boolean focusrectangle = false
end type

type st_copies from statictext within w_print_view_popup
integer x = 2414
integer y = 976
integer width = 247
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "장수 :"
boolean focusrectangle = false
end type

type rb_land from radiobutton within w_print_view_popup
integer x = 2446
integer y = 1432
integer width = 507
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "     가로"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	dw_1.Object.datawindow.Print.orientation = 1
end if
end event

type rb_port from radiobutton within w_print_view_popup
integer x = 2446
integer y = 1348
integer width = 507
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "     세로"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	dw_1.Object.datawindow.Print.orientation = 2
end if
end event

type gb_prient from groupbox within w_print_view_popup
integer x = 2400
integer y = 1264
integer width = 658
integer height = 272
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "출력위치"
borderstyle borderstyle = stylelowered!
end type

type rb_all from radiobutton within w_print_view_popup
integer x = 2414
integer y = 1700
integer width = 251
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "전부"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,128,128)
	st_pages2.TextColor = rgb(128,128,128)
	sle_range.BackColor = rgb(192,192,192)
	sle_range.Enabled = FALSE
end if
//wf_page_range(this)
end event

type rb_current_page from radiobutton within w_print_view_popup
integer x = 2414
integer y = 1784
integer width = 466
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "현재 면"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,128,128)
	st_pages2.TextColor = rgb(128,128,128)
	sle_range.BackColor = rgb(192,192,192)
	sle_range.Enabled = FALSE
end if
//wf_page_range(this)
end event

type rb_pages from radiobutton within w_print_view_popup
integer x = 2414
integer y = 1876
integer width = 494
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "페이지 선택"
borderstyle borderstyle = stylelowered!
end type

event clicked;if this.Checked then
	st_pages1.TextColor = rgb(128,0,0)
	st_pages2.TextColor = rgb(128,0,0)
	sle_range.BackColor = rgb(255,255,255)
	sle_range.Enabled = TRUE
	sle_range.SetFocus()
end if
//wf_page_range(this)
end event

type sle_range from singlelineedit within w_print_view_popup
integer x = 2510
integer y = 1960
integer width = 558
integer height = 92
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

type st_pages1 from statictext within w_print_view_popup
integer x = 2510
integer y = 2068
integer width = 535
integer height = 112
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8421504
long backcolor = 67108864
string text = "출력할 구간을 입력하세요"
boolean focusrectangle = false
end type

type st_pages2 from statictext within w_print_view_popup
integer x = 2510
integer y = 2196
integer width = 370
integer height = 56
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8421504
long backcolor = 67108864
string text = "Ex. 1, 3, 5-10"
boolean focusrectangle = false
end type

type gb_paper from groupbox within w_print_view_popup
integer x = 2354
integer y = 1604
integer width = 750
integer height = 692
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "출력 구간"
borderstyle borderstyle = stylelowered!
end type

type cbx_print_to_file from checkbox within w_print_view_popup
integer x = 2414
integer y = 1164
integer width = 494
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "파일로 출력"
borderstyle borderstyle = stylelowered!
end type

type gb_print from groupbox within w_print_view_popup
integer x = 2354
integer y = 876
integer width = 750
integer height = 696
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "출력 설정"
borderstyle borderstyle = stylelowered!
end type

type cbx_rulers from checkbox within w_print_view_popup
integer x = 2414
integer y = 732
integer width = 279
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "Rulers"
boolean checked = true
borderstyle borderstyle = stylelowered!
end type

event clicked;IF THIS.Checked THEN
	dw_1.modify("datawindow.print.Preview.Rulers	= YES")
ELSE
	dw_1.modify("datawindow.print.Preview.Rulers	= NO")
END IF
end event

type pb_first from picturebutton within w_print_view_popup
integer x = 535
integer y = 52
integer width = 155
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "c:\fatimav12\icon\first_s.bmp"
alignment htextalign = left!
end type

event clicked;dw_1.ScrollToRow(1)
end event

type pb_prior from picturebutton within w_print_view_popup
integer x = 690
integer y = 52
integer width = 155
integer height = 88
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "c:\fatimav12\icon\prior1_es.bmp"
alignment htextalign = left!
end type

event clicked;Dw_1.ScrollPriorPage()
end event

type pb_next from picturebutton within w_print_view_popup
integer x = 846
integer y = 52
integer width = 155
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "c:\fatimav12\icon\next1_s.bmp"
alignment htextalign = left!
end type

event clicked;Dw_1.ScrollNextPage()
end event

type pb_last from picturebutton within w_print_view_popup
integer x = 1001
integer y = 52
integer width = 155
integer height = 88
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "c:\fatimav12\icon\last_s.bmp"
alignment htextalign = left!
end type

event clicked;dw_1.ScrollToRow(dw_1.RowCount())
end event

type st_6 from statictext within w_print_view_popup
integer x = 526
integer y = 44
integer width = 635
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_preview from statictext within w_print_view_popup
integer x = 2414
integer y = 296
integer width = 242
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
string text = "배율 :"
boolean focusrectangle = false
end type

type gb_preview from groupbox within w_print_view_popup
integer x = 2354
integer y = 184
integer width = 750
integer height = 660
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 79741120
string text = "미리보기 설정"
borderstyle borderstyle = stylelowered!
end type

