﻿$PBExportHeader$w_message_display.srw
forward
global type w_message_display from window
end type
type pb_print from picturebutton within w_message_display
end type
type pb_next from picturebutton within w_message_display
end type
type pb_prior from picturebutton within w_message_display
end type
type pb_exit from picturebutton within w_message_display
end type
type dw_data_show from datawindow within w_message_display
end type
type st_1 from statictext within w_message_display
end type
end forward

global type w_message_display from window
boolean visible = false
integer width = 2459
integer height = 1924
boolean titlebar = true
string title = "[ 전달사항]"
boolean controlmenu = true
boolean hscrollbar = true
windowtype windowtype = response!
long backcolor = 12632256
boolean clientedge = true
event ue_keydown pbm_dwnkey
pb_print pb_print
pb_next pb_next
pb_prior pb_prior
pb_exit pb_exit
dw_data_show dw_data_show
st_1 st_1
end type
global w_message_display w_message_display

type variables
DataStore ids_message
string is_ini,is_progname,is_sabun,is_maxdate
int row_no = 1 

end variables

on w_message_display.create
this.pb_print=create pb_print
this.pb_next=create pb_next
this.pb_prior=create pb_prior
this.pb_exit=create pb_exit
this.dw_data_show=create dw_data_show
this.st_1=create st_1
this.Control[]={this.pb_print,&
this.pb_next,&
this.pb_prior,&
this.pb_exit,&
this.dw_data_show,&
this.st_1}
end on

on w_message_display.destroy
destroy(this.pb_print)
destroy(this.pb_next)
destroy(this.pb_prior)
destroy(this.pb_exit)
destroy(this.dw_data_show)
destroy(this.st_1)
end on

event open;Integer	li_cnt, li_lastrow 
String	ls_date, ls_imsi  
DateTime ldt_mdate

is_ini = 'C:\fatimav12\config\Postmessage.ini'
IF FileExists(is_ini) = False THEN
	integer li_FileNum
	li_FileNum = FileOpen(is_ini, StreamMode!, Write!, LockWrite!, Replace!)
	FileClose(li_FileNum)
END IF

ls_imsi		= Message.StringParm
is_progname = Upper(Trim(leftA(ls_imsi, 7))) 
is_sabun 	= Trim(midA(ls_imsi, 8, 15)) 

IF isnull(is_progname) or is_progname <= ' ' THEN 
	messagebox("is_program","프로그램 명칭, 사번을 알 수 없습니다.") 
	Close (This)
	Return
END IF

ls_date = ProfileString(is_ini, is_progname, is_sabun, "2000/01/01 00:00:01")

li_cnt = 0

SELECT count(mdate)	INTO :li_cnt 
FROM tz_postmsg  
WHERE progname = :is_progname AND mdate > to_date(:ls_date, 'YYYY/MM/DD HH24:MI:SS')  
USING SQLCA ;
IF li_cnt = 0 THEN 
	Close (This)
	Return
ELSE
	SELECT to_char(max(mdate),'YYYY/MM/DD') || ' ' || to_char(max(mdate),'HH24:MI:SS') 
		INTO :is_maxdate 
		FROM tz_postmsg  
		WHERE progname = :is_progname AND mdate > to_date(:ls_date, 'YYYY/MM/DD HH24:MI:SS')  
		USING SQLCA ;
END IF

SetProfileString (is_ini, is_progname, is_sabun, is_maxdate)

gf_center(this) 

this.visible = True 

dw_data_show.SetTransObject(SQLCA)
dw_data_show.Retrieve (is_progname)

li_lastrow = dw_data_show.Rowcount() 
dw_data_show.ScrollToRow (li_lastrow) 
//dw_data_show.SelectRow (li_lastrow, True) 


end event

type pb_print from picturebutton within w_message_display
integer x = 1778
integer y = 20
integer width = 302
integer height = 104
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "출력"
string picturename = "c:\fatimav12\icon\print_em.bmp"
string disabledname = "c:\fatimav12\icon\print_dm.bmp"
alignment htextalign = right!
end type

event clicked;if messagebox("확인!","출력하시겠습니까?", Question!, YesNo!, 1) = 2 then return 

integer	row 
string	ls_temp, ls_command 

row = dw_data_show.getrow()

string	err
err = dw_data_show.Modify("DataWindow.Print.page.range= '"+string(row)+"'")
IF err <> "" THEN
		MessageBox("Status", "DataWindow.Print.page.range " + err)
		RETURN
END IF

dw_data_show.Print()

end event

type pb_next from picturebutton within w_message_display
integer x = 302
integer y = 20
integer width = 261
integer height = 104
integer taborder = 30
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = ">>"
string picturename = " "
string disabledname = " "
end type

event clicked;dw_data_show.ScrollNextRow()

end event

type pb_prior from picturebutton within w_message_display
integer x = 32
integer y = 20
integer width = 261
integer height = 104
integer taborder = 30
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "<<"
string picturename = " "
string disabledname = " "
end type

event clicked;dw_data_show.ScrollPriorRow ( )

end event

type pb_exit from picturebutton within w_message_display
integer x = 2085
integer y = 20
integer width = 302
integer height = 104
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "종료"
string picturename = "c:\fatimav12\icon\exit_em.bmp"
string disabledname = "c:\fatimav12\icon\exit_dm.bmp"
alignment htextalign = right!
end type

event clicked;close (parent) 

end event

type dw_data_show from datawindow within w_message_display
integer x = 14
integer y = 148
integer width = 2409
integer height = 1608
integer taborder = 20
string dataobject = "d_postmsg_main"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event itemchanged;CHOOSE CASE dwo.name
	CASE 'progname'
		this.setcolumn("message")
END CHOOSE

end event

type st_1 from statictext within w_message_display
integer x = 18
integer y = 8
integer width = 2400
integer height = 128
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = right!
boolean focusrectangle = false
end type

