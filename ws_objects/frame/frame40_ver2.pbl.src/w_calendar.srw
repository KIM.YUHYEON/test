﻿$PBExportHeader$w_calendar.srw
$PBExportComments$날짜 선택 달력
forward
global type w_calendar from window
end type
type sle_date from singlelineedit within w_calendar
end type
type pb_4 from picturebutton within w_calendar
end type
type pb_3 from picturebutton within w_calendar
end type
type pb_2 from picturebutton within w_calendar
end type
type pb_1 from picturebutton within w_calendar
end type
type dw_calendar from datawindow within w_calendar
end type
type st_1 from statictext within w_calendar
end type
type st_2 from statictext within w_calendar
end type
type st_3 from statictext within w_calendar
end type
end forward

global type w_calendar from window
integer x = 1074
integer y = 484
integer width = 1728
integer height = 1168
boolean titlebar = true
string title = "Calendar"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 79741120
sle_date sle_date
pb_4 pb_4
pb_3 pb_3
pb_2 pb_2
pb_1 pb_1
dw_calendar dw_calendar
st_1 st_1
st_2 st_2
st_3 st_3
end type
global w_calendar w_calendar

type variables
String is_date
Boolean ib_sw
DataStore ids_date

int ro = 1,co = 1
String days[]
Boolean downflag = False
end variables

forward prototypes
public subroutine wf_calendar ()
end prototypes

public subroutine wf_calendar ();IF IsDate(sle_date.Text + "/01") = False OR lenA(sle_date.Text) <> 7 THEN
	MessageBox ("YYYY/MM", "원하는 연도와 월을 Type에 맞게 입력하시기 바랍니다.")
	Return
END IF

Integer		i, row, day, li_day, li_maxday
String 		ls_date, ls_maxdate, ls_select
DataStore	lds_holiday

dw_calendar.Reset()

FOR	i=1	TO		6
	dw_calendar.InsertRow(0)
NEXT

// 이번달의 마지막 일자
ls_date = sle_date.Text + "/01"

SELECT TO_CHAR(LAST_DAY(TO_DATE(:ls_date, 'YYYY/MM/DD')), 'YYYYMMDD') 
INTO   :ls_maxdate
FROM  dual 
USING SQLCA;

li_maxday = Integer(rightA(ls_maxdate, 2))

// 이번달의 1일은 무슨 요일인가?
li_day = Daynumber(Date(ls_date))

// 일자 Setting
DO UNTIL day >= li_maxday
	row++
	FOR	i=1	TO		7
		IF	row > 1 OR i >= li_day THEN
			day++
			IF day > li_maxday THEN Exit
			dw_calendar.Object.Data[row, i] = String(day)
		END IF
	NEXT
LOOP


// 휴일 Setting
lds_holiday = Create DataStore
lds_holiday.DataObject = "d_sys_holiday"

lds_holiday.SetTransObject (SQLCA)
row = lds_holiday.Retrieve (leftA(sle_date.Text, 4)+rightA(sle_date.Text, 2) + '%')


IF row > 0 THEN
	FOR	i=1	TO		lds_holiday.RowCount()
		row = Integer(rightA(lds_holiday.Object.hdate[i], 2))
		ls_date = leftA(lds_holiday.Object.hdate[i], 4)+"/"+midA(lds_holiday.Object.hdate[i], 5, 2)+"/"+rightA(lds_holiday.Object.hdate[i], 2)
		day = Daynumber(Date(ls_date))
		dw_calendar.Object.Data[Integer((row + li_day - 2) / 7) + 1, day + 7] = Trim(lds_holiday.Object.hname[i])
	NEXT
END IF
	
Destroy lds_holiday

//당일 표시
IF sle_date.Text = leftA(is_date, 7) THEN
	dw_calendar.Object.Today[Integer((Integer(rightA(is_date, 2)) + li_day - 2) / 7) + 1] = &
		String(Daynumber(Date(is_date)))

	ro = Integer((Integer(rightA(is_date, 2)) + li_day - 2) / 7) + 1
	co = Daynumber(Date(is_date))

	dw_calendar.SetItem(ro,"col",co)
	dw_calendar.SetRow(ro)
	dw_calendar.ScrollToRow(ro)
END IF


ib_sw = False

end subroutine

on w_calendar.create
this.sle_date=create sle_date
this.pb_4=create pb_4
this.pb_3=create pb_3
this.pb_2=create pb_2
this.pb_1=create pb_1
this.dw_calendar=create dw_calendar
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.Control[]={this.sle_date,&
this.pb_4,&
this.pb_3,&
this.pb_2,&
this.pb_1,&
this.dw_calendar,&
this.st_1,&
this.st_2,&
this.st_3}
end on

on w_calendar.destroy
destroy(this.sle_date)
destroy(this.pb_4)
destroy(this.pb_3)
destroy(this.pb_2)
destroy(this.pb_1)
destroy(this.dw_calendar)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
end on

event open;String ls_select


days[1] = 'sun'
days[2] = 'mon'
days[3] = 'tue'
days[4] = 'wed'
days[5] = 'thu'
days[6] = 'fri'
days[7] = 'sat'


ls_select = Message.StringParm

IF lenA(ls_select) = 8 AND IsNumber(ls_select) THEN
	is_date = leftA(ls_select, 4) + "/" + midA(ls_select, 5, 2) + "/" + rightA(ls_select, 2)
ELSE	
	
	SELECT TO_CHAR(sysdate,'YYYYMMDD')
	INTO   :ls_select
	FROM   dual
	USING SQLCA ;
	
	is_date = leftA(ls_select, 4) + "/" + midA(ls_select, 5, 2) + "/" + rightA(ls_select, 2)

END IF

ib_sw = True

sle_date.Text = leftA(is_date, 7)


end event

type sle_date from singlelineedit within w_calendar
event datachange pbm_enchange
integer x = 704
integer y = 20
integer width = 338
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

event datachange;IF ib_sw = False THEN Return

wf_calendar ()

end event

event modified;wf_calendar ()

end event

type pb_4 from picturebutton within w_calendar
integer x = 1408
integer y = 16
integer width = 279
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "연도 >>"
boolean originalsize = true
alignment htextalign = left!
vtextalign vtextalign = vcenter!
end type

event clicked;String ls_select, ls_date

ib_sw = True

ls_date = sle_date.Text + "/01"

SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:ls_date, 'YYYY/MM/DD'), 12), 'YYYY/MM') 
INTO   :ls_select
FROM dual 
USING SQLCA;

sle_date.Text = ls_select

end event

type pb_3 from picturebutton within w_calendar
integer x = 1221
integer y = 16
integer width = 187
integer height = 96
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "월 >"
boolean originalsize = true
alignment htextalign = left!
vtextalign vtextalign = vcenter!
end type

event clicked;String ls_select, ls_date

ib_sw = True

ls_date = sle_date.Text + "/01"

SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:ls_date, 'YYYY/MM/DD'), 1), 'YYYY/MM') 
INTO   :ls_select
FROM dual 
USING SQLCA;

sle_date.Text = ls_select

end event

type pb_2 from picturebutton within w_calendar
integer x = 311
integer y = 16
integer width = 187
integer height = 96
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "< 월"
boolean originalsize = true
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;String ls_select, ls_date

ib_sw = True

ls_date = sle_date.Text + "/01"

SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:ls_date, 'YYYY/MM/DD'), -1), 'YYYY/MM') 
INTO   :ls_select
FROM dual 
USING SQLCA;

sle_date.Text = ls_select

end event

type pb_1 from picturebutton within w_calendar
integer x = 32
integer y = 16
integer width = 279
integer height = 96
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "<< 연도"
boolean originalsize = true
alignment htextalign = right!
vtextalign vtextalign = vcenter!
end type

event clicked;String ls_select, ls_date

ib_sw = True

ls_date = sle_date.Text + "/01"

SELECT TO_CHAR(ADD_MONTHS(TO_DATE(:ls_date, 'YYYY/MM/DD'), -12), 'YYYY/MM') 
INTO   :ls_select
FROM dual 
USING SQLCA;

sle_date.Text = ls_select

end event

type dw_calendar from datawindow within w_calendar
event we_mousemove pbm_dwnmousemove
event ue_lbuttonup pbm_dwnlbuttonup
event ue_lbuttondown pbm_lbuttondown
integer x = 32
integer y = 140
integer width = 1650
integer height = 920
string dataobject = "d_sys_calendar"
borderstyle borderstyle = stylelowered!
end type

event we_mousemove;int no

IF downflag = False THEN Return
	
co = PointerX() / (this.Width / 7) + 1
ro = (PointerY() - 109) / ( 821 / 6 ) + 1
dw_calendar.SetItem(ro,"col",co)
dw_calendar.SetRow(ro)
dw_calendar.ScrollToRow(ro)

end event

event ue_lbuttonup;downflag = False
end event

event ue_lbuttondown;downflag = True
end event

event doubleclicked;//String ls_dd, st
//
//
//IF row = 0 THEN Return
////lenA(String(st)) > 3 
//
//st = days[co]
//
//
//ls_dd = rightA("0" + GetItemString(row, st), 2)
//IF IsNull(ls_dd) OR Trim(ls_dd) = '' THEN Return
//
//CloseWithReturn (Parent, leftA(sle_date.Text, 4) + rightA(sle_date.Text, 2) + ls_dd)
//
end event

event clicked;int no
String ls_dd, st

IF row < 1 THEN Return
	
co = PointerX() / (this.Width / 7) + 1
ro = row
dw_calendar.SetItem(ro,"col",co)

IF row = 0 THEN Return

st = days[co]

ls_dd = rightA("0" + GetItemString(row, st), 2)
IF IsNull(ls_dd) OR Trim(ls_dd) = '' THEN Return

CloseWithReturn (Parent, leftA(sle_date.Text, 4) + rightA(sle_date.Text, 2) + ls_dd)


end event

type st_1 from statictext within w_calendar
integer x = 18
integer y = 128
integer width = 1678
integer height = 944
integer textsize = -12
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_2 from statictext within w_calendar
integer x = 23
integer y = 8
integer width = 480
integer height = 108
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 79741120
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_3 from statictext within w_calendar
integer x = 1211
integer y = 8
integer width = 480
integer height = 108
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 79741120
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

