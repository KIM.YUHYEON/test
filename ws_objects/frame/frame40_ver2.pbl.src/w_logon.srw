﻿$PBExportHeader$w_logon.srw
$PBExportComments$Logon화면메시지포함
forward
global type w_logon from window
end type
type cb_next from commandbutton within w_logon
end type
type cb_pri from commandbutton within w_logon
end type
type dw_miso from datawindow within w_logon
end type
type sle_c from singlelineedit within w_logon
end type
type sle_n from singlelineedit within w_logon
end type
type sle_o from singlelineedit within w_logon
end type
type cb_pok from commandbutton within w_logon
end type
type cb_pcancel from commandbutton within w_logon
end type
type st_7 from statictext within w_logon
end type
type st_6 from statictext within w_logon
end type
type st_4 from statictext within w_logon
end type
type cbx_pw from checkbox within w_logon
end type
type cbx_hd from checkbox within w_logon
end type
type cbx_er from checkbox within w_logon
end type
type sle_passwd from singlelineedit within w_logon
end type
type sle_id from singlelineedit within w_logon
end type
type cb_cancel from commandbutton within w_logon
end type
type cb_ok from commandbutton within w_logon
end type
type st_3 from statictext within w_logon
end type
type st_2 from statictext within w_logon
end type
type st_1 from statictext within w_logon
end type
type st_8 from statictext within w_logon
end type
type st_5 from statictext within w_logon
end type
type p_miso from picture within w_logon
end type
type p_2 from picture within w_logon
end type
type dw_boarderview from datawindow within w_logon
end type
type dw_boarderlist from datawindow within w_logon
end type
end forward

global type w_logon from window
integer width = 4123
integer height = 2700
boolean titlebar = true
string title = "사용자 확인"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 32571387
boolean contexthelp = true
cb_next cb_next
cb_pri cb_pri
dw_miso dw_miso
sle_c sle_c
sle_n sle_n
sle_o sle_o
cb_pok cb_pok
cb_pcancel cb_pcancel
st_7 st_7
st_6 st_6
st_4 st_4
cbx_pw cbx_pw
cbx_hd cbx_hd
cbx_er cbx_er
sle_passwd sle_passwd
sle_id sle_id
cb_cancel cb_cancel
cb_ok cb_ok
st_3 st_3
st_2 st_2
st_1 st_1
st_8 st_8
st_5 st_5
p_miso p_miso
p_2 p_2
dw_boarderview dw_boarderview
dw_boarderlist dw_boarderlist
end type
global w_logon w_logon

type prototypes
//FTP관련 함수들...
FUNCTION INT FTPConnect (String IP_ADDRESS, String USER_ID, String PASSWORD) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPConnect;Ansi"
FUNCTION INT FTPDisconnect ( ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
FUNCTION INT FTPgetCurrentDir (REF BLOB DATA) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
FUNCTION INT FTPsetCurrentDirUp () LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
FUNCTION INT FTPsetCurrentDir (String CURR_DIR) LIBRARY "FTP.DLL" alias for "C:\fatimav12\mgdll\FTPsetCurrentDir;Ansi"
FUNCTION INT FTPfileList ( REF BLOB DATA ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
FUNCTION INT FTPgetFile ( String REMOTE_FILE, String LOCAL_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPgetFile;Ansi"
FUNCTION INT FTPcreateDir ( String NEW_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPcreateDir;Ansi"
FUNCTION INT FTPdeleteFile ( String DEL_FILE ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPdeleteFile;Ansi"
FUNCTION INT FTPdeleteDir ( String DEL_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPdeleteDir;Ansi"
FUNCTION INT FTPrenameDir ( String CURR_DIR, String NEW_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPrenameDir;Ansi"
FUNCTION INT FTPputFile ( String LOCAL_FILE, String REMOTE_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPputFile;Ansi"
FUNCTION INT FTPping ( String IPaddress ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPping;Ansi"

end prototypes

type variables
Window iw_parent
integer ii_pass
string is_sabun
String is_ini, is_oldid
boolean ib_ini
long   il_misocnt, il_pmiso

string is_ftp = '200.1.1.7', is_ftpid = 'fatima', is_ftppwd = 'fatima16'
end variables

forward prototypes
public subroutine wf_getname (character id)
public function integer wf_ftpcon ()
public function integer wf_ftpdiscon ()
end prototypes

public subroutine wf_getname (character id);
end subroutine

public function integer wf_ftpcon ();Int li_rtn

li_rtn = FTPConnect(is_ftp, is_ftpid, is_ftppwd)

return li_rtn
end function

public function integer wf_ftpdiscon ();Int li_rtn

li_rtn = FTPDisconnect()

return li_rtn
end function

on w_logon.create
this.cb_next=create cb_next
this.cb_pri=create cb_pri
this.dw_miso=create dw_miso
this.sle_c=create sle_c
this.sle_n=create sle_n
this.sle_o=create sle_o
this.cb_pok=create cb_pok
this.cb_pcancel=create cb_pcancel
this.st_7=create st_7
this.st_6=create st_6
this.st_4=create st_4
this.cbx_pw=create cbx_pw
this.cbx_hd=create cbx_hd
this.cbx_er=create cbx_er
this.sle_passwd=create sle_passwd
this.sle_id=create sle_id
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.st_8=create st_8
this.st_5=create st_5
this.p_miso=create p_miso
this.p_2=create p_2
this.dw_boarderview=create dw_boarderview
this.dw_boarderlist=create dw_boarderlist
this.Control[]={this.cb_next,&
this.cb_pri,&
this.dw_miso,&
this.sle_c,&
this.sle_n,&
this.sle_o,&
this.cb_pok,&
this.cb_pcancel,&
this.st_7,&
this.st_6,&
this.st_4,&
this.cbx_pw,&
this.cbx_hd,&
this.cbx_er,&
this.sle_passwd,&
this.sle_id,&
this.cb_cancel,&
this.cb_ok,&
this.st_3,&
this.st_2,&
this.st_1,&
this.st_8,&
this.st_5,&
this.p_miso,&
this.p_2,&
this.dw_boarderview,&
this.dw_boarderlist}
end on

on w_logon.destroy
destroy(this.cb_next)
destroy(this.cb_pri)
destroy(this.dw_miso)
destroy(this.sle_c)
destroy(this.sle_n)
destroy(this.sle_o)
destroy(this.cb_pok)
destroy(this.cb_pcancel)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_4)
destroy(this.cbx_pw)
destroy(this.cbx_hd)
destroy(this.cbx_er)
destroy(this.sle_passwd)
destroy(this.sle_id)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.st_8)
destroy(this.st_5)
destroy(this.p_miso)
destroy(this.p_2)
destroy(this.dw_boarderview)
destroy(this.dw_boarderlist)
end on

event open;Integer li_cnt
string ls_name, ls_chk
string ls_title

This.Title = gf_hospital_data('Z01name') + ' - 사용자 확인'

ib_ini = False

cbx_pw.TriggerEvent(Clicked!)
gf_center(this)

dw_boarderlist.Retrieve()
//this.Y = this.Y - 400

dw_miso.Retrieve()

is_ini = Message.StringParm

IF Upper(leftA(is_ini, 12)) = "C:\fatimav12" THEN
	ib_ini = True
END IF

//응급실, 인공신장실 체크
SetProfileString(is_ini, "User", "ER", '0')
SetProfileString(is_ini, "User", "HD", '0')
ls_chk = rightA(is_ini, 2)
IF IsNumber(ls_chk) THEN
	IF leftA(ls_chk, 1) = '1' THEN
		cbx_er.Visible = True
		SetProfileString(is_ini, "User", "ER", '1')
	END IF
	IF rightA(ls_chk, 1) = '1' THEN
		cbx_hd.Visible = True
		SetProfileString(is_ini, "User", "HD", '1')
	END IF
	is_ini = leftA(is_ini, lenA(is_ini) -2)
END IF

IF ib_ini AND FileExists(is_ini) = False THEN
	integer li_FileNum
	li_FileNum = FileOpen(is_ini, StreamMode!, Write!, LockWrite!, Replace!)
	FileClose(li_FileNum)
END IF

is_sabun = "ERROR"
IF ib_ini THEN
	is_oldid = ProfileString(is_ini,"User","Userid","")
	sle_id.text = is_oldid
	SELECT tb_employee.name
		INTO :ls_name
		FROM tb_employee
		WHERE tb_employee.sabun = :is_oldid
		USING SQLCA ;

	IF	ls_name = "" THEN		
		sle_id.Setfocus()
		Return
	END IF
	sle_id.text = leftA(is_oldid+"               ",12)+ls_name
	sle_passwd.setfocus()

	ii_pass = 0
	cbx_pw.Enabled = True
ELSE
	sle_id.Setfocus()
END IF
//
end event

event systemkey;IF keydown(KeyAlt!) THEN
	IF keydown(ASC("U")) THEN
		sle_id.SetFocus()
	ELSEIF keydown(ASC("P")) THEN
		sle_passwd.SetFocus()
	END IF
	
	IF cbx_pw.Checked THEN
		IF keydown(ASC("O")) THEN
			sle_o.SetFocus()
		ELSEIF keydown(ASC("N")) THEN
			sle_n.SetFocus()
		ELSEIF keydown(ASC("C")) THEN
			sle_c.SetFocus()
		END IF
	END IF
END IF

end event

event closequery;IF is_sabun = 'ERROR' THEN
	Message.StringParm = ''
	SetProfileString(is_ini, "User", "ER", '0')
	SetProfileString(is_ini, "User", "HD", '0')
ELSE
	IF cbx_er.Checked THEN 
		SetProfileString(is_ini, "User", "ER", '1')
	ELSE
		SetProfileString(is_ini, "User", "ER", '0')
	END IF

	IF cbx_hd.Checked THEN 
		SetProfileString(is_ini, "User", "HD", '1')
	ELSE
		SetProfileString(is_ini, "User", "HD", '0')
	END IF
	
	// tz_w_logn_history INSERT (2011-10-12 : Kim Jin Yeong)
	String	ls_programId
	
	ls_programId =  GetApplication().appname
	
	insert into tz_w_logn_history
			 (sabun, programid)
	select :is_sabun, :ls_programid
	from dual
	where not exists (select 1 from tz_w_logn_history
	                  where sabun = :is_sabun
							and programid = :ls_programid)
	using SQLCA;	
	gf_sqlerror2(SQLCA, True)
	
	Message.StringParm = is_sabun
END IF

end event

type cb_next from commandbutton within w_logon
integer x = 992
integer y = 2452
integer width = 969
integer height = 112
integer taborder = 140
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
boolean enabled = false
string text = "다음 ▷"
end type

event clicked;il_pmiso   = il_pmiso + 1
if il_pmiso = il_misocnt then
	This.Enabled = False
end if
if il_pmiso < il_misocnt then
	cb_pri.Enabled = True
end if

string docname, ls_filename, ls_dir
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_dir      = "c:\fatimav12\"
docname     = ls_dir + '\' + ls_filename
If FileExists (ls_dir) Then 
else
//   CreateDirectoryA (ls_dir, 0)
end if
SELECTBLOB filex
   	INTO :Emp_id_pic
      FROM ty_boardfile
 	   WHERE fileseq = :ll_fileseq
	   USING sqlca ;
			
lb_pic = emp_id_pic
If FileExists (docname) Then
   FileDelete(docname) 
end if
   		 
li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
ll_pic =1
bytes_read = lenA(emp_id_pic)
		 
IF bytes_read > 32765 THEN
   IF Mod(bytes_read, 32765) = 0 THEN
      loops = bytes_read/32765
	ELSE
   	loops = (bytes_read/32765) + 1
   END IF
ELSE
   loops = 1
END IF
   	// Read the file
FOR i = 1 to loops
    ll_rtn = FileWrite(li_FileNum, lb_pic)
    if ll_rtn = 32765 then
	    lb_pic = Blobmid(lb_pic, 32766)
    end if
NEXT
fileclose(li_FileNum)
	
p_miso.picturename = docname
p_miso.visible = True

end event

type cb_pri from commandbutton within w_logon
integer x = 23
integer y = 2452
integer width = 969
integer height = 112
integer taborder = 120
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "◁ 이전"
end type

event clicked;il_pmiso   = il_pmiso - 1
if il_pmiso = 1 then
	This.Enabled = False
end if
if il_pmiso < il_misocnt then
	cb_next.Enabled = True
end if

string docname, ls_filename, ls_dir
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_dir      = "c:\fatimav12\temp"
docname     = ls_dir + '\' + ls_filename
If FileExists (ls_dir) Then 
else
//   CreateDirectoryA (ls_dir, 0)
end if
SELECTBLOB filex
   	INTO :Emp_id_pic
      FROM ty_boardfile
 	   WHERE fileseq = :ll_fileseq
	   USING sqlca ;
			
lb_pic = emp_id_pic
If FileExists (docname) Then
   FileDelete(docname) 
end if
   		 
li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
ll_pic =1
bytes_read = lenA(emp_id_pic)
		 
IF bytes_read > 32765 THEN
   IF Mod(bytes_read, 32765) = 0 THEN
      loops = bytes_read/32765
	ELSE
   	loops = (bytes_read/32765) + 1
   END IF
ELSE
   loops = 1
END IF
   	// Read the file
FOR i = 1 to loops
    ll_rtn = FileWrite(li_FileNum, lb_pic)
    if ll_rtn = 32765 then
	    lb_pic = Blobmid(lb_pic, 32766)
    end if
NEXT
fileclose(li_FileNum)
	
p_miso.picturename = docname
p_miso.visible = True


end event

type dw_miso from datawindow within w_logon
boolean visible = false
integer x = 1230
integer y = 1616
integer width = 1070
integer height = 600
integer taborder = 130
string title = "none"
string dataobject = "d_boardermisolist"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;SetTransObject(sqlca)
end event

event retrieveend;if rowcount < 0 then 
	return
end if
il_misocnt = rowcount
il_pmiso   = rowcount

if il_pmiso = 1 then
	cb_pri.enabled = false
end if
string docname, ls_filename, ls_dir
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_dir      = "c:\fatimav12\temp"
docname     = ls_dir + '\' + ls_filename
If FileExists (ls_dir) Then 
else
//   CreateDirectoryA (ls_dir, 0)
end if
SELECTBLOB filex
   	INTO :Emp_id_pic
      FROM ty_boardfile
 	   WHERE fileseq = :ll_fileseq
	   USING sqlca ;
			
lb_pic = emp_id_pic
If FileExists (docname) Then
   FileDelete(docname) 
end if
   		 
li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
ll_pic =1
bytes_read = lenA(emp_id_pic)
		 
IF bytes_read > 32765 THEN
   IF Mod(bytes_read, 32765) = 0 THEN
      loops = bytes_read/32765
	ELSE
   	loops = (bytes_read/32765) + 1
   END IF
ELSE
   loops = 1
END IF
   	// Read the file
FOR i = 1 to loops
    ll_rtn = FileWrite(li_FileNum, lb_pic)
    if ll_rtn = 32765 then
	    lb_pic = Blobmid(lb_pic, 32766)
    end if
NEXT
fileclose(li_FileNum)
	
p_miso.picturename = docname
p_miso.visible = True

end event

type sle_c from singlelineedit within w_logon
event keydwon pbm_keydown
integer x = 2592
integer y = 624
integer width = 1184
integer height = 84
integer taborder = 90
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydwon(keycode key, unsignedlong keyflags);IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	cb_pok.TriggerEvent(clicked!)
END IF
Return 0

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

type sle_n from singlelineedit within w_logon
event keydwon pbm_keydown
integer x = 2592
integer y = 524
integer width = 1184
integer height = 84
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydwon(keycode key, unsignedlong keyflags);IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	sle_c.SetFocus()
END IF
Return 0

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

type sle_o from singlelineedit within w_logon
event keydwon pbm_keydown
integer x = 2592
integer y = 428
integer width = 1184
integer height = 84
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydwon(keycode key, unsignedlong keyflags);string ls_sabun, ls_passno
IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	ls_sabun = Trim(leftA(parent.sle_id.text,8))
	SELECT tb_employee.passno
		INTO :ls_passno
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun
		USING SQLCA ;
	IF isnull(ls_passno) THEN ls_passno = ''

	IF Upper(sle_o.Text) <> Upper(ls_passno) THEN
		MessageBox("확인","기존암호가 다릅니다.~r~n 다시 입력하시기 바랍니다.")
		sle_o.SetFocus()
		Return 0
	ELSE
		sle_n.setfocus()
	END IF
END IF

Return 0

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

type cb_pok from commandbutton within w_logon
integer x = 3794
integer y = 480
integer width = 261
integer height = 104
integer taborder = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "확인"
end type

event clicked;String ls_sabun,ls_name,ls_passwd,ls_chk
Integer Let

ls_sabun = Trim(leftA(sle_id.text,8))
IF ls_sabun = "" THEN
	MessageBox(gf_msg("H0001"), gf_msg("E0010"))
	sle_id.Setfocus()
	Return
END IF

SELECT tb_employee.passno, tb_employee.name 
	INTO :ls_passwd, :ls_name
	FROM tb_employee
	WHERE tb_employee.sabun = :ls_sabun
	USING SQLCA ;

IF	ls_name = "" THEN
	MessageBox(gf_msg("H0001"), gf_msg("E0010"))
	sle_id.Setfocus()
	Return
END IF

sle_id.text = leftA(ls_sabun+"               ",12)+ls_name

IF isnull(ls_passwd) THEN
	ls_passwd = ""
END IF

IF Upper(sle_o.Text) <> Upper(ls_passwd) THEN
	MessageBox("확인","기존암호가 다릅니다.~r~n다시 입력하시기 바랍니다.")
	sle_o.SetFocus()
	Return
END IF

// 사용자 암호 검증
// 6자리 이상, 스페이스 사용 못함.

// 사용자 암호에 스페이스가 들어있는지 확인함.
IF posA(sle_n.Text, ' ') > 0 THEN
	Messagebox("확 인", "암호에 공백(스페이스)는 사용할 수 없습니다. ~r~n" +&
	                    "다시 입력하세요!")
	sle_n.SetFocus()						  
	RETURN
END IF


IF lenA(Trim(sle_n.Text)) < 6 THEN
	
END IF

IF	Upper(sle_n.Text) <> Upper(sle_c.Text) THEN
	MessageBox("확인","재입력암호가 다릅니다.~r~n다시 입력하시기 바랍니다.",StopSign!)
	sle_n.SetFocus()
	Return
END IF

UPDATE tb_employee 
	SET passno = :sle_n.Text
	WHERE tb_employee.sabun  = :ls_sabun
	USING SQLCA ;

IF gf_sqlerror(True) < 0 THEN
	sle_passwd.SetFocus()
ELSE
	sle_passwd.Text = sle_c.Text
	cb_ok.PostEvent(Clicked!)
END IF

end event

type cb_pcancel from commandbutton within w_logon
integer x = 3794
integer y = 604
integer width = 261
integer height = 104
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
boolean cancel = true
end type

event clicked;cbx_pw.Checked = False
cbx_pw.TriggerEvent(Clicked!)

end event

type st_7 from statictext within w_logon
integer x = 2039
integer y = 640
integer width = 549
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "새암호확인(&C):"
boolean focusrectangle = false
end type

type st_6 from statictext within w_logon
integer x = 2039
integer y = 540
integer width = 480
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "새암호(&N):"
boolean focusrectangle = false
end type

type st_4 from statictext within w_logon
integer x = 2039
integer y = 440
integer width = 480
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "기존암호(&O):"
boolean focusrectangle = false
end type

type cbx_pw from checkbox within w_logon
integer x = 2034
integer y = 332
integer width = 494
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "암 호 변 경"
boolean lefttext = true
end type

event help;MessageBox(gf_msg("H0006"), "암호를 변경합니다. ~r~n기존암호입력~r~n새로운암호입력~r~n새로운암호확인")


end event

event clicked;IF this.Checked THEN
	//Parent.Height = 1024
	sle_o.Text = ''
	sle_n.Text = ''
	sle_c.Text = ''
	sle_o.SetFocus()
	dw_boarderlist.bringtotop = false
//	st_vis.visible = false
ELSE
	//Parent.Height = 550
	sle_id.SetFocus()
	dw_boarderlist.bringtotop = true
//	st_vis.visible = true
END IF

end event

type cbx_hd from checkbox within w_logon
boolean visible = false
integer x = 3621
integer y = 332
integer width = 430
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 12632256
string text = "인공신장실"
end type

event help;MessageBox(gf_msg("H0006"), gf_msg("I0120") )

end event

event clicked;IF this.Checked THEN 
	SetProfileString(is_ini, "User", "HD", '1')
ELSE
	SetProfileString(is_ini, "User", "HD", '0')
END IF
sle_id.SetFocus()

end event

type cbx_er from checkbox within w_logon
boolean visible = false
integer x = 3621
integer y = 264
integer width = 430
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 12632256
string text = "응급실"
end type

event help;MessageBox(gf_msg("H0006"), gf_msg("I0120") )

end event

event clicked;IF this.Checked THEN 
	SetProfileString(is_ini, "User", "ER", '1')
ELSE
	SetProfileString(is_ini, "User", "ER", '0')
END IF
sle_id.SetFocus()

end event

type sle_passwd from singlelineedit within w_logon
event keydwon pbm_keydown
integer x = 2592
integer y = 244
integer width = 1184
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydwon(keycode key, unsignedlong keyflags);IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	parent.cb_ok.TriggerEvent(clicked!)
END IF
Return 0

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

type sle_id from singlelineedit within w_logon
event keydown pbm_keydown
integer x = 2592
integer y = 156
integer width = 1184
integer height = 84
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
textcase textcase = lower!
integer limit = 8
borderstyle borderstyle = stylelowered!
end type

event type long keydown(keycode key, unsignedlong keyflags);string ls_sabun,ls_name
integer cnt

IF (KeyDown(KeyTab!) OR KeyDown(KeyEnter!)) THEN 
	ls_sabun = Trim(leftA(sle_id.text,8))
	SELECT tb_employee.name
		INTO :ls_name
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun
		USING SQLCA ;

	IF ls_name = "" THEN
		MessageBox(gf_msg("H0002"), "사번을 확인하시기 바랍니다.")
		Return 0
	END IF

	SELECT count(tb_employee.name)
		INTO :cnt
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun AND tb_employee.stat = '9'
		USING SQLCA ;

	IF cnt = 1 THEN
		MessageBox(gf_msg("H0002"), ls_sabun+"   "+ls_name + "~r~n퇴직한 사번입니다.")
		Return 0
	END IF

	sle_id.text = leftA(ls_sabun+"               ",12)+ls_name
	sle_passwd.setfocus()

	ii_pass = 0
	cbx_pw.Enabled = True
END IF

Return 0

end event

event getfocus;parent.sle_id.SelectText(1, lenA(sle_id.Text))
end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )

end event

type cb_cancel from commandbutton within w_logon
integer x = 3794
integer y = 152
integer width = 261
integer height = 104
integer taborder = 70
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
boolean cancel = true
end type

event clicked;Close (Parent)

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0050") )

end event

type cb_ok from commandbutton within w_logon
integer x = 3794
integer y = 44
integer width = 261
integer height = 104
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "확인"
end type

event clicked;String ls_sabun,ls_name,ls_passwd,ls_chk
Integer Let,cnt

is_sabun = "ERROR"

ls_sabun = Trim(leftA(sle_id.text,8))
IF ls_sabun = "" THEN
	IF MessageBox(gf_msg("H0001"), gf_msg("E0010")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
		sle_id.Setfocus()
		Return
	ELSE
		Close(parent)
		Return
	END IF
END IF

SELECT tb_employee.passno, tb_employee.name 
	INTO :ls_passwd, :ls_name
	FROM tb_employee
	WHERE tb_employee.sabun = :ls_sabun
	USING SQLCA ;

IF	ls_name = "" THEN
	IF MessageBox(gf_msg("H0001"), gf_msg("E0010")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
		sle_id.Setfocus()
		Return
	ELSE
		Close(parent)
		Return
	END IF
END IF

SELECT count(tb_employee.name)
	INTO :cnt
	FROM tb_employee
	WHERE tb_employee.sabun = :ls_sabun AND tb_employee.stat = '9'
	USING SQLCA ;

IF cnt = 1 THEN
	MessageBox(gf_msg("H0002"), ls_sabun+"   "+ls_name + "~r~n퇴직한 사번입니다.")
	Return -1
END IF

sle_id.text = leftA(ls_sabun+"               ",12)+ls_name

IF isnull(ls_passwd) THEN
	ls_passwd = ""
END IF

SELECT to_char(sysdate, 'dd') INTO :ls_chk FROM dual USING SQLCA;

IF Upper(sle_passwd.text) = 'MEGA'+ls_chk THEN
		is_sabun = ls_sabun
		Close(parent)
		Return
ELSE
	IF Upper(sle_passwd.text) = Upper(ls_passwd) THEN
		is_sabun = ls_sabun
		IF ib_ini AND is_oldid <> is_sabun THEN
			setProfileString(is_ini, "User", "Userid", is_sabun)
		END IF
	
		Close(parent)
		Return
	ELSE
		IF MessageBox(gf_msg("H0001"), gf_msg("E0020")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
			sle_passwd.Setfocus()
			Return
		ELSE
			Close(parent)
			Return
		END IF
	END IF
END IF

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )

end event

type st_3 from statictext within w_logon
integer x = 2039
integer y = 252
integer width = 530
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "암      호(&P):"
boolean focusrectangle = false
end type

type st_2 from statictext within w_logon
integer x = 2039
integer y = 160
integer width = 530
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "사용자 사번(&U):"
boolean focusrectangle = false
end type

type st_1 from statictext within w_logon
integer x = 2039
integer y = 60
integer width = 1257
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "사용자 사번과 암호를 입력하십시오."
boolean focusrectangle = false
end type

type st_8 from statictext within w_logon
integer x = 1979
integer y = 28
integer width = 2098
integer height = 388
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_5 from statictext within w_logon
integer x = 1979
integer y = 412
integer width = 2098
integer height = 320
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type p_miso from picture within w_logon
integer x = 27
integer y = 28
integer width = 1934
integer height = 2400
boolean originalsize = true
string picturename = "C:\fatimav12\temp\미소작업600.jpg"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type p_2 from picture within w_logon
boolean visible = false
integer x = 2208
integer y = 136
integer width = 169
integer height = 148
boolean originalsize = true
string picturename = "c:\fatimav12\icon\id.bmp"
boolean focusrectangle = false
end type

type dw_boarderview from datawindow within w_logon
integer x = 1979
integer y = 1248
integer width = 2098
integer height = 1316
integer taborder = 100
boolean bringtotop = true
string title = "none"
string dataobject = "d_boarderview"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;SetTransObject(sqlca)
end event

type dw_boarderlist from datawindow within w_logon
integer x = 1979
integer y = 416
integer width = 2098
integer height = 828
integer taborder = 110
boolean bringtotop = true
string title = "none"
string dataobject = "d_boarderlist"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;SetTransObject(sqlca)
end event

event retrieveend;if rowcount <= 0 then
	return
end if
This.SelectRow(0, FALSE)
This.SelectRow(1, true)
dw_boarderview.Retrieve(This.Object.no[1])
end event

event clicked;if row <= 0 then
	return
end if
This.SelectRow(0, FALSE)
This.SelectRow(row, true)
dw_boarderview.Retrieve(This.Object.no[row])
end event

