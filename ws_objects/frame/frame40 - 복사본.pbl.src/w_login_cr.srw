﻿$PBExportHeader$w_login_cr.srw
$PBExportComments$EMR 인증로그인
forward
global type w_login_cr from window
end type
type cb_2 from commandbutton within w_login_cr
end type
type cb_change_pwd from commandbutton within w_login_cr
end type
type cb_1 from commandbutton within w_login_cr
end type
type sle_passwd from singlelineedit within w_login_cr
end type
type st_3 from statictext within w_login_cr
end type
type st_2 from statictext within w_login_cr
end type
type st_1 from statictext within w_login_cr
end type
type gb_1 from groupbox within w_login_cr
end type
type gb_2 from groupbox within w_login_cr
end type
type sle_id from singlelineedit within w_login_cr
end type
end forward

global type w_login_cr from window
integer width = 1390
integer height = 700
boolean titlebar = true
string title = "인증서로그인"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 67108864
cb_2 cb_2
cb_change_pwd cb_change_pwd
cb_1 cb_1
sle_passwd sle_passwd
st_3 st_3
st_2 st_2
st_1 st_1
gb_1 gb_1
gb_2 gb_2
sle_id sle_id
end type
global w_login_cr w_login_cr

type variables
string	is_dn, is_id
end variables

on w_login_cr.create
this.cb_2=create cb_2
this.cb_change_pwd=create cb_change_pwd
this.cb_1=create cb_1
this.sle_passwd=create sle_passwd
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.gb_1=create gb_1
this.gb_2=create gb_2
this.sle_id=create sle_id
this.Control[]={this.cb_2,&
this.cb_change_pwd,&
this.cb_1,&
this.sle_passwd,&
this.st_3,&
this.st_2,&
this.st_1,&
this.gb_1,&
this.gb_2,&
this.sle_id}
end on

on w_login_cr.destroy
destroy(this.cb_2)
destroy(this.cb_change_pwd)
destroy(this.cb_1)
destroy(this.sle_passwd)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.sle_id)
end on

event open;gf_center(this)

is_dn = Message.StringParm

is_id = midA(is_dn,1,posA(is_dn,',')-1)
sle_id.text = is_id

sle_passwd.setfocus()
end event

type cb_2 from commandbutton within w_login_cr
integer x = 1056
integer y = 452
integer width = 288
integer height = 100
integer taborder = 70
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
end type

event clicked;CloseWithReturn(parent, "")
end event

type cb_change_pwd from commandbutton within w_login_cr
integer x = 50
integer y = 452
integer width = 361
integer height = 100
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "암호변경"
end type

event clicked;gf_emr_cr_pawdchange(gs_dn,gs_sabun)
end event

type cb_1 from commandbutton within w_login_cr
integer x = 741
integer y = 452
integer width = 288
integer height = 100
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "확인"
end type

event clicked;CloseWithReturn(parent, sle_passwd.Text)
end event

type sle_passwd from singlelineedit within w_login_cr
event ue_enter pbm_keydown
integer x = 704
integer y = 320
integer width = 599
integer height = 88
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean password = true
borderstyle borderstyle = stylelowered!
end type

event type long ue_enter(keycode key, unsignedlong keyflags);IF keydown(keyEnter!) then
	cb_1.triggerevent(clicked!)
	return 0
end if

return 0
end event

type st_3 from statictext within w_login_cr
integer x = 96
integer y = 324
integer width = 576
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 81324524
string text = "인증서 암호 :"
boolean focusrectangle = false
end type

type st_2 from statictext within w_login_cr
integer x = 96
integer y = 212
integer width = 576
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 81324524
string text = "인증서 사용자 :"
boolean focusrectangle = false
end type

type st_1 from statictext within w_login_cr
integer x = 55
integer y = 80
integer width = 1280
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12639424
string text = "인증서로그인"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_login_cr
integer x = 46
integer y = 40
integer width = 1303
integer height = 132
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_2 from groupbox within w_login_cr
integer x = 46
integer y = 144
integer width = 1303
integer height = 296
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type sle_id from singlelineedit within w_login_cr
integer x = 704
integer y = 200
integer width = 599
integer height = 88
integer taborder = 30
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
borderstyle borderstyle = stylelowered!
end type

