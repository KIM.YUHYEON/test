﻿$PBExportHeader$w_logon_msg.srw
$PBExportComments$Logon화면메시지포함
forward
global type w_logon_msg from window
end type
type sle_1 from singlelineedit within w_logon_msg
end type
type cbx_jari from checkbox within w_logon_msg
end type
type cbx_con from checkbox within w_logon_msg
end type
type cbx_easy from checkbox within w_logon_msg
end type
type cbx_conchar from checkbox within w_logon_msg
end type
type cbx_num from checkbox within w_logon_msg
end type
type cbx_tuk from checkbox within w_logon_msg
end type
type cbx_so from checkbox within w_logon_msg
end type
type cbx_dae from checkbox within w_logon_msg
end type
type cb_next from commandbutton within w_logon_msg
end type
type cb_pri from commandbutton within w_logon_msg
end type
type dw_miso from datawindow within w_logon_msg
end type
type sle_c from singlelineedit within w_logon_msg
end type
type sle_n from singlelineedit within w_logon_msg
end type
type sle_o from singlelineedit within w_logon_msg
end type
type cb_pok from commandbutton within w_logon_msg
end type
type cb_pcancel from commandbutton within w_logon_msg
end type
type st_7 from statictext within w_logon_msg
end type
type st_6 from statictext within w_logon_msg
end type
type st_4 from statictext within w_logon_msg
end type
type cbx_pw from checkbox within w_logon_msg
end type
type cbx_hd from checkbox within w_logon_msg
end type
type cbx_er from checkbox within w_logon_msg
end type
type sle_passwd from singlelineedit within w_logon_msg
end type
type sle_id from singlelineedit within w_logon_msg
end type
type cb_cancel from commandbutton within w_logon_msg
end type
type cb_ok from commandbutton within w_logon_msg
end type
type st_3 from statictext within w_logon_msg
end type
type st_2 from statictext within w_logon_msg
end type
type st_1 from statictext within w_logon_msg
end type
type st_5 from statictext within w_logon_msg
end type
type p_miso from picture within w_logon_msg
end type
type p_2 from picture within w_logon_msg
end type
type dw_boarderlist from datawindow within w_logon_msg
end type
type st_8 from statictext within w_logon_msg
end type
type dw_employee from datawindow within w_logon_msg
end type
type dw_boarderview from datawindow within w_logon_msg
end type
end forward

global type w_logon_msg from window
integer width = 4137
integer height = 2700
boolean titlebar = true
string title = "사용자 확인"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 32571387
boolean contexthelp = true
sle_1 sle_1
cbx_jari cbx_jari
cbx_con cbx_con
cbx_easy cbx_easy
cbx_conchar cbx_conchar
cbx_num cbx_num
cbx_tuk cbx_tuk
cbx_so cbx_so
cbx_dae cbx_dae
cb_next cb_next
cb_pri cb_pri
dw_miso dw_miso
sle_c sle_c
sle_n sle_n
sle_o sle_o
cb_pok cb_pok
cb_pcancel cb_pcancel
st_7 st_7
st_6 st_6
st_4 st_4
cbx_pw cbx_pw
cbx_hd cbx_hd
cbx_er cbx_er
sle_passwd sle_passwd
sle_id sle_id
cb_cancel cb_cancel
cb_ok cb_ok
st_3 st_3
st_2 st_2
st_1 st_1
st_5 st_5
p_miso p_miso
p_2 p_2
dw_boarderlist dw_boarderlist
st_8 st_8
dw_employee dw_employee
dw_boarderview dw_boarderview
end type
global w_logon_msg w_logon_msg

type prototypes
FUNCTION boolean CreateDirectoryA(ref string pathname, int sa) LIBRARY "Kernel32.dll" alias for "CreateDirectoryA;Ansi"

//FTP관련 함수들...
//FUNCTION INT FTPConnect (String IP_ADDRESS, String USER_ID, String PASSWORD) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPConnect;Ansi"
//FUNCTION INT FTPDisconnect ( ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
//FUNCTION INT FTPgetCurrentDir (REF BLOB DATA) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
//FUNCTION INT FTPsetCurrentDirUp () LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
//FUNCTION INT FTPsetCurrentDir (String CURR_DIR) LIBRARY "FTP.DLL" alias for "C:\fatimav12\mgdll\FTPsetCurrentDir;Ansi"
//FUNCTION INT FTPfileList ( REF BLOB DATA ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL"
//FUNCTION INT FTPgetFile ( String REMOTE_FILE, String LOCAL_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPgetFile;Ansi"
//FUNCTION INT FTPcreateDir ( String NEW_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPcreateDir;Ansi"
//FUNCTION INT FTPdeleteFile ( String DEL_FILE ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPdeleteFile;Ansi"
//FUNCTION INT FTPdeleteDir ( String DEL_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPdeleteDir;Ansi"
//FUNCTION INT FTPrenameDir ( String CURR_DIR, String NEW_DIR ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPrenameDir;Ansi"
//FUNCTION INT FTPputFile ( String LOCAL_FILE, String REMOTE_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPputFile;Ansi"
//FUNCTION INT FTPping ( String IPaddress ) LIBRARY "C:\fatimav12\mgdll\FTP.DLL" alias for "FTPping;Ansi"

end prototypes

type variables
Window iw_parent
integer ii_pass
string is_sabun
String is_ini, is_oldid
boolean ib_ini
long   il_misocnt, il_pmiso, il_mode = 0
string is_ftp = '200.1.1.7', is_ftpid = 'fatima', is_ftppwd = 'fatima16'

string is_sabun2, is_passno
end variables

forward prototypes
public subroutine wf_getname (character id)
public function integer wf_password_chk (string a_password)
private function integer wf_ftpcon ()
private function integer wf_ftpdiscon ()
public subroutine wf_pw_hardchk ()
public subroutine wf_pwchk_reset ()
public function boolean wf_passchk (string a_passno)
end prototypes

public subroutine wf_getname (character id);
end subroutine

public function integer wf_password_chk (string a_password);// 사용자 암호 검증

// 사용자 암호에 스페이스가 들어있는지 확인함.
IF posA(a_password, ' ') > 0 THEN
	Messagebox("확 인", "새 암호에 공백(스페이스)는 사용할 수 없습니다. ~r~n" +&
	                    "다시 입력하세요!")
	RETURN 0
END IF

// 사용자 암호에 스페이스가 들어있는지 확인함.
IF lenA(Trim(a_password)) < 6 THEN
	Messagebox("확 인", "영문, 숫자, 특수문자를 혼용(6자 이상 16자 이하) ~r~n" +&
	                    "하시면 보다 안전합니다.")
	RETURN 0	
END IF

//wf_pw_hardchk()


RETURN 1
end function

private function integer wf_ftpcon ();Int li_rtn

uo_Ftp	uFtp

li_rtn = uFtp.FTPConnect(is_ftp, is_ftpid, is_ftppwd)

return li_rtn
end function

private function integer wf_ftpdiscon ();Int li_rtn
uo_Ftp	uFtp

li_rtn = uFtp.FTPDisconnect()

return li_rtn
end function

public subroutine wf_pw_hardchk ();
end subroutine

public subroutine wf_pwchk_reset ();cbx_dae.checked = FALSE
cbx_jari.checked = FALSE
cbx_so.checked = FALSE
cbx_con.checked = FALSE
cbx_tuk.checked = FALSE
cbx_easy.checked = FALSE
cbx_num.checked = FALSE
cbx_conchar.checked = FALSE
end subroutine

public function boolean wf_passchk (string a_passno);//======================================================================

Long ll_cnt

ll_cnt = 0 
String ls_pass
Boolean lb_special 
//lb_special = false

wf_pwchk_reset()

ls_pass = Trim(a_passno)
//ls_pass = is_passno
	

IF len(ls_pass) <= 8 THEN
//	messagebox("확인","8자리 이하")
	cbx_jari.checked = TRUE
END IF

IF Match(ls_pass, "[A-Z]") = FALSE THEN
//	messagebox("확인","대문자가 포함 되지 않음")
	cbx_dae.checked = TRUE
ELSE
	ll_cnt = ll_cnt + 1
END IF

IF Match(ls_pass, "[a-z]") = FALSE THEN
//	messagebox("확인","소문자가 포함 되지 않음")
	cbx_so.checked = TRUE
ELSE
	ll_cnt = ll_cnt + 1
END IF

IF Match(ls_pass, "[0-9]") = FALSE THEN
//	messagebox("확인","숫자가 포함 되지 않음")
	cbx_num.checked = TRUE
ELSE
	ll_cnt = ll_cnt + 1
END IF


////OR Match(ls_pass,"333") = TRUE Match(ls_pass,"444") = TRUE OR Match(ls_pass,"555") = TRUE OR Match(ls_pass,"666") = TRUE OR  Match(ls_pass,"777") = TRUE OR Match(ls_pass,"888") = TRUE OR Match(ls_pass,"999") = TRUE OR Match(ls_pass,"000") = TRUE THEN
//IF Match(ls_pass,"111") = TRUE  OR  Match(ls_pass,"222") = TRUE OR Match(ls_pass,"333") = TRUE  OR Match(ls_pass,"444") = TRUE  OR Match(ls_pass,"555") = TRUE  &
//OR Match(ls_pass,"666") = TRUE  OR Match(ls_pass,"777") = TRUE  OR Match(ls_pass,"888") = TRUE OR Match(ls_pass,"999") = TRUE OR Match(ls_pass,"000") = TRUE  THEN
//	messagebox("확인","연속된 숫자 입니다.")
//	cbx_con.checked = TRUE
//END IF
//
//IF Match(ls_pass,"qwer") = TRUE  OR  Match(ls_pass,"asdf") = TRUE OR Match(ls_pass,"zxcv") = TRUE OR Match(ls_pass,"abc") = TRUE OR Match(ls_pass,"123") = TRUE &
//	OR Match(ls_pass,"456") = TRUE OR Match(ls_pass,"789") = TRUE THEN
//	messagebox("확인","쉬운문자 입니다.")
//	cbx_easy.checked = TRUE
//END IF
//
//IF Match(ls_pass,"aaa") = TRUE  OR  Match(ls_pass,"bbb") = TRUE OR Match(ls_pass,"ccc") = TRUE  OR Match(ls_pass,"ddd") = TRUE  OR Match(ls_pass,"eee") = TRUE  &
//OR Match(ls_pass,"zzz") = TRUE  OR Match(ls_pass,"xxx") = TRUE  OR Match(ls_pass,"sss") = TRUE OR Match(ls_pass,"qqq") = TRUE OR Match(ls_pass,"rrr") = TRUE  THEN
//	messagebox("확인","연속된 문자 입니다.")
//	cbx_conchar.checked = TRUE
//END IF
//////

IF Match(ls_pass, "!") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, '@') = TRUE THEN
	lb_special = TRUE	
ELSEIF Match(ls_pass, '#') = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\$") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "%") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\^") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "&") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "(") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\*") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, ")") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "_") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "-") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\+") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "{") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "}") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\[") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "]") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "|") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\\") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "'") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, ",") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "<") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, ">") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\?") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "/") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "=") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "\+") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, "`") = TRUE THEN
	lb_special = TRUE
ELSEIF Match(ls_pass, ":") = TRUE THEN
	lb_special = TRUE	
ELSEIF Match(ls_pass, ";") = TRUE THEN
	lb_special = TRUE	
ELSEIF Match(ls_pass, "\.") = TRUE THEN
	lb_special = TRUE	
END IF

long ll_etc_cnt
select count(*)
   into :ll_etc_cnt
from dual
where :ls_pass like '%~%'
using mega1;

long ll_etc_cnt2
select count(*)
   into :ll_etc_cnt2
from dual
where :ls_pass like '%"%'
using mega1;

IF ll_etc_cnt > 0 THEN
	lb_special = TRUE
END IF
IF ll_etc_cnt2 > 0 THEN
	lb_special = TRUE
END IF

IF lb_special = TRUE THEN
	ll_cnt = ll_cnt + 1
END IF

////messagebox('', match(ls_pass, "^[0-9a-zA-Z][-?_?.?0-9a-zA-Z]*@[0-9a-zA-Z]*.?[a-zA-Z]*.?.[a-zA-Z][a-zA-Z]?[a-zA-Z]$" ))
//
 IF len(ls_pass) >= 8 and len(ls_pass) <= 9 and ll_cnt < 3 THEN
	return false
 ELSEIF  len(ls_pass) >= 10 and ll_cnt < 2 THEN	
	return false
 ELSEIF  len(ls_pass) < 8  THEN	
	return false	
 END IF


//======================================================================
return true
end function

on w_logon_msg.create
this.sle_1=create sle_1
this.cbx_jari=create cbx_jari
this.cbx_con=create cbx_con
this.cbx_easy=create cbx_easy
this.cbx_conchar=create cbx_conchar
this.cbx_num=create cbx_num
this.cbx_tuk=create cbx_tuk
this.cbx_so=create cbx_so
this.cbx_dae=create cbx_dae
this.cb_next=create cb_next
this.cb_pri=create cb_pri
this.dw_miso=create dw_miso
this.sle_c=create sle_c
this.sle_n=create sle_n
this.sle_o=create sle_o
this.cb_pok=create cb_pok
this.cb_pcancel=create cb_pcancel
this.st_7=create st_7
this.st_6=create st_6
this.st_4=create st_4
this.cbx_pw=create cbx_pw
this.cbx_hd=create cbx_hd
this.cbx_er=create cbx_er
this.sle_passwd=create sle_passwd
this.sle_id=create sle_id
this.cb_cancel=create cb_cancel
this.cb_ok=create cb_ok
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.st_5=create st_5
this.p_miso=create p_miso
this.p_2=create p_2
this.dw_boarderlist=create dw_boarderlist
this.st_8=create st_8
this.dw_employee=create dw_employee
this.dw_boarderview=create dw_boarderview
this.Control[]={this.sle_1,&
this.cbx_jari,&
this.cbx_con,&
this.cbx_easy,&
this.cbx_conchar,&
this.cbx_num,&
this.cbx_tuk,&
this.cbx_so,&
this.cbx_dae,&
this.cb_next,&
this.cb_pri,&
this.dw_miso,&
this.sle_c,&
this.sle_n,&
this.sle_o,&
this.cb_pok,&
this.cb_pcancel,&
this.st_7,&
this.st_6,&
this.st_4,&
this.cbx_pw,&
this.cbx_hd,&
this.cbx_er,&
this.sle_passwd,&
this.sle_id,&
this.cb_cancel,&
this.cb_ok,&
this.st_3,&
this.st_2,&
this.st_1,&
this.st_5,&
this.p_miso,&
this.p_2,&
this.dw_boarderlist,&
this.st_8,&
this.dw_employee,&
this.dw_boarderview}
end on

on w_logon_msg.destroy
destroy(this.sle_1)
destroy(this.cbx_jari)
destroy(this.cbx_con)
destroy(this.cbx_easy)
destroy(this.cbx_conchar)
destroy(this.cbx_num)
destroy(this.cbx_tuk)
destroy(this.cbx_so)
destroy(this.cbx_dae)
destroy(this.cb_next)
destroy(this.cb_pri)
destroy(this.dw_miso)
destroy(this.sle_c)
destroy(this.sle_n)
destroy(this.sle_o)
destroy(this.cb_pok)
destroy(this.cb_pcancel)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_4)
destroy(this.cbx_pw)
destroy(this.cbx_hd)
destroy(this.cbx_er)
destroy(this.sle_passwd)
destroy(this.sle_id)
destroy(this.cb_cancel)
destroy(this.cb_ok)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.st_5)
destroy(this.p_miso)
destroy(this.p_2)
destroy(this.dw_boarderlist)
destroy(this.st_8)
destroy(this.dw_employee)
destroy(this.dw_boarderview)
end on

event open;Integer li_cnt
string ls_name, ls_chk
string ls_title

This.Title = gf_hospital_data('Z01name') + ' - 사용자 확인'

ib_ini = False

cbx_pw.TriggerEvent(Clicked!)
gf_center(this)

dw_boarderlist.Retrieve()
//this.Y = this.Y - 400

dw_miso.Retrieve()

is_ini = Message.StringParm
//messagebox(gs_ini, Message.StringParm)
IF lower(leftA(is_ini, 12)) = "c:\fatimav12" THEN
	ib_ini = True
END IF
//응급실, 인공신장실 체크
SetProfileString(is_ini, "User", "ER", '0')
SetProfileString(is_ini, "User", "HD", '0')
ls_chk = rightA(is_ini, 2)
IF IsNumber(ls_chk) THEN
	IF leftA(ls_chk, 1) = '1' THEN
		cbx_er.Visible = True
		SetProfileString(is_ini, "User", "ER", '1')
	END IF
	IF rightA(ls_chk, 1) = '1' THEN
		cbx_hd.Visible = True
		SetProfileString(is_ini, "User", "HD", '1')
	END IF
	is_ini = leftA(is_ini, lenA(is_ini) -2)
END IF

IF ib_ini AND FileExists(is_ini) = False THEN
	integer li_FileNum
	li_FileNum = FileOpen(is_ini, StreamMode!, Write!, LockWrite!, Replace!)
	FileClose(li_FileNum)
END IF

is_sabun = "ERROR"
IF ib_ini THEN
	is_oldid = ProfileString(is_ini,"User","Userid","")
	sle_id.text = is_oldid
	SELECT tb_employee.name
		INTO :ls_name
		FROM tb_employee
		WHERE tb_employee.sabun = :is_oldid
		USING SQLCA ;

	IF	ls_name = "" THEN		
		sle_id.Setfocus()
		Return
	END IF
	sle_id.text = leftA(is_oldid+"               ",12)+ls_name
	sle_passwd.setfocus()

	ii_pass = 0
	cbx_pw.Enabled = True
ELSE
	sle_id.Setfocus()
END IF
//
end event

event systemkey;IF keydown(KeyAlt!) THEN
	IF keydown(ASC("U")) THEN
		sle_id.SetFocus()
	ELSEIF keydown(ASC("P")) THEN
		sle_passwd.SetFocus()
	END IF
	
	IF cbx_pw.Checked THEN
		IF keydown(ASC("O")) THEN
			sle_o.SetFocus()
		ELSEIF keydown(ASC("N")) THEN
			sle_n.SetFocus()
		ELSEIF keydown(ASC("C")) THEN
			sle_c.SetFocus()
		END IF
	END IF
END IF

end event

event closequery;String	ls_programId

IF is_sabun = 'ERROR' THEN
	Message.StringParm = ''
	SetProfileString(is_ini, "User", "ER", '0')
	SetProfileString(is_ini, "User", "HD", '0')
ELSE
	IF cbx_er.Checked THEN 
		SetProfileString(is_ini, "User", "ER", '1') 
	ELSE
		SetProfileString(is_ini, "User", "ER", '0')
	END IF

	IF cbx_hd.Checked THEN 
		SetProfileString(is_ini, "User", "HD", '1')
	ELSE
		SetProfileString(is_ini, "User", "HD", '0')
	END IF
	
	/* 
	주석처리(2011-10-21 : Kim Jin Yeong : 이학송 대리 요청)
	TZ_USERMENU INSERT (2011-10-12 : Kim Jin Yeong : 서충기 대리 요청)
	TZ_USERMENU_HISTORY INSERT (2012.12.22  : Kim Jin Yeong : 김진영 대리 요청)
	*/
	
	IF gf_hospital_data("LOGONHISTORY") = 'Y' THEN	
		ls_programId =  GetApplication().appname
		
		insert into tz_usermenu_history
				 (sabun, programid, menuid, visible)
		select :is_sabun, :ls_programid, '%', 'Y'
		  from dual
		 where not exists (select 1 from tz_usermenu_history
								  where sabun = :is_sabun
									 and programid = :ls_programid
									 and menuid = '%')
		using SQLCA;	
		gf_sqlerror2(SQLCA, True)
	END IF
	
	gf_program_log('L', is_sabun, ls_programId, ' ', ' ', '로그인 성공')	
	
	Message.StringParm = is_sabun
END IF

end event

type sle_1 from singlelineedit within w_logon_msg
boolean visible = false
integer x = 2665
integer y = 328
integer width = 919
integer height = 84
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

type cbx_jari from checkbox within w_logon_msg
boolean visible = false
integer x = 2706
integer y = 820
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "자리수"
borderstyle borderstyle = styleraised!
end type

type cbx_con from checkbox within w_logon_msg
boolean visible = false
integer x = 2706
integer y = 892
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "연속된숫자"
borderstyle borderstyle = styleraised!
end type

type cbx_easy from checkbox within w_logon_msg
boolean visible = false
integer x = 2706
integer y = 972
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "쉬운문자"
borderstyle borderstyle = styleraised!
end type

type cbx_conchar from checkbox within w_logon_msg
boolean visible = false
integer x = 2706
integer y = 1048
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "연속된문자"
borderstyle borderstyle = styleraised!
end type

type cbx_num from checkbox within w_logon_msg
boolean visible = false
integer x = 2098
integer y = 1048
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "숫자"
borderstyle borderstyle = styleraised!
end type

type cbx_tuk from checkbox within w_logon_msg
boolean visible = false
integer x = 2098
integer y = 976
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "특수문자"
borderstyle borderstyle = styleraised!
end type

type cbx_so from checkbox within w_logon_msg
boolean visible = false
integer x = 2098
integer y = 900
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "소문자"
borderstyle borderstyle = styleraised!
end type

type cbx_dae from checkbox within w_logon_msg
boolean visible = false
integer x = 2098
integer y = 828
integer width = 517
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
string text = "대문자"
borderstyle borderstyle = styleraised!
end type

type cb_next from commandbutton within w_logon_msg
integer x = 992
integer y = 2452
integer width = 969
integer height = 112
integer taborder = 140
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
boolean enabled = false
string text = "다음 ▷"
end type

event clicked;uo_Ftp	uFtp

//uFtp.FTPgetFile uFtp,

il_pmiso   = il_pmiso + 1
if il_pmiso = il_misocnt then
	This.Enabled = False
end if
if il_pmiso < il_misocnt then
	cb_pri.Enabled = True
end if

string docname, ls_filename, ls_dir, ls_server_filepath, ls_client_filepath, ls_gubun
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn, li_rtn

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_gubun    = dw_miso.Object.gubun   [il_pmiso]
ls_dir      = "c:\fatimav12\temp"
docname     = ls_dir + '\' + ls_filename
If FileExists (ls_dir) Then 
else
   CreateDirectoryA (ls_dir, 0)
end if
if FileExists(docname) then
	FileDelete(docname) 
end if

if wf_ftpcon() <> -1 then
	ls_server_filepath = '/ftp/board/' + ls_gubun + '/' + string(ll_fileseq)
	ls_client_filepath = docname
	li_rtn = uFtp.FTPgetFile (ls_server_filepath, ls_client_filepath, il_mode) 
	IF li_rtn < 0 THEN 
		MessageBox("알림!" + String(li_rtn), "FTP서버로 부터 파일 가져오기를 실패했습니다.")
		wf_ftpdiscon()
		return
	end if
	wf_ftpdiscon()
end if

//SELECTBLOB filex
//   	INTO :Emp_id_pic
//      FROM ty_boardfile
// 	   WHERE fileseq = :ll_fileseq
//	   USING sqlca ;
//			
//lb_pic = emp_id_pic
//If FileExists (docname) Then
//   FileDelete(docname) 
//end if
//   		 
//li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
//ll_pic =1
//bytes_read = lenA(emp_id_pic)
//		 
//IF bytes_read > 32765 THEN
//   IF Mod(bytes_read, 32765) = 0 THEN
//      loops = bytes_read/32765
//	ELSE
//   	loops = (bytes_read/32765) + 1
//   END IF
//ELSE
//   loops = 1
//END IF
//   	// Read the file
//FOR i = 1 to loops
//    ll_rtn = FileWrite(li_FileNum, lb_pic)
//    if ll_rtn = 32765 then
//	    lb_pic = blobmid(lb_pic, 32766)
//    end if
//NEXT
//fileclose(li_FileNum)
//	
p_miso.picturename = docname
p_miso.visible = True

end event

type cb_pri from commandbutton within w_logon_msg
integer x = 23
integer y = 2452
integer width = 969
integer height = 112
integer taborder = 120
integer textsize = -12
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "◁ 이전"
end type

event clicked;il_pmiso   = il_pmiso - 1
if il_pmiso = 1 then
	This.Enabled = False
end if
if il_pmiso < il_misocnt then
	cb_next.Enabled = True
end if

string docname, ls_filename, ls_dir, ls_server_filepath, ls_client_filepath, ls_gubun
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn, li_rtn
uo_Ftp uFtp

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_gubun    = dw_miso.Object.gubun[il_pmiso]
ls_dir      = "c:\fatimav12\temp"
docname     = ls_dir + '\' + ls_filename
If FileExists (ls_dir) Then 
else
  CreateDirectoryA (ls_dir, 0)
end if
if FileExists(docname) then
	FileDelete(docname) 
end if

if wf_ftpcon() <> -1 then
	ls_server_filepath = '/ftp/board/'+ ls_gubun + '/' + string(ll_fileseq)
	ls_client_filepath = docname
	li_rtn = uFtp.FTPgetFile (ls_server_filepath, ls_client_filepath, il_mode) 
	IF li_rtn < 0 THEN 
		MessageBox("알림!" + String(li_rtn), "FTP서버로 부터 파일 가져오기를 실패했습니다.")
		wf_ftpdiscon()
		return
	end if
	wf_ftpdiscon()
end if

//SELECTBLOB filex
//   	INTO :Emp_id_pic
//      FROM ty_boardfile
// 	   WHERE fileseq = :ll_fileseq
//	   USING sqlca ;
//			
//lb_pic = emp_id_pic
//If FileExists (docname) Then
//   FileDelete(docname) 
//end if
//   		 
//li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
//ll_pic =1
//bytes_read = lenA(emp_id_pic)
//		 
//IF bytes_read > 32765 THEN
//   IF Mod(bytes_read, 32765) = 0 THEN
//      loops = bytes_read/32765
//	ELSE
//   	loops = (bytes_read/32765) + 1
//   END IF
//ELSE
//   loops = 1
//END IF
//   	// Read the file
//FOR i = 1 to loops
//    ll_rtn = FileWrite(li_FileNum, lb_pic)
//    if ll_rtn = 32765 then
//	    lb_pic = blobmid(lb_pic, 32766)
//    end if
//NEXT
//fileclose(li_FileNum)
//	
p_miso.picturename = docname
p_miso.visible = True


end event

type dw_miso from datawindow within w_logon_msg
boolean visible = false
integer x = 1230
integer y = 1616
integer width = 1070
integer height = 600
integer taborder = 130
string title = "none"
string dataobject = "d_boardermisolist"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;SetTransObject(sqlca)
end event

event retrieveend;if rowcount < 0 then 
	return
end if
il_misocnt = rowcount
il_pmiso   = rowcount

if il_pmiso = 1 then
	cb_pri.enabled = false
end if
string docname, ls_filename, ls_dir
Blob   Emp_id_pic, lb_pic
long   li_FileNum, ll_fileseq, ll_pic, bytes_read, loops, i, ll_rtn, li_rtn
string ls_server_filepath, ls_client_filepath, ls_gubun
uo_Ftp uFtp

ll_fileseq  = dw_miso.Object.fileseq[il_pmiso]
ls_filename = dw_miso.Object.filename[il_pmiso]
ls_gubun    = dw_miso.Object.gubun[il_pmiso]
ls_dir      = "c:\fatimav12\temp"
docname     = ls_dir + '\' + ls_filename	

If FileExists (ls_dir) Then 
else
   CreateDirectoryA (ls_dir, 0)
end if
if FileExists(docname) then
	FileDelete(docname) 
end if

if wf_ftpcon() <> -1 then
	ls_server_filepath = '/ftp/board/' + ls_gubun + '/' + string(ll_fileseq)
	ls_client_filepath = docname
	li_rtn = uFtp.FTPgetFile (ls_server_filepath, ls_client_filepath, il_mode) 
	IF li_rtn < 0 THEN 
		MessageBox("알림!" + String(li_rtn), "FTP서버로 부터 파일 가져오기를 실패했습니다.")
		wf_ftpdiscon()
		return
	end if
	wf_ftpdiscon()
end if

//SELECTBLOB filex
//   	INTO :Emp_id_pic
//      FROM ty_boardfile
// 	   WHERE fileseq = :ll_fileseq
//	   USING sqlca ;
//			
//lb_pic = emp_id_pic
//If FileExists (docname) Then
//   FileDelete(docname) 
//end if
//   		 
//li_FileNum = FileOpen(docname, StreamMode!, write!, shared!, Append!)
//ll_pic =1
//bytes_read = lenA(emp_id_pic)
//		 
//IF bytes_read > 32765 THEN
//   IF Mod(bytes_read, 32765) = 0 THEN
//      loops = bytes_read/32765
//	ELSE
//   	loops = (bytes_read/32765) + 1
//   END IF
//ELSE
//   loops = 1
//END IF
//   	// Read the file
//FOR i = 1 to loops
//    ll_rtn = FileWrite(li_FileNum, lb_pic)
//    if ll_rtn = 32765 then
//	    lb_pic = blobmid(lb_pic, 32766)
//    end if
//NEXT
//fileclose(li_FileNum)
//	
p_miso.picturename = docname
p_miso.visible = True

end event

type sle_c from singlelineedit within w_logon_msg
event keydwon pbm_keydown
integer x = 2665
integer y = 624
integer width = 919
integer height = 84
integer taborder = 90
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event keydwon;IF keydown(keyenter!) THEN 
	IF	Upper(sle_n.Text) <> Upper(This.Text) THEN
		MessageBox("확 인","재입력암호가 다릅니다.~r~n다시 입력하시기 바랍니다.",StopSign!)
		sle_c.SetFocus()
	ELSE
		cb_pok.TriggerEvent(clicked!)
	END IF		
END IF

Return 1


end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

event losefocus;IF keydown(keyTab!) THEN 
	IF	Upper(sle_n.Text) <> Upper(This.Text) THEN
		MessageBox("확 인","재입력암호가 다릅니다.~r~n다시 입력하시기 바랍니다.",StopSign!)
		sle_c.SetFocus()
	ELSE
		cb_pok.TriggerEvent(clicked!)
	END IF		
END IF

Return 1

end event

type sle_n from singlelineedit within w_logon_msg
event keydwon pbm_keydown
integer x = 2665
integer y = 524
integer width = 919
integer height = 84
integer taborder = 40
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event keydwon;IF keydown(keyenter!) THEN 
	IF wf_passchk(This.Text) = false then
		messagebox("확인","개인정보의 안전성 확보 강화를 위하여 비밀번호를 변경 해주시기를 부탁드립니다. ~r~n~r~n" +&
								"최소10자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  2종류 조합 ~r~n" +&
								"최소  8자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  3종류 조합")
		This.SetFocus()
	ELSE
		sle_c.SetFocus()
	END IF
END IF

Return 1

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

event losefocus;IF keydown(KeyTab!) THEN
	// 사용자 암호 검증
	IF wf_passchk(This.Text) = false then
		messagebox("확인","개인정보의 안전성 확보 강화를 위하여 비밀번호를 변경 해주시기를 부탁드립니다. ~r~n~r~n" +&
								"최소10자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  2종류 조합 ~r~n" +&
								"최소  8자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  3종류 조합")
		This.SetFocus()
	ELSE
		sle_c.SetFocus()
	END IF	
END IF

Return 1
end event

type sle_o from singlelineedit within w_logon_msg
event keydwon pbm_keydown
integer x = 2665
integer y = 428
integer width = 919
integer height = 84
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event keydwon;String ls_sabun, ls_crypto_passwd, ls_crypto_npassno
IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	ls_sabun = Trim(leftA(parent.sle_id.text,8))
	
	SELECT rawtohex(dbms_crypto.hash(to_clob(:sle_o.Text),3))
	  INTO :ls_crypto_passwd
	  FROM DUAL
	USING SQLCA;	
	
	
	SELECT tb_employee.npassno
		INTO :ls_crypto_npassno
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun
		USING SQLCA ;
	IF isnull(ls_crypto_npassno) THEN ls_crypto_npassno = ''

	IF ls_crypto_passwd <> ls_crypto_npassno THEN
		MessageBox("확인","기존암호가 다릅니다.~r~n 다시 입력하시기 바랍니다.")
		sle_o.SetFocus()
		Return 0
	ELSE
		sle_n.setfocus()
	END IF
END IF

Return 0
end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

event losefocus;string ls_sabun, ls_crypto_passwd, ls_crypto_npassno
IF keydown(KeyTab!) THEN 
	ls_sabun = Trim(leftA(parent.sle_id.text,8))
	
	SELECT rawtohex(dbms_crypto.hash(to_clob(:sle_o.Text),3))
	  INTO :ls_crypto_passwd
	  FROM DUAL
	USING SQLCA;	
	
	SELECT tb_employee.npassno
		INTO :ls_crypto_npassno
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun
	USING SQLCA ;
	IF isnull(ls_crypto_npassno) THEN ls_crypto_npassno = ''

	IF ls_crypto_passwd <> ls_crypto_npassno THEN
		MessageBox("확인","기존암호가 다릅니다.~r~n 다시 입력하시기 바랍니다.")
		sle_o.SetFocus()
		Return 0
	ELSE
		sle_n.setfocus()
	END IF
END IF

Return 0
end event

type cb_pok from commandbutton within w_logon_msg
integer x = 3611
integer y = 480
integer width = 430
integer height = 104
integer taborder = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "확인"
end type

event clicked;String ls_sabun,ls_name,ls_passwd,ls_chk, ls_crypto_passwd, ls_crypto_npassno, ls_new_crypto_passwd
Integer Let, li_spectrausers_cnt

ls_sabun = Trim(leftA(sle_id.text,8))
IF ls_sabun = "" THEN
	MessageBox(gf_msg("H0001"), gf_msg("E0010"))
	sle_id.Setfocus()
	Return
END IF

SELECT rawtohex(dbms_crypto.hash(to_clob(:sle_o.Text),3))
  INTO :ls_crypto_passwd
  FROM DUAL
USING SQLCA;	
	

SELECT tb_employee.npassno, tb_employee.name 
	INTO :ls_crypto_npassno, :ls_name
	FROM tb_employee
	WHERE tb_employee.sabun = :ls_sabun
	USING SQLCA ;

IF	ls_name = "" THEN
	MessageBox(gf_msg("H0001"), gf_msg("E0010"))
	sle_id.Setfocus()
	Return
END IF

sle_id.text = leftA(ls_sabun+"               ",12)+ls_name

IF isnull(ls_crypto_npassno) THEN
	ls_crypto_npassno = ""
END IF

IF ls_crypto_passwd <> ls_crypto_npassno THEN
	MessageBox("확인","기존암호가 다릅니다.~r~n다시 입력하시기 바랍니다.")
	sle_o.SetFocus()
	Return
END IF

SELECT rawtohex(dbms_crypto.hash(to_clob(:sle_n.Text),3))
  INTO :ls_new_crypto_passwd
  FROM DUAL
USING SQLCA;

// 20140819 기존암호와 동일암호 다른 지 확인
IF ls_new_crypto_passwd = ls_crypto_npassno THEN
	MessageBox("확인","기존암호와 동일한 암호로 변경할 수 없습니다.~r~n다시 입력하시기 바랍니다.")
	sle_n.SetFocus()
	Return
END IF

// 사용자 암호 검증
IF wf_passchk(sle_n.Text) = false then
	messagebox("확인","개인정보의 안전성 확보 강화를 위하여 비밀번호를 변경 해주시기를 부탁드립니다. ~r~n~r~n" +&
							"최소10자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  2종류 조합 ~r~n" +&
							"최소  8자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  3종류 조합")
	sle_n.SetFocus()
	Return
END IF

// 사용자 암호 검증
// (2015.01.29 김진영)
//IF wf_password_chk(sle_n.Text) = 0 THEN
//	sle_n.SetFocus()						  
//	RETURN
//END IF

IF	Upper(sle_n.Text) <> Upper(sle_c.Text) THEN
	MessageBox("확인","재입력암호가 다릅니다.~r~n다시 입력하시기 바랍니다.",StopSign!)
	sle_n.SetFocus()
	Return
END IF


// 패스워드 암호화(2012.10.12 : 김진영), 암호변경 시 6개월 후 날짜 저장
UPDATE TB_EMPLOYEE E
   SET E.NPASSNO = rawtohex(dbms_crypto.hash(to_clob(NVL(:sle_n.Text,'fatima')),3)) , expdate = to_char(add_months(sysdate,6),'yyyymmdd'),
	    E.PASSNO = :sle_n.Text
 WHERE E.SABUN = :ls_sabun
USING SQLCA;

/* 기존소스 주석처리 (2012.10.10 : 김진영)*/
//UPDATE tb_employee 
//	SET passno = :sle_n.Text
// WHERE tb_employee.sabun  = :ls_sabun
//USING SQLCA ;

// (2015.01.29 김진영) PACS 암호변경.
select count(1)
  into :li_spectrausers_cnt
  from spectra.users a
 where userid = :ls_sabun
USING SQLCA;

IF li_spectrausers_cnt > 0 THEN
	// (2015.01.29 김진영) PACS 사용자 암호변경 
	UPDATE spectra.users a
		SET password = :sle_n.Text
	 WHERE userid = :ls_sabun
	USING SQLCA;
END IF

IF gf_sqlerror(True) < 0 THEN
	sle_passwd.SetFocus()
ELSE
	sle_passwd.Text = sle_c.Text
	cb_ok.PostEvent(Clicked!)
END IF

end event

type cb_pcancel from commandbutton within w_logon_msg
integer x = 3611
integer y = 604
integer width = 430
integer height = 104
integer taborder = 50
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
boolean cancel = true
end type

event clicked;cbx_pw.Checked = False
cbx_pw.TriggerEvent(Clicked!)

end event

type st_7 from statictext within w_logon_msg
integer x = 2094
integer y = 640
integer width = 549
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "새암호확인(&C):"
boolean focusrectangle = false
end type

type st_6 from statictext within w_logon_msg
integer x = 2094
integer y = 540
integer width = 480
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "새암호(&N):"
boolean focusrectangle = false
end type

type st_4 from statictext within w_logon_msg
integer x = 2094
integer y = 440
integer width = 480
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean enabled = false
string text = "기존암호(&O):"
boolean focusrectangle = false
end type

type cbx_pw from checkbox within w_logon_msg
integer x = 2089
integer y = 332
integer width = 494
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "암 호 변 경"
boolean lefttext = true
end type

event help;MessageBox(gf_msg("H0006"), "암호를 변경합니다. ~r~n기존암호입력~r~n새로운암호입력~r~n새로운암호확인")


end event

event clicked;IF this.Checked THEN
	//Parent.Height = 1024
	sle_o.Text = ''
	sle_n.Text = ''
	sle_c.Text = ''
	sle_o.SetFocus()
	dw_boarderlist.bringtotop = false
//	st_vis.visible = false
ELSE
	//Parent.Height = 550
	sle_id.SetFocus()
	dw_boarderlist.bringtotop = true
//	st_vis.visible = true
END IF

end event

type cbx_hd from checkbox within w_logon_msg
boolean visible = false
integer x = 3611
integer y = 336
integer width = 430
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 12632256
string text = "인공신장실"
end type

event help;MessageBox(gf_msg("H0006"), gf_msg("I0120") )

end event

event clicked;IF this.Checked THEN 
	SetProfileString(is_ini, "User", "HD", '1')
ELSE
	SetProfileString(is_ini, "User", "HD", '0')
END IF
sle_id.SetFocus()

end event

type cbx_er from checkbox within w_logon_msg
boolean visible = false
integer x = 3611
integer y = 268
integer width = 430
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 128
long backcolor = 12632256
string text = "응급실"
end type

event help;MessageBox(gf_msg("H0006"), gf_msg("I0120") )

end event

event clicked;IF this.Checked THEN 
	SetProfileString(is_ini, "User", "ER", '1')
ELSE
	SetProfileString(is_ini, "User", "ER", '0')
END IF
sle_id.SetFocus()

end event

type sle_passwd from singlelineedit within w_logon_msg
event keydwon pbm_keydown
integer x = 2665
integer y = 244
integer width = 919
integer height = 84
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
boolean password = true
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydwon(keycode key, unsignedlong keyflags);IF keydown(keyenter!) OR keydown(KeyTab!) THEN 
	parent.cb_ok.TriggerEvent(clicked!)
END IF
Return 0

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )
end event

type sle_id from singlelineedit within w_logon_msg
event keydown pbm_keydown
integer x = 2665
integer y = 156
integer width = 919
integer height = 84
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
boolean autohscroll = false
textcase textcase = lower!
integer limit = 12
borderstyle borderstyle = stylelowered!
end type

event type long keydown(keycode key, unsignedlong keyflags);string ls_sabun,ls_name
integer cnt

IF (KeyDown(KeyTab!) OR KeyDown(KeyEnter!)) THEN 
	ls_sabun = Trim(leftA(sle_id.text,8))
	SELECT tb_employee.name
		INTO :ls_name
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun
		USING SQLCA ;

	IF ls_name = "" THEN
		MessageBox(gf_msg("H0002"), "사번을 확인하시기 바랍니다.")
		Return 0
	END IF

	SELECT count(tb_employee.name)
		INTO :cnt
		FROM tb_employee
		WHERE tb_employee.sabun = :ls_sabun AND tb_employee.stat = '9'
		USING SQLCA ;

	IF cnt = 1 THEN
		MessageBox(gf_msg("H0002"), ls_sabun+"   "+ls_name + "~r~n퇴직한 사번입니다.")
		Return 0
	END IF

	sle_id.text = leftA(ls_sabun+"               ",12)+ls_name
	sle_passwd.setfocus()

	ii_pass = 0
	cbx_pw.Enabled = True
END IF

Return 0

end event

event getfocus;parent.sle_id.SelectText(1, lenA(sle_id.Text))
end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )

end event

type cb_cancel from commandbutton within w_logon_msg
integer x = 3611
integer y = 152
integer width = 430
integer height = 104
integer taborder = 70
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "취소"
boolean cancel = true
end type

event clicked;Close (Parent)

end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0050") )

end event

type cb_ok from commandbutton within w_logon_msg
integer x = 3611
integer y = 44
integer width = 430
integer height = 104
integer taborder = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string text = "확인"
end type

event clicked;String ls_sabun,ls_name,ls_passwd,ls_chk
String ls_programId, ls_empstat, ls_ocsdept, ls_ocsdeptname, ls_programNm, ls_roleDept, ls_empdept,ls_expdate
String ls_crypto_npassno, ls_crypto_passwd
Integer Let,cnt
Menu	lm_menu

is_sabun = "ERROR"

ls_sabun = Trim(leftA(sle_id.text,8))
IF ls_sabun = "" THEN
	IF MessageBox(gf_msg("H0001"), gf_msg("E0010")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
		sle_id.Setfocus()
		Return
	ELSE
		Close(parent)
		Return
	END IF
END IF

SELECT tb_employee.passno, tb_employee.name, tb_employee.empstat, tb_employee.dept, tb_employee.npassno, tb_employee.expdate
  INTO :ls_passwd, :ls_name, :ls_empstat, :ls_empdept, :ls_crypto_npassno, :ls_expdate
  FROM tb_employee
 WHERE tb_employee.sabun = :ls_sabun
USING SQLCA ;

IF	ls_name = "" THEN
	IF MessageBox(gf_msg("H0001"), gf_msg("E0010")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
		sle_id.Setfocus()
		Return
	ELSE
		Close(parent)
		Return
	END IF
END IF

SELECT count(tb_employee.name)
  INTO :cnt
  FROM tb_employee
 WHERE tb_employee.sabun = :ls_sabun AND tb_employee.stat = '9'
USING SQLCA ;

IF cnt = 1 THEN
	MessageBox(gf_msg("H0002"), ls_sabun+"   "+ls_name + "~r~n퇴직한 사번입니다.")
	Return -1
END IF

// 로그인 롤 관리(2012.12.21:김진영) - START
IF gf_sysdate() >= '20140901' THEN
	IF TRIM(gf_hospital_data("ROLEYN")) = 'Y' AND ls_empdept <> "57" THEN	
		ls_programId =  GetApplication().Appname
		
		gf_usermenu01('L', lm_menu, ls_programId, ls_sabun)
	END IF
ELSE
// 2014.09.01 이전
	// 근무부서가 정보지원과 및 의사는 로그인 롤 관리에서 제외.
	IF (ls_empstat <> 'S' AND ls_empstat <> 'I'  AND ls_empstat <> 'R1' AND ls_empstat <> 'R2' AND ls_empstat <> 'R3' AND ls_empstat <> 'R4' AND ls_empstat <> 'RX') &
		AND ls_empdept <> "57" THEN
		IF TRIM(gf_hospital_data("ROLEYN")) = 'Y' THEN	
			ls_programId =  GetApplication().appname
			
			// 로그인 롤 관리 대상부서 체크.	
			select count(1)
			  into :cnt
			  from tz_role_dept a
			 where (a.dept = :ls_empdept or a.dept = '%%')
			using SQLCA;
			
			IF cnt > 0 THEN
				gf_usermenu01('L', lm_menu, ls_programId, ls_sabun)
			END IF
		END IF
	END IF
END IF

/* 주석처리(2012.12.24:김진영) : gf_usermenu01함수로 대체하여 주석처리함.
// 로그인 롤 관리(2012.03.24:김진영) - START
IF TRIM(gf_hospital_data("ROLEYN")) = 'Y' THEN	
	ls_roleDept = TRIM(gf_hospital_data("ROLEDEPT"))
	
	// 근무부서가 정보지원과 및 의사는 로그인 롤 관리에서 제외.
	IF (ls_empstat <> 'S' AND ls_empstat <> 'I'  AND ls_empstat <> 'R1' AND ls_empstat <> 'R2' AND ls_empstat <> 'R3' AND ls_empstat <> 'R4' AND ls_empstat <> 'RX') &
	   AND ls_empdept <> "57" THEN	
		
		IF ls_roleDept = '%' OR ls_roleDept = ls_empdept THEN
			ls_programId =  GetApplication().appname
			
			// 실행명 가져오기.
			select menudesc
			  into :ls_programNm
			  from tz_versionmenudir_v12 
			 where menuname = :ls_programNm
			using SQLCA;
			
			cnt = 0
			select nvl(count(1), 0)
			  into :cnt
			  from tz_role_programid a
			 where a.sabun = :ls_sabun
				and a.programid = '%'
			using SQLCA;
			
			IF cnt = 0 THEN
				select nvl(count(1), 0)
				  into :cnt
				  from tz_role_programid a
				 where a.sabun = :ls_sabun
					and a.programid = :ls_programId
				using SQLCA;
				
				IF cnt = 0 THEN
					// 만료일자까지는 알림 메세지만 표기함.
					IF gf_sysdate() <= TRIM(gf_hospital_data("ROLEMSGEXPDATE")) THEN
						MessageBox(gf_msg("H0002"), ls_name + "[" + ls_sabun + "] 사용자는 " + ls_programNm + " 프로그램을 사용할 권한이 없습니다.~r~n" +&
															 String("20120402", "@@@@년@@월@@일") + "이후에는 사용할 수 없습니다.~r~n" +&
															 "권한이 필요한 경우에는 행정지원과로 문의하세요!")			
					ELSE
						MessageBox(gf_msg("H0002"), ls_name + "[" + ls_sabun + "] 사용자는 " + ls_programNm + " 프로그램을 사용할 권한이 없습니다.~r~n" +&
															 "권한이 필요한 경우에는 행정지원과로 문의하세요!")
						Return -1			
					END IF
				END IF
			END IF
		END IF
	END IF
END IF
*/
// 로그인 롤 관리(2012.03.24:김진영) - END


sle_id.text = leftA(ls_sabun+"               ",12)+ls_name

IF ISNULL(ls_passwd) THEN
	ls_passwd = ""
END IF

IF ISNULL(ls_crypto_npassno) THEN
	ls_crypto_npassno = ""
END IF

//20140819 암호 변경 6개월 초과 시
if gf_sysdate() > ls_expdate then
	Messagebox("확 인", "암호를 변경하신지 6개월이 지났습니다. 변경하여 주시기 바랍니다.")
	Return
end if

//IF lenA(ls_passwd) < 9 THEN
//	Messagebox("확 인", "비밀번호를 8자리 이상으로 변경하셔야 합니다.!")
//	Return -1
//ELSE
//	IF posA(ls_passwd, ' ') > 0 THEN
//		Messagebox("확 인", "비밀번호를 공백(스페이스)를 제외한 6자리 이상으로 변경하셔야 합니다.!")
//		Return -1
//	END IF
//END IF


//// 패스워드 강화 (2014.10)
// IF ls_empstat <> 'S' AND ls_empstat <> 'I' AND left(ls_empstat,1) <> 'R'  THEN
//	IF wf_passchk() = false then
//		messagebox("확인","개인정보의 안전성 확보 강화를 위하여 비밀번호를 변경 해주시기를 부탁드립니다. ~r~n~r~n&
//최소10자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  2종류 조합 ~r~n&
//최소  8자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  3종류 조합")
////		return
//	END IF
//END IF
SELECT to_char(sysdate, 'dd') INTO :ls_chk FROM dual USING SQLCA;

IF Upper(sle_passwd.text) = 'MEGA'+ls_chk THEN
		is_sabun = ls_sabun
		Close(parent)
		Return

ELSE

	// 패스워드 암호화 (2012.10.12 : 김진영)
	IF ls_crypto_npassno = "" OR ISNULL(ls_crypto_npassno) THEN
		IF Upper(sle_passwd.text) = Upper(ls_passwd) THEN
			is_sabun = ls_sabun
			IF ib_ini AND is_oldid <> is_sabun THEN
				setProfileString(is_ini, "User", "Userid", is_sabun)
			END IF
			Close(parent)
			Return
		ELSE

			IF MessageBox(gf_msg("H0001"), gf_msg("E0020")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
				sle_passwd.Setfocus()
				Return
			ELSE
				Close(parent)
				Return
			END IF
		END IF
	ELSE

		SELECT rawtohex(dbms_crypto.hash(to_clob(:sle_passwd.Text),3))
		  INTO :ls_crypto_passwd
		  FROM DUAL
		USING SQLCA;

		IF ls_crypto_passwd = ls_crypto_npassno THEN
			IF ls_empstat <> 'S' AND ls_empstat <> 'I' AND left(ls_empstat,1) <> 'R'  THEN
				IF wf_passchk(sle_passwd.Text) = false then
					messagebox("확인","개인정보의 안전성 확보 강화를 위하여 비밀번호를 변경 해주시기를 부탁드립니다. ~r~n~r~n" +&
                                 "최소10자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  2종류 조합 ~r~n" +&
                                 "최소  8자리 :~r~n대문자(A-Z), 소문자(a-z), 숫자(0-9), 특수문자(32개) 중  3종류 조합")
					return
				END IF
			END IF				
			
			is_sabun = ls_sabun
			
			IF ib_ini AND is_oldid <> is_sabun THEN
				setProfileString(is_ini, "User", "Userid", is_sabun)
			END IF
			Close(parent)
			Return
		ELSE


			IF MessageBox(gf_msg("H0001"), gf_msg("E0020")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
				sle_passwd.Setfocus()
				Return
			ELSE			
				Close(parent)
				Return
			END IF
		END IF
	END IF
		
	/* 기존 소스 주석처리 (2012.10.12 : 김진영)
	IF Upper(sle_passwd.text) = Upper(ls_passwd) THEN
		is_sabun = ls_sabun
		IF ib_ini AND is_oldid <> is_sabun THEN
			setProfileString(is_ini, "User", "Userid", is_sabun)
		END IF
	
		Close(parent)
		Return
	ELSE
		IF MessageBox(gf_msg("H0001"), gf_msg("E0020")+"~r~n"+gf_msg("Q0060"), Question!, YesNo!)=1 THEN
			sle_passwd.Setfocus()
			Return
		ELSE
			Close(parent)
			Return
		END IF
	END IF*/
END IF
end event

event help;MessageBox(gf_msg("H0006"), gf_msg("I0060") )

end event

type st_3 from statictext within w_logon_msg
integer x = 2094
integer y = 252
integer width = 530
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "암      호(&P):"
boolean focusrectangle = false
end type

type st_2 from statictext within w_logon_msg
integer x = 2094
integer y = 160
integer width = 530
integer height = 64
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "사용자 사번(&U):"
boolean focusrectangle = false
end type

type st_1 from statictext within w_logon_msg
integer x = 2094
integer y = 60
integer width = 1257
integer height = 68
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "사용자 사번과 암호를 입력하십시오."
boolean focusrectangle = false
end type

type st_5 from statictext within w_logon_msg
integer x = 1979
integer y = 412
integer width = 2098
integer height = 328
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type p_miso from picture within w_logon_msg
integer x = 27
integer y = 28
integer width = 1934
integer height = 2400
boolean originalsize = true
string picturename = "C:\fatima-v2\temp\미소작업600.jpg"
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type p_2 from picture within w_logon_msg
boolean visible = false
integer x = 2208
integer y = 136
integer width = 169
integer height = 148
boolean originalsize = true
string picturename = "c:\fatimav12\icon\id.bmp"
boolean focusrectangle = false
end type

type dw_boarderlist from datawindow within w_logon_msg
integer x = 1979
integer y = 420
integer width = 2098
integer height = 824
integer taborder = 110
boolean bringtotop = true
string title = "none"
string dataobject = "d_boarderlist"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;SetTransObject(sqlca)
end event

event retrieveend;if rowcount <= 0 then
	return
end if
This.SelectRow(0, FALSE)
This.SelectRow(1, true)
dw_boarderview.Retrieve(This.Object.no[1])
end event

event clicked;if row <= 0 then
	return
end if
This.SelectRow(0, FALSE)
This.SelectRow(row, true)
dw_boarderview.Retrieve(This.Object.no[row])
end event

type st_8 from statictext within w_logon_msg
integer x = 1979
integer y = 28
integer width = 2098
integer height = 388
integer textsize = -10
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_employee from datawindow within w_logon_msg
boolean visible = false
integer x = 2290
integer y = 1196
integer width = 1637
integer height = 1208
integer taborder = 100
boolean bringtotop = true
boolean titlebar = true
string title = "dw_employee"
string dataobject = "d_employee"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_boarderview from datawindow within w_logon_msg
integer x = 1979
integer y = 1244
integer width = 2098
integer height = 1316
integer taborder = 100
boolean bringtotop = true
string title = "none"
string dataobject = "d_boarderview"
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event constructor;SetTransObject(sqlca)
end event

