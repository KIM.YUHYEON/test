﻿$PBExportHeader$uo_ftp.sru
forward
global type uo_ftp from nonvisualobject
end type
end forward

global type uo_ftp from nonvisualobject autoinstantiate
end type

type prototypes
//FTP관련 함수들...
FUNCTION INT FTPConnect (String IP_ADDRESS, String USER_ID, String PASSWORD) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPConnect;Ansi"
FUNCTION INT FTPDisconnect ( ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL"
FUNCTION INT FTPgetCurrentDir (REF BLOB DATA) LIBRARY "C:\fatimav19\mgdll\FTP.DLL"
FUNCTION INT FTPsetCurrentDirUp () LIBRARY "C:\fatimav19\mgdll\FTP.DLL"
FUNCTION INT FTPsetCurrentDir (String CURR_DIR) LIBRARY "FTP.DLL" alias for "C:\fatimav19\mgdll\FTPsetCurrentDir;Ansi"
FUNCTION INT FTPfileList ( REF BLOB DATA ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL"
FUNCTION INT FTPgetFile ( String REMOTE_FILE, String LOCAL_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPgetFile;Ansi"
FUNCTION INT FTPcreateDir ( String NEW_DIR ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPcreateDir;Ansi"
FUNCTION INT FTPdeleteFile ( String DEL_FILE ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPdeleteFile;Ansi"
FUNCTION INT FTPdeleteDir ( String DEL_DIR ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPdeleteDir;Ansi"
FUNCTION INT FTPrenameDir ( String CURR_DIR, String NEW_DIR ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPrenameDir;Ansi"
FUNCTION INT FTPputFile ( String LOCAL_FILE, String REMOTE_FILE, integer BINARY_OR_TEXT ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPputFile;Ansi"
FUNCTION INT FTPping ( String IPaddress ) LIBRARY "C:\fatimav19\mgdll\FTP.DLL" alias for "FTPping;Ansi"
end prototypes

on uo_ftp.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ftp.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

