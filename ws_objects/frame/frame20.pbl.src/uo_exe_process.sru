﻿$PBExportHeader$uo_exe_process.sru
forward
global type uo_exe_process from nonvisualobject
end type
end forward

global type uo_exe_process from nonvisualobject
end type
global uo_exe_process uo_exe_process

type prototypes
FUNCTION boolean CloseHandle(ulong h) library 'kernel32.dll'
FUNCTION ulong OpenProcess(ulong dwDesiredAccess,  boolean bInheritHandle, ulong dwProcessId ) library 'kernel32.dll'
FUNCTION boolean TerminateProcess(ulong handle, uint exitcode) library "kernel32.dll"
FUNCTION ulong CreateToolhelp32Snapshot(long dwFlags, long th32ProcessID) library 'kernel32.dll' 
FUNCTION boolean Process32First(ulong hSnapshot, ref lstr_PROCESSENTRY32 lppe) library 'kernel32.dll' alias for "Process32First;Ansi"
FUNCTION boolean Process32Next(ulong hSnapshot, ref lstr_PROCESSENTRY32 lppe) library 'kernel32.dll' alias for "Process32Next;Ansi"

FUNCTION boolean EnumProcesses(ref ulong lpidProcess[],  ulong cb, ref ulong cbneeded ) library 'psapi.dll'
FUNCTION boolean EnumProcessModules(ulong hProcess, ref ulong lphModule, ulong cb, ref ulong lpcbNeeded  ) library 'psapi.dll'
FUNCTION ulong GetModuleBaseName(ulong hProcess, ulong hModule, ref string lpBaseName, ulong nSize ) library 'psapi.dll' alias for "GetModuleBaseNameA;Ansi"
SUBROUTINE SetFocus(long objhandle) LIBRARY "User32.dll"
FUNCTION boolean BringWindowToTop(ulong w_handle) LIBRARY "User32.dll"
Function int GetWindowTextA( Long hWindow, ref string windowtext, int length)  Library "USER32.DLL"
FUNCTION uInt GetWindow( uInt hWindow, Int nRelationShip) Library "user32.dll"
FUNCTION long SendMessageA(ulong hwndle,UINT wmsg,ulong wParam,ulong lParam) Library "User32.dll"
FUNCTION boolean PostMessageA(ulong hwndle,UINT wmsg,ulong wParam,ulong lParam) Library "User32.dll"
SUBROUTINE ExitThread(ulong thandle) LIBRARY "kernel32.dll"
FUNCTION ulong CreateMutexA (ulong lpMutexAttributes,  int bInitialOwner,  ref string lpName) library "kernel32.dll"
FUNCTION ulong GetLastError () library "kernel32.dll"
FUNCTION boolean ExitProcess(ulong handle) library "kernel32.dll"
FUNCTION long GetCurrentProcessId() LIBRARY "kernel32.dll"


end prototypes

type variables
CONSTANT LONG TH32CS_SNAPPROCESS = 2



end variables

forward prototypes
public function integer of_dupcheck (string as_exename)
public function integer of_killprocessid (unsignedlong aul_processid)
end prototypes

public function integer of_dupcheck (string as_exename);// 반환값 = 0 : 중복 프로그램 없음 
// 반환값 > 1 : 중복 프로그램이 존재함.

Ulong   ll_th32processid
Ulong   lul_SnapShot
Boolean rtn_chk
Long    hWindow,hNextWindow
String  szText,Ls_Frame_Title, ls_filename
Integer	li_dupCheck = 0
Long	ll_pid
			
ll_pid = GetCurrentProcessId()

lstr_processentry32 lstr_ProcEntry

as_exename = Upper(as_exename)
ll_th32processid = 0

lul_SnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0)

IF lul_SnapShot <> -1 THEN   
   lstr_ProcEntry.dwSize = 296
   rtn_chk = Process32First(lul_SnapShot, lstr_ProcEntry)

   DO WHILE( rtn_chk )  
      ls_filename = Upper(lstr_ProcEntry.szexefile)
		
      if as_exename = ls_filename AND lstr_ProcEntry.th32processid <> ll_pid then
         ll_th32processid = lstr_ProcEntry.th32processid
			
			Exit
      end if
      rtn_chk = Process32Next(lul_SnapShot, lstr_ProcEntry)  
   LOOP 
   CloseHandle(lul_SnapShot)
END IF

Return ll_th32processid
end function

public function integer of_killprocessid (unsignedlong aul_processid);Ulong lul_THandle

IF aul_Processid > 0 THEN
 lul_THandle = OpenProcess(1, False, aul_Processid )
 TerminateProcess(lul_THandle, 0)
 CloseHandle(lul_THandle)
END IF

Return 0

end function

on uo_exe_process.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_exe_process.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

