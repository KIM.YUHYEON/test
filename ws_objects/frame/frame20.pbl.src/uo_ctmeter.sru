﻿$PBExportHeader$uo_ctmeter.sru
$PBExportComments$Progress Bar
forward
global type uo_ctmeter from userobject
end type
type st_2 from statictext within uo_ctmeter
end type
type meter from statictext within uo_ctmeter
end type
end forward

global type uo_ctmeter from userobject
integer width = 2016
integer height = 144
long backcolor = 79741120
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
st_2 st_2
meter meter
end type
global uo_ctmeter uo_ctmeter

forward prototypes
public subroutine uf_meter (long max, long i)
end prototypes

public subroutine uf_meter (long max, long i);Integer	li_i,ix,iy,xx,yy

IF i = max THEN
	li_i = 100
ELSE
	li_i = i / max * 100
END IF

IF li_i > 100 THEN li_i = 100

st_2.Text = String(li_i) + "% "

st_2.Width = meter.Width * li_i / 100 - 10
end subroutine

on uo_ctmeter.create
this.st_2=create st_2
this.meter=create meter
this.Control[]={this.st_2,&
this.meter}
end on

on uo_ctmeter.destroy
destroy(this.st_2)
destroy(this.meter)
end on

type st_2 from statictext within uo_ctmeter
integer x = 14
integer y = 12
integer width = 41
integer height = 96
integer textsize = -14
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 65535
long backcolor = 16711680
boolean enabled = false
string text = "0%"
alignment alignment = right!
boolean focusrectangle = false
end type

type meter from statictext within uo_ctmeter
integer x = 9
integer y = 4
integer width = 1984
integer height = 116
integer textsize = -14
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 65535
long backcolor = 1090519039
boolean enabled = false
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

