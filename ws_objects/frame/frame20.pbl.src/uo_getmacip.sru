﻿$PBExportHeader$uo_getmacip.sru
forward
global type uo_getmacip from nonvisualobject
end type
end forward

global type uo_getmacip from nonvisualobject autoinstantiate
end type

type prototypes
Function integer GetIPAddress ( ref string buf, integer len ) library "C:\fatimav19\MGDLL\getmacip.dll" alias for "GetIPAddress;Ansi" 
Function integer GetMACAddress( ref string buf, integer len ) library "C:\fatimav19\MGDLL\getmacip.dll" alias for "GetMACAddress;Ansi" 
end prototypes

on uo_getmacip.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_getmacip.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

