﻿$PBExportHeader$uo_date.sru
$PBExportComments$날짜 선택하는 Object, uf_date('20000101') : 날짜 지정, uf_text() :  날짜 선택후 '20000101' 로 Return
forward
global type uo_date from userobject
end type
type pb_1 from picturebutton within uo_date
end type
type em_day from editmask within uo_date
end type
type em_year from editmask within uo_date
end type
type em_month from editmask within uo_date
end type
end forward

global type uo_date from userobject
integer width = 768
integer height = 92
long backcolor = 67108864
string text = "none"
long tabtextcolor = 33554432
long picturemaskcolor = 536870912
pb_1 pb_1
em_day em_day
em_year em_year
em_month em_month
end type
global uo_date uo_date

type variables
String is_small
end variables

forward prototypes
public function string uf_text ()
public subroutine uf_date (string as_date)
public subroutine uf_calendar (string as_small)
end prototypes

public function string uf_text ();return em_year.Text + em_month.Text + em_day.Text
end function

public subroutine uf_date (string as_date);IF as_date = '' THEN
	em_year.Text = leftA(gf_sysdate(), 4)
	em_month.Text = midA(gf_sysdate(), 5, 2)
	em_day.Text = rightA(gf_sysdate(), 2)
ELSE
	em_year.Text = leftA(as_date, 4)
	em_month.Text = midA(as_date, 5, 2)
	em_day.Text = rightA(as_date, 2)
END IF
	
end subroutine

public subroutine uf_calendar (string as_small);IF as_small = '1' THEN
	is_small = '1'
ELSE
	is_small = ''
END IF
end subroutine

on uo_date.create
this.pb_1=create pb_1
this.em_day=create em_day
this.em_year=create em_year
this.em_month=create em_month
this.Control[]={this.pb_1,&
this.em_day,&
this.em_year,&
this.em_month}
end on

on uo_date.destroy
destroy(this.pb_1)
destroy(this.em_day)
destroy(this.em_year)
destroy(this.em_month)
end on

event constructor;uf_date('')

end event

type pb_1 from picturebutton within uo_date
integer width = 110
integer height = 96
integer taborder = 40
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
string picturename = "c:\fatimav19\icon\calendar_es.bmp"
alignment htextalign = left!
end type

event clicked;String ls_date

IF is_small = '1' THEN
	OpenWithParm(w_calendar_small, gf_sysdate())
ELSE
	OpenWithParm(w_calendar, gf_sysdate())
END IF

ls_date = Message.StringParm

IF ls_date = '' THEN
	em_year.Text = leftA(gf_sysdate(), 4)
	em_month.Text = midA(gf_sysdate(), 5, 2)
	em_day.Text = rightA(gf_sysdate(), 2)
ELSE
	em_year.Text = leftA(ls_date, 4)
	em_month.Text = midA(ls_date, 5, 2)
	em_day.Text = rightA(ls_date, 2)
END IF
end event

type em_day from editmask within uo_date
integer x = 581
integer width = 187
integer height = 96
integer taborder = 30
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = styleraised!
string mask = "00"
boolean spin = true
string displaydata = "~t/"
double increment = 1
string minmax = "1~~31"
end type

type em_year from editmask within uo_date
integer x = 114
integer width = 270
integer height = 96
integer taborder = 10
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = styleraised!
string mask = "0000"
boolean autoskip = true
boolean spin = true
double increment = 1
string minmax = "1900~~2999"
end type

type em_month from editmask within uo_date
integer x = 389
integer width = 187
integer height = 96
integer taborder = 20
integer textsize = -11
integer weight = 700
fontcharset fontcharset = hangeul!
fontpitch fontpitch = fixed!
fontfamily fontfamily = modern!
string facename = "굴림체"
long textcolor = 33554432
borderstyle borderstyle = styleraised!
string mask = "00"
boolean autoskip = true
boolean spin = true
double increment = 1
string minmax = "1~~12"
end type

event modified;String ls_minmax, ls_max

ls_max = String(gf_countday(em_year.Text + em_month.Text))

ls_minmax = '1 ~~ ' + ls_max

em_day.MinMax = (ls_minmax)

IF em_day.Text > ls_max THEN
	em_day.Text = ls_max
END IF
end event

