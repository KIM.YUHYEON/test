﻿$PBExportHeader$w_anc_list_master_h.srw
$PBExportComments$dw Cond & List & Master & detail
forward
global type w_anc_list_master_h from w_anc_sheet
end type
type dw_master from pf_u_datawindow within w_anc_list_master_h
end type
type dw_list from pf_u_datawindow within w_anc_list_master_h
end type
type dw_cond from pf_u_datawindow within w_anc_list_master_h
end type
type st_splitbar_horizontal from pf_u_splitbar_horizontal within w_anc_list_master_h
end type
end forward

global type w_anc_list_master_h from w_anc_sheet
dw_master dw_master
dw_list dw_list
dw_cond dw_cond
st_splitbar_horizontal st_splitbar_horizontal
end type
global w_anc_list_master_h w_anc_list_master_h

on w_anc_list_master_h.create
int iCurrent
call super::create
this.dw_master=create dw_master
this.dw_list=create dw_list
this.dw_cond=create dw_cond
this.st_splitbar_horizontal=create st_splitbar_horizontal
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_master
this.Control[iCurrent+2]=this.dw_list
this.Control[iCurrent+3]=this.dw_cond
this.Control[iCurrent+4]=this.st_splitbar_horizontal
end on

on w_anc_list_master_h.destroy
call super::destroy
destroy(this.dw_master)
destroy(this.dw_list)
destroy(this.dw_cond)
destroy(this.st_splitbar_horizontal)
end on

type ln_templeft from w_anc_sheet`ln_templeft within w_anc_list_master_h
end type

type ln_tempright from w_anc_sheet`ln_tempright within w_anc_list_master_h
integer beginx = 4457
integer endx = 4457
end type

type ln_temptop from w_anc_sheet`ln_temptop within w_anc_list_master_h
end type

type ln_tempbuttom from w_anc_sheet`ln_tempbuttom within w_anc_list_master_h
end type

type ln_tempbutton from w_anc_sheet`ln_tempbutton within w_anc_list_master_h
end type

type ln_tempstart from w_anc_sheet`ln_tempstart within w_anc_list_master_h
end type

type p_retrieve from w_anc_sheet`p_retrieve within w_anc_list_master_h
integer x = 2802
end type

type p_new from w_anc_sheet`p_new within w_anc_list_master_h
integer x = 3081
end type

type p_save from w_anc_sheet`p_save within w_anc_list_master_h
integer x = 3360
end type

type p_print from w_anc_sheet`p_print within w_anc_list_master_h
integer x = 3639
end type

type p_excel from w_anc_sheet`p_excel within w_anc_list_master_h
integer x = 3918
end type

type p_delete from w_anc_sheet`p_delete within w_anc_list_master_h
integer x = 4197
end type

type dw_master from pf_u_datawindow within w_anc_list_master_h
integer x = 73
integer y = 1212
integer width = 4384
integer height = 1408
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
boolean scaletobottom = true
end type

type dw_list from pf_u_datawindow within w_anc_list_master_h
integer x = 73
integer y = 548
integer width = 4384
integer height = 644
integer taborder = 30
boolean bringtotop = true
boolean scaletobottom = true
end type

type dw_cond from pf_u_datawindow within w_anc_list_master_h
integer x = 73
integer y = 184
integer width = 4384
integer height = 328
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
end type

type st_splitbar_horizontal from pf_u_splitbar_horizontal within w_anc_list_master_h
integer x = 73
integer y = 1192
integer width = 4393
boolean bringtotop = true
boolean scaletoright = true
end type

event constructor;call super::constructor;of_set_topobject(dw_list)
of_set_bottomobject(dw_master)

end event

