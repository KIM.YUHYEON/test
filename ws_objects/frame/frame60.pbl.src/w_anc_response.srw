﻿$PBExportHeader$w_anc_response.srw
forward
global type w_anc_response from pf_w_response
end type
type uc_retrieve from pf_u_picture within w_anc_response
end type
type uc_cancel from pf_u_picture within w_anc_response
end type
type uc_close from pf_u_picture within w_anc_response
end type
type uc_ok from pf_u_picture within w_anc_response
end type
type ln_templeft from line within w_anc_response
end type
type ln_temptop from line within w_anc_response
end type
type ln_tempbutton from line within w_anc_response
end type
type ln_tempstart from line within w_anc_response
end type
type ln_4 from line within w_anc_response
end type
type ln_tempright from line within w_anc_response
end type
end forward

global type w_anc_response from pf_w_response
integer width = 3525
integer height = 1692
string title = "Untitled"
long backcolor = 16777215
event ue_postopen ( )
event ue_ok ( )
event ue_close ( )
event ue_retrieve ( )
event ue_cancel ( )
uc_retrieve uc_retrieve
uc_cancel uc_cancel
uc_close uc_close
uc_ok uc_ok
ln_templeft ln_templeft
ln_temptop ln_temptop
ln_tempbutton ln_tempbutton
ln_tempstart ln_tempstart
ln_4 ln_4
ln_tempright ln_tempright
end type
global w_anc_response w_anc_response

event ue_ok();//
end event

event ue_close();//
end event

event ue_retrieve();//
end event

on w_anc_response.create
int iCurrent
call super::create
this.uc_retrieve=create uc_retrieve
this.uc_cancel=create uc_cancel
this.uc_close=create uc_close
this.uc_ok=create uc_ok
this.ln_templeft=create ln_templeft
this.ln_temptop=create ln_temptop
this.ln_tempbutton=create ln_tempbutton
this.ln_tempstart=create ln_tempstart
this.ln_4=create ln_4
this.ln_tempright=create ln_tempright
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.uc_retrieve
this.Control[iCurrent+2]=this.uc_cancel
this.Control[iCurrent+3]=this.uc_close
this.Control[iCurrent+4]=this.uc_ok
this.Control[iCurrent+5]=this.ln_templeft
this.Control[iCurrent+6]=this.ln_temptop
this.Control[iCurrent+7]=this.ln_tempbutton
this.Control[iCurrent+8]=this.ln_tempstart
this.Control[iCurrent+9]=this.ln_4
this.Control[iCurrent+10]=this.ln_tempright
end on

on w_anc_response.destroy
call super::destroy
destroy(this.uc_retrieve)
destroy(this.uc_cancel)
destroy(this.uc_close)
destroy(this.uc_ok)
destroy(this.ln_templeft)
destroy(this.ln_temptop)
destroy(this.ln_tempbutton)
destroy(this.ln_tempstart)
destroy(this.ln_4)
destroy(this.ln_tempright)
end on

event open;Post Event ue_postopen()
end event

type uc_retrieve from pf_u_picture within w_anc_response
integer x = 2363
integer y = 36
integer width = 265
integer height = 84
boolean originalsize = false
string picturename = "..\img\button\imagebutton\topbtn_search.gif"
end type

type uc_cancel from pf_u_picture within w_anc_response
integer x = 2926
integer y = 36
integer width = 265
integer height = 84
boolean originalsize = false
string picturename = "..\img\button\imagebutton\topBtn_cancel.gif"
end type

type uc_close from pf_u_picture within w_anc_response
integer x = 3205
integer y = 36
integer width = 265
integer height = 84
boolean originalsize = false
string picturename = "..\img\button\imagebutton\topBtn_close.gif"
end type

type uc_ok from pf_u_picture within w_anc_response
integer x = 2647
integer y = 36
integer width = 265
integer height = 84
boolean originalsize = false
string picturename = "..\img\button\imagebutton\topBtn_ok.gif"
end type

type ln_templeft from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginx = 46
integer beginy = 24
integer endx = 46
integer endy = 2272
end type

type ln_temptop from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginy = 36
integer endx = 3863
integer endy = 36
end type

type ln_tempbutton from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginy = 120
integer endx = 3863
integer endy = 120
end type

type ln_tempstart from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginy = 160
integer endx = 3863
integer endy = 160
end type

type ln_4 from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginy = 1548
integer endx = 4471
integer endy = 1548
end type

type ln_tempright from line within w_anc_response
boolean visible = false
long linecolor = 134217857
integer linethickness = 4
integer beginx = 3465
integer beginy = 24
integer endx = 3465
integer endy = 2272
end type

