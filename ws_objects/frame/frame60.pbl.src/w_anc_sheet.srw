﻿$PBExportHeader$w_anc_sheet.srw
forward
global type w_anc_sheet from pf_w_sheet
end type
type p_retrieve from pf_u_imagebutton within w_anc_sheet
end type
type p_new from pf_u_imagebutton within w_anc_sheet
end type
type p_save from pf_u_imagebutton within w_anc_sheet
end type
type p_print from pf_u_imagebutton within w_anc_sheet
end type
type p_excel from pf_u_imagebutton within w_anc_sheet
end type
type p_delete from pf_u_imagebutton within w_anc_sheet
end type
type p_help from pf_u_imagebutton within w_anc_sheet
end type
end forward

global type w_anc_sheet from pf_w_sheet
integer width = 4535
event ue_btn_retrieve ( )
event ue_btn_new ( )
event ue_btn_save ( )
event ue_btn_print ( )
event ue_btn_excel ( )
event ue_btn_delete ( )
event ue_btn_pre_print ( )
event ue_btn_help ( )
p_retrieve p_retrieve
p_new p_new
p_save p_save
p_print p_print
p_excel p_excel
p_delete p_delete
p_help p_help
end type
global w_anc_sheet w_anc_sheet

type variables
Datawindow idw_print
end variables

event ue_btn_print();Vector 		lvc_data //출력팝업
Long			ll_rowcnt
DataStore	lds_ds
lvc_data = Create Vector
lds_ds = Create DataStore	

ll_rowcnt = idw_print.RowCount( )
IF ll_rowcnt = 0 THEN RETURN 

lds_ds.DataObject = idw_print.DataObject
lds_ds.Object.Data = idw_print.Object.Data 

lvc_data.setproperty( 'dwtype', 'datastore')
lvc_data.setproperty( 'datastore', lds_ds)
lvc_data.setproperty( 'printsize', lds_ds.Describe("DataWindow.Print.Paper.Size") )
lvc_data.setproperty( 'orientation', lds_ds.Describe("DataWindow.Print.Orientation") )
lvc_data.setproperty( 'printzoom', '100')
lvc_data.setproperty( 'zoom', '100')

Openwithparm(pf_w_print_view, lvc_data)

Destroy lvc_data
end event

event ue_btn_pre_print();//idw_print = 'dw_print'
end event

on w_anc_sheet.create
int iCurrent
call super::create
this.p_retrieve=create p_retrieve
this.p_new=create p_new
this.p_save=create p_save
this.p_print=create p_print
this.p_excel=create p_excel
this.p_delete=create p_delete
this.p_help=create p_help
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.p_retrieve
this.Control[iCurrent+2]=this.p_new
this.Control[iCurrent+3]=this.p_save
this.Control[iCurrent+4]=this.p_print
this.Control[iCurrent+5]=this.p_excel
this.Control[iCurrent+6]=this.p_delete
this.Control[iCurrent+7]=this.p_help
end on

on w_anc_sheet.destroy
call super::destroy
destroy(this.p_retrieve)
destroy(this.p_new)
destroy(this.p_save)
destroy(this.p_print)
destroy(this.p_excel)
destroy(this.p_delete)
destroy(this.p_help)
end on

event ue_postopen;call super::ue_postopen;//프로그램명 셋팅
//This.title= inv_menu.is_pgm_name 
end event

type ln_templeft from pf_w_sheet`ln_templeft within w_anc_sheet
end type

type ln_tempright from pf_w_sheet`ln_tempright within w_anc_sheet
integer beginx = 4462
integer endx = 4462
end type

type ln_temptop from pf_w_sheet`ln_temptop within w_anc_sheet
end type

type ln_tempbuttom from pf_w_sheet`ln_tempbuttom within w_anc_sheet
end type

type ln_tempbutton from pf_w_sheet`ln_tempbutton within w_anc_sheet
end type

type ln_tempstart from pf_w_sheet`ln_tempstart within w_anc_sheet
end type

type p_retrieve from pf_u_imagebutton within w_anc_sheet
integer x = 2505
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
boolean originalsize = true
string picturename = "..\img\button\imagebutton\topbtn_search.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_retrieve()
end event

type p_new from pf_u_imagebutton within w_anc_sheet
integer x = 2784
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
string picturename = "..\img\button\imagebutton\topbtn_add.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_new()
end event

type p_save from pf_u_imagebutton within w_anc_sheet
integer x = 3063
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
string picturename = "..\img\button\imagebutton\topbtn_save.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_save()
end event

type p_print from pf_u_imagebutton within w_anc_sheet
integer x = 3342
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
string picturename = "..\img\button\imagebutton\topbtn_print.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_pre_print()
Parent.Event ue_btn_print()
end event

type p_excel from pf_u_imagebutton within w_anc_sheet
integer x = 3621
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
string picturename = "..\img\button\imagebutton\topbtn_excel.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_excel()
end event

type p_delete from pf_u_imagebutton within w_anc_sheet
integer x = 3899
integer y = 40
integer width = 261
integer height = 84
boolean bringtotop = true
string picturename = "..\img\button\imagebutton\topbtn_delete.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;gf_frame_history(gs_sabun,GetApplication().Appname,inv_menu.is_pgm_id,this.classname(),'CLICKED')
Parent.Event ue_btn_delete()
end event

type p_help from pf_u_imagebutton within w_anc_sheet
integer x = 4178
integer y = 40
integer width = 283
integer height = 84
boolean bringtotop = true
boolean originalsize = true
string picturename = "..\img\button\imagebutton\topbtn_help.gif"
boolean fixedtoright = true
end type

event clicked;call super::clicked;Parent.Event ue_btn_help()
end event

