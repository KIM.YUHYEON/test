﻿$PBExportHeader$w_anc_treeviewlist_master_detail.srw
$PBExportComments$dw Cond & List & Master & detail
forward
global type w_anc_treeviewlist_master_detail from w_anc_sheet
end type
type dw_master from pf_u_datawindow within w_anc_treeviewlist_master_detail
end type
type st_splitbar_vertical from pf_u_splitbar_vertical within w_anc_treeviewlist_master_detail
end type
type dw_cond from pf_u_datawindow within w_anc_treeviewlist_master_detail
end type
type st_splitbar_horizontal from pf_u_splitbar_horizontal within w_anc_treeviewlist_master_detail
end type
type dw_detail from pf_u_datawindow within w_anc_treeviewlist_master_detail
end type
type tv_pf_list from pf_u_treeview within w_anc_treeviewlist_master_detail
end type
end forward

global type w_anc_treeviewlist_master_detail from w_anc_sheet
dw_master dw_master
st_splitbar_vertical st_splitbar_vertical
dw_cond dw_cond
st_splitbar_horizontal st_splitbar_horizontal
dw_detail dw_detail
tv_pf_list tv_pf_list
end type
global w_anc_treeviewlist_master_detail w_anc_treeviewlist_master_detail

on w_anc_treeviewlist_master_detail.create
int iCurrent
call super::create
this.dw_master=create dw_master
this.st_splitbar_vertical=create st_splitbar_vertical
this.dw_cond=create dw_cond
this.st_splitbar_horizontal=create st_splitbar_horizontal
this.dw_detail=create dw_detail
this.tv_pf_list=create tv_pf_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_master
this.Control[iCurrent+2]=this.st_splitbar_vertical
this.Control[iCurrent+3]=this.dw_cond
this.Control[iCurrent+4]=this.st_splitbar_horizontal
this.Control[iCurrent+5]=this.dw_detail
this.Control[iCurrent+6]=this.tv_pf_list
end on

on w_anc_treeviewlist_master_detail.destroy
call super::destroy
destroy(this.dw_master)
destroy(this.st_splitbar_vertical)
destroy(this.dw_cond)
destroy(this.st_splitbar_horizontal)
destroy(this.dw_detail)
destroy(this.tv_pf_list)
end on

type ln_templeft from w_anc_sheet`ln_templeft within w_anc_treeviewlist_master_detail
end type

type ln_tempright from w_anc_sheet`ln_tempright within w_anc_treeviewlist_master_detail
end type

type ln_temptop from w_anc_sheet`ln_temptop within w_anc_treeviewlist_master_detail
end type

type ln_tempbuttom from w_anc_sheet`ln_tempbuttom within w_anc_treeviewlist_master_detail
end type

type ln_tempbutton from w_anc_sheet`ln_tempbutton within w_anc_treeviewlist_master_detail
end type

type ln_tempstart from w_anc_sheet`ln_tempstart within w_anc_treeviewlist_master_detail
end type

type p_retrieve from w_anc_sheet`p_retrieve within w_anc_treeviewlist_master_detail
end type

type p_new from w_anc_sheet`p_new within w_anc_treeviewlist_master_detail
end type

type p_save from w_anc_sheet`p_save within w_anc_treeviewlist_master_detail
end type

type p_print from w_anc_sheet`p_print within w_anc_treeviewlist_master_detail
end type

type p_excel from w_anc_sheet`p_excel within w_anc_treeviewlist_master_detail
end type

type p_delete from w_anc_sheet`p_delete within w_anc_treeviewlist_master_detail
end type

type dw_master from pf_u_datawindow within w_anc_treeviewlist_master_detail
integer x = 1294
integer y = 548
integer width = 3168
integer height = 660
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
end type

type st_splitbar_vertical from pf_u_splitbar_vertical within w_anc_treeviewlist_master_detail
integer x = 1275
integer y = 544
integer height = 2080
boolean bringtotop = true
boolean scaletobottom = true
end type

event constructor;call super::constructor;of_set_leftobject(tv_pf_list)
of_set_rightobject(dw_master)
of_set_rightobject(dw_detail)
end event

type dw_cond from pf_u_datawindow within w_anc_treeviewlist_master_detail
integer x = 73
integer y = 184
integer width = 4389
integer height = 328
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
end type

type st_splitbar_horizontal from pf_u_splitbar_horizontal within w_anc_treeviewlist_master_detail
integer x = 1271
integer y = 1208
integer width = 3209
boolean bringtotop = true
boolean scaletoright = true
end type

event constructor;call super::constructor;of_set_topobject(dw_master)
of_set_bottomobject(dw_detail)

end event

type dw_detail from pf_u_datawindow within w_anc_treeviewlist_master_detail
integer x = 1294
integer y = 1224
integer width = 3168
integer height = 1396
integer taborder = 40
boolean bringtotop = true
boolean scaletoright = true
boolean scaletobottom = true
end type

type tv_pf_list from pf_u_treeview within w_anc_treeviewlist_master_detail
integer x = 78
integer y = 544
integer width = 1184
integer height = 2076
integer taborder = 40
boolean bringtotop = true
boolean scaletobottom = true
end type

