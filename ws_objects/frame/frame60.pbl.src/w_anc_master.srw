﻿$PBExportHeader$w_anc_master.srw
$PBExportComments$dw Cond & Master
forward
global type w_anc_master from w_anc_sheet
end type
type dw_cond from pf_u_datawindow within w_anc_master
end type
type dw_master from pf_u_datawindow within w_anc_master
end type
end forward

global type w_anc_master from w_anc_sheet
dw_cond dw_cond
dw_master dw_master
end type
global w_anc_master w_anc_master

on w_anc_master.create
int iCurrent
call super::create
this.dw_cond=create dw_cond
this.dw_master=create dw_master
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cond
this.Control[iCurrent+2]=this.dw_master
end on

on w_anc_master.destroy
call super::destroy
destroy(this.dw_cond)
destroy(this.dw_master)
end on

type ln_templeft from w_anc_sheet`ln_templeft within w_anc_master
end type

type ln_tempright from w_anc_sheet`ln_tempright within w_anc_master
end type

type ln_temptop from w_anc_sheet`ln_temptop within w_anc_master
end type

type ln_tempbuttom from w_anc_sheet`ln_tempbuttom within w_anc_master
end type

type ln_tempbutton from w_anc_sheet`ln_tempbutton within w_anc_master
end type

type ln_tempstart from w_anc_sheet`ln_tempstart within w_anc_master
end type

type p_retrieve from w_anc_sheet`p_retrieve within w_anc_master
end type

type p_new from w_anc_sheet`p_new within w_anc_master
end type

type p_save from w_anc_sheet`p_save within w_anc_master
end type

type p_print from w_anc_sheet`p_print within w_anc_master
end type

type p_excel from w_anc_sheet`p_excel within w_anc_master
end type

type p_delete from w_anc_sheet`p_delete within w_anc_master
end type

type dw_cond from pf_u_datawindow within w_anc_master
integer x = 73
integer y = 184
integer width = 4389
integer height = 328
integer taborder = 20
boolean bringtotop = true
boolean scaletoright = true
end type

type dw_master from pf_u_datawindow within w_anc_master
integer x = 73
integer y = 536
integer width = 4389
integer height = 2080
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
boolean scaletobottom = true
end type

