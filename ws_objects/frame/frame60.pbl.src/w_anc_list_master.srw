﻿$PBExportHeader$w_anc_list_master.srw
$PBExportComments$dw Cond & List & Master
forward
global type w_anc_list_master from w_anc_sheet
end type
type dw_master from pf_u_datawindow within w_anc_list_master
end type
type dw_list from pf_u_datawindow within w_anc_list_master
end type
type st_splitbar_vertical from pf_u_splitbar_vertical within w_anc_list_master
end type
type dw_cond from pf_u_datawindow within w_anc_list_master
end type
end forward

global type w_anc_list_master from w_anc_sheet
dw_master dw_master
dw_list dw_list
st_splitbar_vertical st_splitbar_vertical
dw_cond dw_cond
end type
global w_anc_list_master w_anc_list_master

on w_anc_list_master.create
int iCurrent
call super::create
this.dw_master=create dw_master
this.dw_list=create dw_list
this.st_splitbar_vertical=create st_splitbar_vertical
this.dw_cond=create dw_cond
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_master
this.Control[iCurrent+2]=this.dw_list
this.Control[iCurrent+3]=this.st_splitbar_vertical
this.Control[iCurrent+4]=this.dw_cond
end on

on w_anc_list_master.destroy
call super::destroy
destroy(this.dw_master)
destroy(this.dw_list)
destroy(this.st_splitbar_vertical)
destroy(this.dw_cond)
end on

type ln_templeft from w_anc_sheet`ln_templeft within w_anc_list_master
end type

type ln_tempright from w_anc_sheet`ln_tempright within w_anc_list_master
end type

type ln_temptop from w_anc_sheet`ln_temptop within w_anc_list_master
end type

type ln_tempbuttom from w_anc_sheet`ln_tempbuttom within w_anc_list_master
end type

type ln_tempbutton from w_anc_sheet`ln_tempbutton within w_anc_list_master
integer beginx = -5
integer endx = 4585
end type

type ln_tempstart from w_anc_sheet`ln_tempstart within w_anc_list_master
end type

type p_retrieve from w_anc_sheet`p_retrieve within w_anc_list_master
end type

type p_new from w_anc_sheet`p_new within w_anc_list_master
end type

type p_save from w_anc_sheet`p_save within w_anc_list_master
end type

type p_print from w_anc_sheet`p_print within w_anc_list_master
end type

type p_excel from w_anc_sheet`p_excel within w_anc_list_master
end type

type p_delete from w_anc_sheet`p_delete within w_anc_list_master
end type

type p_help from w_anc_sheet`p_help within w_anc_list_master
end type 

type dw_master from pf_u_datawindow within w_anc_list_master
integer x = 1303
integer y = 544
integer width = 3163
integer height = 2076
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
boolean scaletobottom = true
end type

type dw_list from pf_u_datawindow within w_anc_list_master
integer x = 73
integer y = 548
integer width = 1202
integer height = 2072
integer taborder = 30
boolean bringtotop = true
boolean scaletobottom = true
end type

type st_splitbar_vertical from pf_u_splitbar_vertical within w_anc_list_master
integer x = 1280
integer y = 548
integer height = 2084
boolean bringtotop = true
boolean scaletobottom = true
end type

event constructor;call super::constructor;of_set_leftobject(dw_list)
of_set_rightobject(dw_master)
end event

type dw_cond from pf_u_datawindow within w_anc_list_master
integer x = 73
integer y = 184
integer width = 4393
integer height = 328
integer taborder = 30
boolean bringtotop = true
boolean scaletoright = true
end type

