﻿$PBExportHeader$w_xls_sys.srw
forward
global type w_xls_sys from window
end type
type dw_sys from datawindow within w_xls_sys
end type
type st_temp from statictext within w_xls_sys
end type
end forward

global type w_xls_sys from window
boolean visible = false
integer width = 2784
integer height = 772
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_sys dw_sys
st_temp st_temp
end type
global w_xls_sys w_xls_sys

on w_xls_sys.create
this.dw_sys=create dw_sys
this.st_temp=create st_temp
this.Control[]={this.dw_sys,&
this.st_temp}
end on

on w_xls_sys.destroy
destroy(this.dw_sys)
destroy(this.st_temp)
end on

event open;if dw_sys.RowCount() = 0 then
	dw_sys.InsertRow(0)
end if
end event

type dw_sys from datawindow within w_xls_sys
integer y = 20
integer width = 2629
integer height = 604
integer taborder = 10
string title = "none"
string dataobject = "dw_sys"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_temp from statictext within w_xls_sys
boolean visible = false
integer x = 1015
integer y = 208
integer width = 247
integer height = 76
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "0"
boolean focusrectangle = false
end type

