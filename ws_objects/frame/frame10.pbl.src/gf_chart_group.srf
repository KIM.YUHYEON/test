﻿$PBExportHeader$gf_chart_group.srf
$PBExportComments$차트 단체대출 Function
global type gf_chart_group from function_object
end type

forward prototypes
global function integer gf_chart_group (string arg_sabun, string arg_hospno, string arg_io, integer arg_cancel)
end prototypes

global function integer gf_chart_group (string arg_sabun, string arg_hospno, string arg_io, integer arg_cancel);/*********************************************************************************************************/
/*                                                                                                       */
/*                       gf_chart_group Function 정의                                                    */
/*                                                                                                       */
/*  Function              : 보험과 차트 단체대출신청                                                     */
/*  Parameter_Description : 신청자사번, 차트번호, 입원/외래 구분, 취소구분(1:대출, 0:취소) 					*/
/*                          *차트번호 뒤에 퇴원날자가 붙여 넘어옴 2002/08/27 조성우 수정*   		  			*/
/*  Parameter_Name        : arg_sabun(String), arg_hospno(String), arg_io(String), arg_cancel(integer)   */
/*  Return_Value          : 1(정상), -1(오류)                                                            */
/*                                                                                                       */
/*   작 성 일             :  2001 년 11 월 18 일.                 작 성 자 : 김 기 석                    */
/*********************************************************************************************************/

String   ls_time, ls_today, ls_hospno, ls_info, ls_io, ls_ab, ls_albong, ls_odate, ls_chart_usegubun
DateTime ldt_datetime

ls_info = gf_hospital_data('CHART_GROUP') //보험과에서 단체대출 Function 사용여부
ls_albong = gf_hospital_data('CHART_ALBONG')
ls_chart_usegubun = gf_hospital_data('CHART_USEGUBUN')
ls_odate = midA(arg_hospno,10,8)
arg_hospno = leftA(arg_hospno,9)

IF ls_info = '' OR IsNull(ls_info) OR ls_info = 'NO' THEN
	RETURN 1
END IF

IF arg_sabun = '' OR IsNull(arg_sabun) THEN
	MessageBox("알림","신청자 사원번호가 없습니다!", StopSign!)
	RETURN -1
END IF

IF arg_hospno = '' OR IsNull(arg_hospno) THEN
	MessageBox("알림","차트 등록번호가 없습니다!", StopSign!)
	RETURN -1
END IF

IF arg_io = '' OR IsNull(arg_io) THEN
	MessageBox("알림","신청한 차트의 입원/외래 구분이 없습니다!", StopSign!)
	RETURN -1
END IF
		
SELECT TO_CHAR(sysdate, 'HH24:MI:SS'), 
		 TO_CHAR(sysdate, 'YYYY-MM-DD')
INTO   :ls_time, :ls_today   
FROM   DUAL
USING  SQLCA ;

IF Time(ls_time) < Time("10:00:00") THEN
	ls_time = "10:00:00"       // 오전 신청시간으로 Setting!
ELSE
	ls_time = "14:00:00"       // 오후 신청시간으로 Setting!
END IF

ldt_datetime = DateTime(Date(ls_today), Time(ls_time))

/* 2002/08/19 조성우 수정! */
CHOOSE CASE ls_albong
	CASE '1' // 알/봉투 관리함!

		IF arg_io = '1' THEN //외래
			ls_ab = '0' // 기본적으로 봉투
		ELSEIF arg_io = '2' THEN //입원
			ls_ab = '1' // 기본적으로 알
		END IF	
		
		IF arg_cancel = 1 THEN  // 단체대출 신청
			
	 	  SELECT hospno        
			INTO   :ls_hospno
			FROM   th_chartgrp
			WHERE  grpsabun = :arg_sabun
			AND    hospno   = :arg_hospno
			AND    iogubun  = :arg_io
			AND    gubun    = :ls_ab
			AND    rentchk = 0
			USING  SQLCA;
			
			IF Trim(ls_hospno) > '' THEN
				MessageBox("알림","이미 대출신청이 되어 있습니다!")
				RETURN -1
			END IF
						
			IF arg_io = '2' THEN //입원
			
				IF ls_odate = '' THEN
					SELECT MAX(odate)  // 넘어 오는 퇴원일자가 없을 때, 마지막 퇴원일자를 세팅
					  INTO :ls_odate
					  FROM th_summary
					 WHERE hospno = :arg_hospno
					 USING SQLCA;
	 			END IF	
				IF ls_odate = '' or Isnull(ls_odate) THEN ls_odate = ' '
				 
			ELSEIF arg_io = '1' THEN //외래
				ls_odate = ' '				
			END IF
					
					
			INSERT INTO th_chartgrp(GRPDEPT,GRPSABUN,SASABUN,HOSPNO,GRPTIME,IOGUBUN,GUBUN,DEPT,ODATE,USEGUBUN,SEL,RENTCHK,BANNAB,BGRPDEPT)
			VALUES ('53',:arg_sabun,:arg_sabun,:arg_hospno,:ldt_datetime,:arg_io,:ls_ab,' ',:ls_odate,'CHUNG',0,0,0,'53')
			 USING SQLCA;
			 
			IF SQLCA.SQLCODE <> 0 THEN
				ROLLBACK USING SQLCA;
				MessageBox("에러","차트 단체대출신청 저장시 에러가 발생했습니다!~r~n" + & 
										"전산실로 문의하시기 바랍니다.", StopSign!)
				RETURN -1
			END IF
		
		ELSEIF arg_cancel = 0 THEN  // 단체대출 취소
			
			DELETE th_chartgrp
			WHERE  grpsabun = :arg_sabun
			AND    hospno   = :arg_hospno
			AND    iogubun  = :arg_io
			AND    gubun    = :ls_ab
			AND    rentchk = 0
			USING  SQLCA;
			
			IF SQLCA.SQLCODE <> 0 THEN
				ROLLBACK USING SQLCA;
				MessageBox("에러","차트 단체대출신청 저장시 에러가 발생했습니다!~r~n" + & 
										"전산실로 문의하시기 바랍니다.", StopSign!)
				RETURN -1
			END IF
			
		END IF
		
		
	CASE '0' // 알/봉투 관리안함
		
		IF arg_cancel = 1 THEN  // 단체대출 신청
			
	 	  SELECT hospno        
			INTO   :ls_hospno
			FROM   th_chartgrp
			WHERE  grpsabun = :arg_sabun
			AND    hospno   = :arg_hospno
			AND    iogubun  = :arg_io
			AND    rentchk = 0
			USING  SQLCA;
			
			IF Trim(ls_hospno) > '' THEN
				MessageBox("알림","이미 대출신청이 되어 있습니다!")
				RETURN -1
			END IF
			
			IF ls_chart_usegubun = '1' THEN  
				INSERT INTO th_chartgrp(GRPDEPT,GRPSABUN,SASABUN,HOSPNO,GRPTIME,IOGUBUN,GUBUN,DEPT,ODATE,USEGUBUN,SEL,RENTCHK,BANNAB,BGRPDEPT)
				VALUES ('53',:arg_sabun,:arg_sabun,:arg_hospno,:ldt_datetime,:arg_io,' ',' ',' ','CHUNG',0,0,0,'53') 
				USING SQLCA;
			ELSE	// 청주용
				INSERT INTO th_chartgrp(GRPDEPT,GRPSABUN,HOSPNO,GRPTIME,IOGUBUN,USEGUBUN,SEL,RENTCHK,BANNAB)
				VALUES ('53',:arg_sabun,:arg_hospno,:ldt_datetime,:arg_io,'8',0,0,0) 
				USING SQLCA;
			END IF
						
			IF SQLCA.SQLCODE <> 0 THEN
				ROLLBACK USING SQLCA;
				MessageBox("에러","차트 단체대출신청 저장시 에러가 발생했습니다!~r~n" + & 
										"전산실로 문의하시기 바랍니다.", StopSign!)
				RETURN -1
			END IF
		
		ELSEIF arg_cancel = 0 THEN  // 단체대출 취소
			
			DELETE th_chartgrp
			WHERE  grpsabun = :arg_sabun
			AND    hospno   = :arg_hospno
			AND    iogubun  = :arg_io
			AND    rentchk = 0
			USING  SQLCA;
			
			IF SQLCA.SQLCODE <> 0 THEN
				ROLLBACK USING SQLCA;
				MessageBox("에러","차트 단체대출신청 저장시 에러가 발생했습니다!~r~n" + & 
										"전산실로 문의하시기 바랍니다.", StopSign!)
				RETURN -1
			END IF
		END IF
		
END CHOOSE
	
COMMIT USING SQLCA ;

RETURN 1

end function

