﻿$PBExportHeader$gf_specialmed_chk.srf
global type gf_specialmed_chk from function_object
end type

forward prototypes
global function integer gf_specialmed_chk (string a_wkgubun, string a_hospno, string a_bsdate, datetime a_bstime, string a_dept, string a_drcode, string a_stat, integer a_special, string a_patkind, string a_pattype, string a_dccode, string as_deptstat)
end prototypes

global function integer gf_specialmed_chk (string a_wkgubun, string a_hospno, string a_bsdate, datetime a_bstime, string a_dept, string a_drcode, string a_stat, integer a_special, string a_patkind, string a_pattype, string a_dccode, string as_deptstat);/*

Function : GF_SPECIALMED_CHK
생성일자 : 2015.05.28
개    요 : C020197.f_c020197_specialmed_chk()함수를 공용으로 이용하기 위해 gf_specialmed_chk()함수 생성함.

□ Parament 정의
* a_wkgubun 정의
 'J' : 외래접수
 'S' : 외래수납
 'I' : 입원등록
 'T' : 퇴원수납
 'Z' : 전과, 전실
 'Y' : 진료예약

□ Return값 정의 
 0 : 선택진료 신청서 작성 대상자
 1 : 선택진료 신청서 작성 완료자
 2 : 비선택 진료의사에게 선택진료를 지정한 경우.
 3 : 급종/유형별 선택진료 지정 위배.
 4 : 진료과 선택진료 지정 위배.
 5 : 감면코드 선택진료 지정 위배.
 6 : 당일 진료과장의 선택진료 불가능.
 7 : 입원등록(I), 퇴원수납(T) 시 선택진료 대상자인데 선택진료를 미지정한 경우.
 8 : 외래접수(J) 선택진료가 대상자이지만 선택진료 미지정한 경우.
10 : 산과는 선택진료를 미적용 진료과장인데 선택진료 지정이 된 경우.
11 : 외래접수(J) 시 산부인과 선택진료 지정이 된 경우. 단 전체 선택진료 가능 진료과장 제외.
12 : 외래수납(S) 시 산부인과 분만외 상병일 경우 선택진료가 가능 안내.
13 : 외래수납(S) 시 선택진료가 불가능 안내.
14 : 급종/유형에 따른 선택진료를 불가능 안내.
15 : 진료예약(Y) 시 산과에 때한 선택진료 불가능 안내. 단 전체 선택진료 가능 진료과장 제외.
16 : 진료구분(메르스)일 경우 선택진료 불가
17 : 자보 산재 100%환자 진료신청서 미작성, 선택진료로 입원등록 및 전과 전실시..
18 : 자보 산재 100%환자 진료신청서 미작성, 일반진료로 입원등록 및 전과 전실시..

※ 주요변경 사항
○2015.05.28 
1. f_c020197_specialyn → gf_specialyn
2. 순차적으로 f_c020197_specialmed_chk를 gf_specialmed_chk 변경할 것.

○2015.06.18  김진영
1.인자값 진료구분 추가.
2.진료구분이 메르스일 경우 선택진료 불가.

*/

String	ls_specialmedapp_chk, ls_chargrate_specialable, ls_dccode_specialable, ls_dccode_name, ls_oridmt_reserve, ls_patkind_special, ls_patkind_patgroup
String   ls_yemast_orbsdate, ls_fdate, ls_oridmt_deptstat
Integer	li_drcode_special, li_employee_special, li_employee_ispecial
Integer	li_oridmt_special, li_gy_diagcnt, li_orslip_cnt
Boolean  lb_og_special

IF a_bsdate < "20130501" THEN
	Return -1
END IF

if a_bsdate >= '20150901' then
	lb_og_special = True
else
	lb_og_special = False
	IF a_bsdate >= "20140101" AND (a_drcode = "01494" OR a_drcode = "02472" OR a_drcode = "03482") THEN
		lb_og_special = True
	END IF
end if

select patgroup
  into :ls_patkind_patgroup
  from tb_patkind
 where patkind = :a_patkind
using SQLCA;


li_oridmt_special = a_special
ls_oridmt_deptstat = as_deptstat


// (2015.06.18:김진영) 진료구분이 메르스일 경우 선택진료 불가.
IF ls_oridmt_deptstat = 'a' THEN
	IF li_oridmt_special = 1 THEN
		IF a_wkgubun <> 'Y' THEN	
			Messagebox("확 인 [gf_c020197_specialmed_chk 16]", "해당 환자의 진료구분(메르스)은 선택진료를 적용할 수 없습니다.~r~n" +&
																			  "선택진료 지정을 취소하세요!")
		END IF
	
		Return 9	
	ELSE
		Return -1
	END IF
END IF

select reserve
  into :ls_oridmt_reserve
  from to_oridmt
 where hospno = :a_hospno
   and bsdate = :a_bsdate
	and bstime = :a_bstime
	and dept   = :a_dept
using SQLCA;
IF ls_oridmt_reserve = '' OR ISNULL(ls_oridmt_reserve) THEN ls_oridmt_reserve = ' '

// 급종-유형별 선택진료 검증
select specialable
  into :ls_chargrate_specialable
  from tb_chargrate c
 where c.patkind = :a_patkind
   and c.pattype = :a_pattype
	and :a_bsdate between c.f_date and c.t_date
using SQLCA;

// 자보 선택진료 적용
select special
  into :ls_patkind_special
  from tb_specialdrcode_patkind a
 where a.drcode = :a_drcode
	and a.patgroup = :ls_patkind_patgroup
	and :a_bsdate between a.fdate and a.tdate
using SQLCA;
IF ls_patkind_special = '' OR ISNULL(ls_patkind_special) THEN ls_patkind_special = '0'

// (2013.11.21 : 김진영) 입원 자보 100% 선택진료 신청서 받음
IF a_wkgubun = 'I' or a_wkgubun = 'Z' THEN
	IF (a_patkind = "50" or a_patkind = "10")  AND a_pattype = "06" THEN  // (2015.06.10 : 황인주) 입원 산재 100% 선택진료 신청서 받음
		select F_SPECIALMEDAPP_CHECK01(:a_wkgubun, :a_hospno, :a_bsdate, :a_bstime, :a_dept, :a_drcode, :a_stat, :a_patkind, :a_pattype) 
		  into :ls_specialmedapp_chk
		  from dual
		using SQLCA;

		IF ls_specialmedapp_chk = '0' THEN
			if li_oridmt_special = 1 then
				Messagebox("확 인 [gf_specialmed_chk 17]", "해당 과장님은 해당 급종/유형은 선택진료를 적용할 수 없습니다.~r~n" +&
																		 "선택진료 지정은 취소,  신청서 선작성 바랍니다.")
				Return 17
			else
				Return 18
			end if
		ELSE
			if ls_patkind_special = '0' and li_oridmt_special = 1  then
				Messagebox("확 인 [gf_specialmed_chk 14]", "해당 과장님은 해당 급종/유형은 선택진료를 적용할 수 없습니다.~r~n" +&
																		  "선택진료 지정을 취소하세요!")
				return 14
			else
				return -1
			end if
		END IF	
   end if
END IF


IF ls_chargrate_specialable = '0' THEN
	IF li_oridmt_special = 1 THEN
		IF a_wkgubun <> 'Y' THEN	
			Messagebox("확 인 [gf_specialmed_chk 2]", "해당 환자의 급종/유형은 선택진료를 적용할 수 없습니다.~r~n" +&
																			 "선택진료 지정을 취소하세요!")
		END IF
		Return 3
	ELSE
		Return -1
	END IF
END IF



IF ls_patkind_special = '0' THEN
	IF li_oridmt_special = 1 THEN
		IF a_wkgubun <> 'Y' THEN	
			Messagebox("확 인 [gf_specialmed_chk 14]", "해당 과장님은 해당 급종/유형은 선택진료를 적용할 수 없습니다.~r~n" +&
																			  "선택진료 지정을 취소하세요!")
		END IF
		Return 14
	ELSE
		Return -1
	END IF
END IF	

// 진료과 선택진료 검증
CHOOSE CASE a_dept
	CASE "ER"
		IF li_oridmt_special = 1 THEN
			Messagebox("확 인 [gf_specialmed_chk 3]", "응급실은 선택진료를 적용할 수 없습니다.~r~n" +&
																			 "선택진료 지정을 취소하세요!")
			Return 4	
		ELSE
			Return -1
		END IF
	CASE "OG"
		IF a_wkgubun = 'J' AND lb_og_special = False THEN
			IF li_oridmt_special = 1 THEN
				Messagebox("확 인 [gf_specialmed_chk 11]", "산부인과는 접수 시 선택진료를 적용할 수 없습니다.~r~n" +&
																				  "선택진료 지정을 취소하세요!")
				Return 11
			ELSE
				Return -1
			END IF
		END IF
	CASE "OB"
		IF lb_og_special = False THEN
			IF li_oridmt_special = 1 THEN
				Messagebox("확 인 [gf_specialmed_chk 10]", "산과는 선택진료를 적용할 수 없습니다.~r~n" +&
																				  "선택진료 지정을 취소하세요!")
				Return 10
			ELSE
				Return -1
			END IF
		END IF
END CHOOSE

// 선택진료 감면 검증
select specialable, dcname
  into :ls_dccode_specialable, :ls_dccode_name
  from tb_dccode d
 where d.dccode = :a_dccode 
using SQLCA;
IF ls_dccode_specialable = '' OR ISNULL(ls_dccode_specialable) THEN ls_dccode_specialable = '0'

IF ls_dccode_specialable = '0' THEN
	IF li_oridmt_special = 1 THEN
		IF a_wkgubun <> 'Y' THEN
			Messagebox("확 인 [gf_specialmed_chk 4]", "[" + ls_dccode_name + "]감면코드는 선택진료를 적용할 수 없습니다.~r~n" +&
																			 "선택진료 지정을 취소하세요!")
		END IF																		 
		
		Return 5	
	ELSE
		Return -1
	END IF
END IF


// 진료과장이 선택의사인가? 아니면 리턴.
select special, ispecial
  into :li_employee_special, :li_employee_ispecial
  from tb_employee e
 where e.drcode = :a_drcode
using SQLCA;

IF a_stat > ' ' THEN
// 입원
	if a_bsdate = gf_sysdate() then//or a_wkgubun = 'Z' or a_wkgubun = 'T' then
		li_drcode_special = li_employee_ispecial
	else
		li_drcode_special = integer(ls_patkind_special)
	end if
ELSE
// 외래
	if a_bsdate = gf_sysdate() then
		li_drcode_special = li_employee_special
	else
		li_drcode_special = integer(ls_patkind_special)
	end if
END IF

select sum(hold)
  into :li_orslip_cnt 
  from to_orslip
 where hospno = :a_hospno and dept = :a_dept
	and bsdate = :a_bsdate and bstime = :a_bstime
	and patkind = :a_patkind and pattype = :a_pattype
using SQLCA;
gf_sqlerror3 (SQLCA, False)					



IF li_drcode_special = 0 THEN
// 비선택진료 의사일 경우	
	IF li_oridmt_special = 1 THEN
		IF a_wkgubun <> 'Y' THEN
			Messagebox("확 인 [gf_specialmed_chk 1]", "비선택 진료의사에게 선택진료가 지정되었습니다.~r~n" +&
																			 "선택진료 지정을 취소하세요!")
		END IF
		
		Return 2
	ELSE
		Return -1	
	END IF
ELSE
// 선택진료 의사일 경우
   IF a_wkgubun = 'Y' THEN
	// 진료예약 점검
		IF a_bsdate >= "20140101" THEN
		// 2014.01.01 이후
			CHOOSE CASE a_dept
				CASE "OG"
					IF lb_og_special THEN
						// 선택진료 신청서작성 유무 확인. 2015.04.14 제창숙쌤 요청 산부인과도 접수 시 선택진료 신청서 받을수 있도록
						select F_SPECIALMEDAPP_CHECK01(:a_wkgubun, :a_hospno, :a_bsdate, :a_bstime, :a_dept, :a_drcode, :a_stat, :a_patkind, :a_pattype) 
						  into :ls_specialmedapp_chk
						  from dual
						using SQLCA;
					
						IF ls_specialmedapp_chk = '0' THEN
							Return 0	
						ELSE
							Return 1
						END IF
						
					ELSE
						select count(1)
						  into :li_gy_diagcnt
						  from tb_iodiag a
						 where a.hospno = :a_hospno
							and a.bsdate = :a_bsdate
							and a.bstime = :a_bstime
							and a.dept = :a_dept
							and a.major = 1
							and (a.diagcode not like 'O%' and a.diagcode not like 'Z321%')
						using SQLCA;
						
						IF li_gy_diagcnt > 0 THEN
						// 부인과 상병일 경우.
							Return 1
						ELSE
						// 산과 상병일 경우.
							Return 14
						END IF
					END IF
				CASE "OB"
					IF lb_og_special THEN
						// 선택진료 신청서작성 유무 확인. 2015.04.14 제창숙쌤 요청 산부인과도 접수 시 선택진료 신청서 받을수 있도록
						select F_SPECIALMEDAPP_CHECK01(:a_wkgubun, :a_hospno, :a_bsdate, :a_bstime, :a_dept, :a_drcode, :a_stat, :a_patkind, :a_pattype) 
						  into :ls_specialmedapp_chk
						  from dual
						using SQLCA;
					
						IF ls_specialmedapp_chk = '0' THEN
							Return 0	
						ELSE
							Return 1
						END IF
						//Return 1
					ELSE
						Return 15
					END IF
				CASE ELSE
					Return 1
			END CHOOSE			
		ELSE
		// 2014.01.01 이전
			CHOOSE CASE a_dept
				CASE "OG"
					select count(1)
					  into :li_gy_diagcnt
					  from tb_iodiag a
					 where a.hospno = :a_hospno
						and a.bsdate = :a_bsdate
						and a.bstime = :a_bstime
						and a.dept = :a_dept
						and a.major = 1
						and (a.diagcode not like 'O%' and a.diagcode not like 'Z321%')
					using SQLCA;
					
					IF li_gy_diagcnt > 0 THEN
					// 부인과 상병일 경우.
						Return 1
					ELSE
					// 산과 상병일 경우.
						Return 14
					END IF
				CASE "OB"
					Return 15
				CASE "GY"
					Return 1
				CASE ELSE
					Return 1
			END CHOOSE
		END IF
	ELSE
	// 진료예약 점검 이외
		CHOOSE CASE a_wkgubun
			CASE 'J', 'S'
			// 외래접수(J), 외래수납(S)
				IF li_oridmt_special = 1 THEN
				// 선택진료 지정
					IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 0 THEN
						IF a_wkgubun = 'S' THEN													
						// 외래수납
						   IF ls_oridmt_reserve = '0' THEN
							// 진료예약 아닐 경우
								IF li_orslip_cnt > 0 THEN
									Messagebox("확 인 [gf_specialmed_chk 13]", "당일 선택진료가 불가능합니다.~r~n" +&
																									  "외래수납 - 전환프로그램에서 일반진료로 전환하세요!")
									Return 13
								ELSE
									update to_order o
										set o.special = 0
									 where o.hospno = :a_hospno and o.dept = :a_dept
										and o.bsdate = :a_bsdate and o.bstime = :a_bstime
										and o.special = 1
										and o.sunab = 0
									using SQLCA;
									gf_sqlerror3 (SQLCA, False)									
									
									update to_oridmt o
										set o.special = 0
									 where o.hospno = :a_hospno and o.dept = :a_dept
										and o.bsdate = :a_bsdate and o.bstime = :a_bstime
										and o.special = 1
									using SQLCA;
									gf_sqlerror3 (SQLCA, False)
									
									li_oridmt_special = 0
									Return -1
								END IF
							END IF
						ELSE
						// 외래접수
							Messagebox("확 인 [gf_specialmed_chk 5]", "당일 선택진료가 불가능합니다.~r~n" +&
																							 "선택진료 지정을 취소하세요!")
							Return 6
						END IF
					END IF
				ELSE
				// 비선택진료 지정 li_oridmt_special <> 1
					IF a_wkgubun = 'J' THEN
					// 외래접수
					   IF a_bsdate >= "20140101" THEN
						// 2014.01.01 이후
							IF a_dept <> "OG" THEN
								IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 1 THEN
									Messagebox("확 인 [gf_specialmed_chk 8]", "당일 선택진료가 가능합니다.~r~n" +&
																									 "선택진료로 지정하세요!")
									Return 8
								END IF
							ELSE
							// 산부인과 박정석(01494) 또는 조현철(02472) 과장님은 산과, 부인과 선택진료 함.
								IF lb_og_special THEN
									IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 1 THEN
										Messagebox("확 인 [gf_specialmed_chk 8]", "당일 선택진료가 가능합니다.~r~n" +&
																										 "선택진료로 지정하세요!")
										Return 8
									END IF
								ELSE
									Return -1
								END IF
							END IF							
						ELSE
						// 2014.01.01 이전
							IF a_dept <> "OG" THEN
								IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 1 THEN
									Messagebox("확 인 [gf_specialmed_chk 8]", "당일 선택진료가 가능합니다.~r~n" +&
																									 "선택진료로 지정하세요!")
									Return 8
								END IF
							ELSE
								Return -1
							END IF
						END IF
					ELSE
					// 외래수납
						IF a_dept = "OG" AND lb_og_special = False THEN
							select count(1)
							  into :li_gy_diagcnt
							  from tb_iodiag a
							 where a.hospno = :a_hospno
								and a.bsdate = :a_bsdate
								and a.bstime = :a_bstime
								and a.dept = :a_dept
								and a.major = 1
								and (a.diagcode not like 'O%' and a.diagcode not like 'Z321%')	// (2013.06.03:김진영) 전현근 주임요청(Z321상병도 산과상병으로 포함.)
							using SQLCA;
							
							IF li_gy_diagcnt > 0 THEN
							// 부인과 상병일 경우
								IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 1 THEN
									
									IF li_orslip_cnt > 0 THEN									
										Messagebox("확 인 [gf_specialmed_chk 12]", "당일 선택진료가 가능합니다.~r~n" +&
																										  "외래수납 - 전환프로그램에서 선택진료로 전환하세요!")
										Return 12
									ELSE
										update to_order o
											set o.special = 1
										 where o.hospno = :a_hospno and o.dept = :a_dept
											and o.bsdate = :a_bsdate and o.bstime = :a_bstime
											and o.special = 0
											and o.sunab = 0
											and exists (select 1 from tb_sumata m
															 where o.pummok = m.pummok
																and o.bsdate between m.f_date and m.t_date
																and m.special = 1)
										using SQLCA;
										gf_sqlerror3 (SQLCA, False)									
										
										update to_oridmt o
											set o.special = 1
										 where o.hospno = :a_hospno and o.dept = :a_dept
											and o.bsdate = :a_bsdate and o.bstime = :a_bstime
											and o.special = 0
										using SQLCA;
										gf_sqlerror3 (SQLCA, False)
										
										li_oridmt_special = 1
									END IF
								ELSE
									Return -1
								END IF
							ELSE
							// 산과 상병일 경우							
								Return -1
							END IF
						ELSE
						// 산부인과 외 진료과(2014.01.01 산부인과도 포함)
							IF gf_specialyn(a_wkgubun, a_drcode, a_bsdate) = 1 THEN
								IF li_orslip_cnt > 0 THEN
									Messagebox("확 인 [gf_specialmed_chk 9]", "당일 선택진료가 가능합니다.~r~n" +&
																									 "외래수납 - 전환프로그램에서 선택진료로 전환하세요!")
									Return 9
								ELSE
									update to_order o
										set o.special = 1
									 where o.hospno = :a_hospno and o.dept = :a_dept
										and o.bsdate = :a_bsdate and o.bstime = :a_bstime
										and o.special = 0
										and o.sunab = 0
										and exists (select 1 from tb_sumata m
														 where o.pummok = m.pummok
															and o.bsdate between m.f_date and m.t_date
															and m.special = 1)
									using SQLCA;
									gf_sqlerror3 (SQLCA, False)									
										
									update to_oridmt o
										set o.special = 1
									 where o.hospno = :a_hospno and o.dept = :a_dept
										and o.bsdate = :a_bsdate and o.bstime = :a_bstime
										and o.special = 0
									using SQLCA;
									gf_sqlerror3 (SQLCA, False)
									li_oridmt_special = 1
								END IF
							ELSE
								Return -1
							END IF		
						END IF
					END IF
				END IF
			CASE 'I', 'T', 'Z'
			// 입원등록(I), 퇴원수납(T)
				IF li_oridmt_special = 0 THEN
					Messagebox("확 인 [gf_specialmed_chk 7]", "선택진료를 지정하세요!")
					Return 7
				END IF
		END CHOOSE

		// 선택진료 신청서작성 유무 확인.
		select F_SPECIALMEDAPP_CHECK01(:a_wkgubun, :a_hospno, :a_bsdate, :a_bstime, :a_dept, :a_drcode, :a_stat, :a_patkind, :a_pattype) 
		  into :ls_specialmedapp_chk
		  from dual
		using SQLCA;
	
		IF ls_specialmedapp_chk = '0' THEN
			Return 0	
		END IF
	END IF
END IF

Return 1
end function

